# unified-ui-framework

## Contribute

Please read thoroughly the [Code Structure & Style Guide](https://engineering.eng.sonicwall.com/pages/viewpage.action?pageId=20468386)

## As a Library

**Only** `shared` and `assets` folders will be exposed for other apps that using UUIF as library, any other folders are strictly specific
and limited to UUIF self-use. Anything placed in folders other than `shared` and `assets` are not reusable for apps, so if you use some of those stuff in the reusable components written in `shared` folder, this reusable component will be just broken when it is used in apps.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm test

# run unit tests with coverage report
npm test -- --coverage

```

## Releases

Refer to [Release Notes](https://engineering.eng.sonicwall.com/display/UXUI/Release+Notes) for details.
