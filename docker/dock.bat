COPY .\docker\.dockerignore .
COPY .\docker\Dockerfile .

docker stop uuif-prod-app
docker rm uuif-prod-app
docker rmi uuif-prod-i
docker build -t uuif-prod-i .

DEL .\.dockerignore
DEL .\Dockerfile

docker run -it --name uuif-prod-app -p 8080:8080 -v %cd%:/var/app uuif-prod-i

