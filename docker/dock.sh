
cp ./docker/.dockerignore .
cp ./docker/Dockerfile .

docker stop uuif-prod-app && docker rm uuif-prod-app
docker rmi uuif-prod-i
docker build -t uuif-prod-i .
rm ./.dockerignore
rm ./Dockerfile

winpty docker run -it --name uuif-prod-app -p 8080:8080 -v /$(pwd):/var/app uuif-prod-i

