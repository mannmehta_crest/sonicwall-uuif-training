const babelTransformer = require('babel-jest');
const tsTransformer = require('ts-jest');
const convertSourceMap = require('convert-source-map');

const tsBabelTransformer = {
  canInstrument: tsTransformer.canInstrument,
  getCacheKey: tsTransformer.getCacheKey,
  process(src, filename, config, options) {
    /** we call ts first then babel next */
    const isTsx = /\.tsx$/.test(filename);
    const isFromVue = /\.vue\.tsx?$/.test(filename);
    let processedSrc = src;

    // console.warn(`> run ts-jest for [${filename}]`);
    processedSrc = tsTransformer.process(processedSrc, filename, config, options);
    if (typeof processedSrc !== 'string') { return processedSrc; }
    // if (filename.indexOf('sw-toggle.spec.ts') >= 0) {
    //   console.warn(processedSrc);
    // }

    if (!isTsx) {
      /** no need for further babel compilation */
      const index = processedSrc.indexOf('\n//# sourceMappingURL=');
      const inlineSourceMap = processedSrc.substr(index + 1);
      const code = processedSrc.substring(0, index);
      const sourceMap = convertSourceMap.fromComment(inlineSourceMap).toJSON();
      if (isFromVue) { return { code, sourceMap }; }
      return { code, map: sourceMap };
    }

    processedSrc = babelTransformer.process(processedSrc, filename, config, options);
    // if (filename.indexOf('sw-tab-group.vue.tsx') >= 0) {
    //   console.warn(processedSrc.map);
    // }

    return processedSrc;
  },
};

module.exports = tsBabelTransformer;
