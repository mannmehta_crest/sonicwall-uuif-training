const babelTransformer = require('babel-jest');

const myBabelTransformer = {
  canInstrument: babelTransformer.canInstrument,
  getCacheKey: undefined,
  process(src, filename, config, options) {
    // console.warn(`> run babel-jest for [${filename}]`);
    let processedSrc = babelTransformer.process(src, filename, config, options);
    // if (filename.indexOf('vue-keylines.js') >= 0) {
    //   console.warn(processedSrc);
    // }

    return processedSrc;
  },
};

module.exports = myBabelTransformer;
