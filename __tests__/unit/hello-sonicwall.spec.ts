// import Vue from 'vue';
import { mount } from '@vue/test-utils';

import { i18nService } from '@libs-uuif/shared/i18n-service';
import SwFtrHelloSonicwall from '@/modules/sw-ftr-hello-sonicwall/sw-ftr-hello-sonicwall.vue';

/* tslint:disable:newline-per-chained-call */

describe('sw-ftr-hello-sonicwall.vue', () => {
  it('Hello SonicWall title', () => {
    const ftrWrapper = mount(SwFtrHelloSonicwall, { i18n: i18nService.i18n });
    const titleWrapper = ftrWrapper.find('h1');

    expect(titleWrapper.exists()).toBe(true);
    expect(titleWrapper.text()).toBe('Hello, SonicWall!');
  });
});
