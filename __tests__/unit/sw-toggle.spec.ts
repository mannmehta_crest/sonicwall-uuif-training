// import Vue from 'vue';
import { mount } from '@vue/test-utils';
import SwFtrToggle from '../../src/modules/sw-ftr-toggle/sw-ftr-toggle.vue';
import SwFtrTabLayout from '../../src/modules/sw-ftr-tab-layout/sw-ftr-tab-layout.vue';
import { i18nService } from '../../src/shared/i18n-service';
// import SwFtrGraphs from '../../src/modules/sw-ftr-graphs/sw-ftr-graphs.vue';

/* tslint:disable:newline-per-chained-call */

describe('sw-ftr-toggle.vue', () => {
  it('displays default message', () => {
    // const vm1 = new SwFtrGraphs().$mount();
    const vm = new SwFtrTabLayout({ i18n: i18nService.i18n }).$mount();
    // expect(vm1.$el.textContent).not.toBe('');
    expect(vm.$el.textContent).not.toBe('');
  });

  it('toggles message when button is clicked', () => {
    const wrapper = mount(SwFtrToggle);
    const button = wrapper.find({ name: 'basicSwitch' });
    const p = wrapper.find('h4');

    expect(button.exists()).toBe(true);
    button.trigger('click');
    setTimeout(
      () => {
        expect(p.text()).toBe('Toggle Button status is : true');
      },
      0);
  });
});
