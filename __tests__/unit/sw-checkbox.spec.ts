// import Vue from 'vue';
import { mount } from '@vue/test-utils';
import SwFtrCheckbox from '../../src/modules/sw-ftr-checkbox/sw-ftr-checkbox.vue';
import { i18nService } from '../../src/shared/i18n-service';

/* tslint:disable:newline-per-chained-call */

describe('sw-ftr-checkbox.vue', () => {
  it('displays checkbox message', () => {
    const vm2 = new SwFtrCheckbox({ i18n: i18nService.i18n }).$mount();
    expect((vm2.$refs.checkboxValue as HTMLSpanElement).textContent).toBe('Value: true');
  });

  function waitExecute(fn: Function) {
    return new Promise((resolve: Function) => {
      fn();
      setTimeout(resolve, 0);
    });
  }

  it('toggles message when checkbox is clicked', () => {
    const wrapper = mount(SwFtrCheckbox);
    const button = wrapper.find({ name: 'regular' });
    expect(button.exists()).toBe(true);
    waitExecute(() => { button.trigger('click'); })
    .then(() => {
      expect(wrapper.find({ ref: 'checkboxValue' }).text()).toBe('Value: false');
    });
  });

});
