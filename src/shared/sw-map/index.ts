import SwMap from './sw-map.vue';
import SwMapGl from './sw-map-gl.vue';

export *  from './types';

export {
  SwMap,
  SwMapGl,
};
