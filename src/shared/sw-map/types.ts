import Vue, { CreateElement, VNode } from 'vue';
import { Control, ControlOptions, TileLayer, Marker, ImageOverlay, MarkerClusterGroupOptions, MarkerCluster } from 'leaflet';
// import { VueRenderFunction } from '../common-types';
import { IconConfig } from '../sw-icon';

export interface MapLocation {
  id: string;
  coordinate: [number, number];
  marker?: MapMarkerConfig;
  popup?: PopupConfig<void>;
  detailView?: DetailViewInfo;
  clusterable?: boolean; // default is true
}

export interface MarkerConfig {
  name: string;
  color: string;
  size: number;
  rotate: number;
  anchorOffset: [number, number];
  draggable: boolean;
}

export type MapMarkerConfig = Partial<MarkerConfig>;
export type PopupRenderFunction<D> = (h: CreateElement, data?: D) => VNode;

export interface PopupConfig<D> {
  content: string | PopupRenderFunction<D>;
  maxWidth?: number;
  maxHeight?: number;
  offset?: [number, number];
}

export interface DetailViewInfo {
  toLoad?: boolean;
  layers: DetailViewLayer[];
}

export interface DetailViewLayer {
  toLoad?: boolean;
  id: string;
  name: string;
  actions?: DetailViewLayerAction[];
  imgSrc: string;
  interests: DetailViewInterest[];
}

export interface DetailViewLayerAction {
  key: string;
  icon: IconConfig | string;
}

export interface DetailViewInterest {
  id: string;
  xy: [number, number];
  marker?: MapMarkerConfig;
  popup?: PopupConfig<void>;
}

export interface DetailViewConfig {
  resolution: number;
  longitudeDegree: number;
}

export type MapDetailViewConfig = Partial<DetailViewConfig>;

export interface MapLayerDescriptor<D> {
  popupVueInst: Vue | null;
  popupData?: () => D;
}

/** Internal descriptor for managing various map layers */
export interface InternalLocationDescriptor extends MapLayerDescriptor<void> {
  location: MapLocation;
  clusterable: boolean;
  markerObj: Marker;
  layerMap: DetailViewLayerDescriptorMap;
}

export interface DetailViewLayerDescriptor extends MapLayerDescriptor<void> {
  layer: DetailViewLayer;
  layerObj: ImageOverlay | null;
  interestMap: DetailViewInterestDescriptorMap;
}

export interface DetailViewInterestDescriptor extends MapLayerDescriptor<void> {
  interest: DetailViewInterest;
  markerObj: Marker;
}

export type MapLocationDescriptorMap = Record<string, InternalLocationDescriptor>;
export type DetailViewLayerDescriptorMap = Record<string, DetailViewLayerDescriptor>;
export type DetailViewInterestDescriptorMap = Record<string, DetailViewInterestDescriptor>;

export interface EasyPrintOptions extends ControlOptions {
  tileLayer: TileLayer;
  exportOnly?: boolean;
  hideControlContainer?: boolean;
  sizeModes?: string[];
  filename?: string;
}

export interface EasyPrint extends Control {
  options: EasyPrintOptions;
  printMap: Function;
}

export interface DetailViewLayerActionEventData {
  layerId: string;
  actionKey: string;
}

export interface MapLocationRelocatedEventData {
  locationId: string;
  location: MapLocation;
  coordinate: [number, number];
}

export interface InterestRelocatedEventData {
  interestId: string;
  interest: DetailViewInterest;
  xy: [number, number];
}

export interface MarkerClusterPopupData {
  locationIds: string[];
}

export interface ExtendedMarkerClusterGroupOptions {
  threshold?: { low: number; high: number; };
  clusterColors?: (locationIds: string[]) => { backgroundColor?: string; fontColor?: string; };
  clusterPopup?: PopupConfig<MarkerClusterPopupData>;
  groupOptions?: MarkerClusterGroupOptions;
}

export interface ExtendedMarkerCluster extends MarkerCluster {
  swSuppressPopup?: boolean;
  layerDesc?: MapLayerDescriptor<MarkerClusterPopupData>;
}
