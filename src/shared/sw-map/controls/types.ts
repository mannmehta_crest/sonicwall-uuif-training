import { DetailViewLayerAction } from '../types';

export interface MapControlledLayer {
  id: string;
  name: string;
  actions: DetailViewLayerAction[];
}
