import 'mapbox-gl';

// return [leftTop, rightTop, rightBottom, leftBottom] fro mapbox-gl imagelayer
function calcBoundfromLocation(image: HTMLImageElement, center: [number, number]): number[][] {
  const aspectRatio = image.width / image.height;  // current accept Landscape image.
  console.warn(`aspectRatio: width/height ${aspectRatio}`);
  const widthOffset = 0.004; // in LatLng Degree.
  const ltPoint: number[] = [center[0] - widthOffset, center[1] + widthOffset / aspectRatio];
  const rtPoint: number[] = [center[0] + widthOffset, center[1] + widthOffset / aspectRatio];
  const rbPoint: number[] = [center[0] + widthOffset, center[1] - widthOffset / aspectRatio];
  const lbPoint: number[] = [center[0] - widthOffset, center[1] - widthOffset / aspectRatio];
  return [ltPoint, rtPoint, rbPoint, lbPoint];
}

export const basicStyle = '/static/data/style.json';
export const emptyStyle: mapboxgl.Style = {
  version: 8,
  name: '',
  bearing: 0,
  pitch: 0,
  sources: {},
  layers: [
    {
      id: 'background',
      type: 'background',
      paint: {
        'background-color': 'hsl(47, 26%, 88%)',
      },
    },
  ],
};

export const googleStyle: mapboxgl.Style = {
  version: 8,
  name: 'googleStyle',
  bearing: 0,
  pitch: 0,
  sources: {
    google: {
      type: 'raster',
      tiles: [`http://www.google.cn/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}
      !4i256!2m3!1e0!2sm!3i342009817!3m9!2sen-US!3sCN!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&token=32965`],
      tileSize: 256,
    },
  },
  layers: [
    {
      id: 'google',
      type: 'raster',
      source: 'google',
    },
  ],
};

export default {
  calcBoundfromLocation,
};
