export interface TabNavItem {
  navId: string;
  color: string;
  icon: string;
  textI18nPath: string;
  navTree: NavNode;
}

export interface NavNode {
  navId: string;
  type?: 'section-divider';
  icon?: string;
  textI18nPath: string;
  route?: string;
  children?: NavNode[];
}
