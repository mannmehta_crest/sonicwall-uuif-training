/**
 * Navigation data and controlled navigating utilities.
 *
 * Since navigation data is static data, they can be placed out of state management.
 */

import VueRouter from 'vue-router';
import { AppStore } from '@/store';
import { tabNavItems } from './nav.data';
import { NavNode, TabNavItem } from './types';

export type NavSearchHierarchyResult = { nodeHierarchy: NavNode[] | null, tabItem: TabNavItem | null };
export type NavSearchResult = { resNode: NavNode | null, tabItem: TabNavItem | null };

function searchNavNode(predicate: (node: NavNode) => boolean): NavSearchHierarchyResult {
  function getNodeHierarchy(children: NavNode[] | undefined): NavNode[] | null {
    if (!children || children.length <= 0) {
      return null;
    }
    let hie: NavNode[] = [];
    children.every((node) => {
      if (predicate(node)) {
        hie.push(node);
        return false;
      }
      if (Array.isArray(node.children)) {
        const childrenHie = getNodeHierarchy(node.children);
        if (childrenHie) {
          hie = hie.concat(node, childrenHie);
          return false;
        }
      }
      return true;
    });
    return hie.length > 0 ? hie : null;
  }

  let nodeHierarchy: NavNode[] | null = null;
  let tabItem: TabNavItem | null = null;
  tabNavItems.every((item) => {
    nodeHierarchy = getNodeHierarchy(item.navTree.children);
    if (nodeHierarchy) {
      tabItem = item;
      return false;
    }
    return true;
  });

  return { nodeHierarchy, tabItem };
}

function getNavNode(navId: string, withHierarchy?: false): NavSearchResult | null;
function getNavNode(navId: string, withHierarchy: true): NavSearchHierarchyResult | null;
function getNavNode(navId: string, withHierarchy: boolean = false) {
  if (!navId) {
    return null;
  }
  const hie = searchNavNode(node => (node.navId === navId));
  return withHierarchy ? hie : {
    resNode: hie.nodeHierarchy ? hie.nodeHierarchy[hie.nodeHierarchy.length - 1] : null,
    tabItem: hie.tabItem,
  };
}

function initNavId(router: VueRouter, store: AppStore) {
  let initialized = false;

  function matchRoute(fullPath: string, routePrefix: string): boolean {
    if (fullPath.indexOf(routePrefix) === 0) {
      const nextChar = fullPath.charAt(routePrefix.length);
      if (nextChar === '/' || nextChar === '?' || nextChar === '#' || !nextChar) {
        return true;
      }
    }
    return false;
  }

  router.onReady(() => {
    const route = router.currentRoute;
    const hie = searchNavNode(node => (!!node.route && matchRoute(route.path, node.route)));
    if (hie.nodeHierarchy && hie.tabItem) {
      store.commit({
        type: 'selectTopTabItem',
        navId: hie.tabItem.navId,
      });
      store.commit({
        type: 'selectNavItem',
        navId: hie.nodeHierarchy[hie.nodeHierarchy.length - 1].navId,
      });
    }
    initialized = true;
  });

  router.beforeResolve((to, from, next) => {// tslint:disable-line: no-unused
    if (initialized) {
      /** watch for changes of route, so that we can sync the route change to store if necessary */
      const hie = searchNavNode(node => (!!node.route && matchRoute(to.path, node.route)));
      if (hie.tabItem && hie.tabItem.navId !== store.state.CoreStoreModule.selectedTopTabItemId) {
        store.commit({
          type: 'selectTopTabItem',
          navId: hie.tabItem.navId,
        });
      }
      if (hie.nodeHierarchy && hie.nodeHierarchy[hie.nodeHierarchy.length - 1].navId !== store.state.CoreStoreModule.selectedNavItemId) {
        store.commit({
          type: 'selectNavItem',
          navId: hie.nodeHierarchy[hie.nodeHierarchy.length - 1].navId,
        });
      }
    }
    next();
  });
}

export const navService = {
  tabNavItems,
  getNavNode,
  initNavId,
};
