import { TabNavItem } from './types';

const i18nPathPrefix = 'navService.items';

export const tabNavItems: TabNavItem[] = [
  {
    navId: 'home',
    color: '#fda348',
    icon: 'home',
    textI18nPath: `${i18nPathPrefix}.home`,
    navTree: {
      navId: '__root',
      textI18nPath: 'rootNode',
      children: [
        {
          navId: 'home',
          icon: 'world-net',
          textI18nPath: `${i18nPathPrefix}.table-component`,
          route: '/m/feature/hello-sonicwall',
        },
      ],
    },
  },
  {
    navId: 'dashboard',
    color: '#fda348',
    icon: 'dashboard',
    textI18nPath: `${i18nPathPrefix}.dashboard`,
    navTree: {
      navId: '__root',
      textI18nPath: 'rootNode',
      children: [
        {
          navId: 'dashboard',
          icon: 'dashboard',
          textI18nPath: `${i18nPathPrefix}.charts-api`,
          route: '/m/feature/dashboard',
        },
        {
          navId: 'charts',
          icon: 'chart-area2',
          textI18nPath: `${i18nPathPrefix}.rt-charts`,
          route: '/m/feature/charts',
        },
      ],
    },
  },
];
