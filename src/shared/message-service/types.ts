import { VueRenderFunction } from '../common-types';

/** we only provide these four types for message-service per specs */
export type MessageType = 'success' | 'major' | 'failed' | 'informational';

export interface MessageDescriptor {
  uuid: string;
  timestamp: number;
  type: MessageType;
  message: string | string[] | (VueRenderFunction);
}

export type MessageCallback = (md: MessageDescriptor) => void;
