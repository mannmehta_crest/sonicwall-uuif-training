import { ReplaySubject, Subscription } from 'rxjs';
import uuidv4 from 'uuid/v4';
import { MessageCallback, MessageDescriptor, MessageType } from './types';

type MessageQueue = ReplaySubject<MessageDescriptor>;

interface MessageQueueDescriptor {
  queue: MessageQueue;
  subscription: Subscription | null;
}

class MessageManager {
  private queueMap = new Map<string, MessageQueueDescriptor>();

  private addQueue(containerId: string) {
    if (this.queueMap.has(containerId)) {
      this.removeQueue(containerId);
    }
    const queue = new ReplaySubject<MessageDescriptor>(20, 1000);
    this.queueMap.set(containerId, {
      queue,
      subscription: null,
    });
  }

  private removeQueue(containerId: string) {
    const qd = this.getQueue(containerId);
    if (!qd) { return; }
    if (qd.subscription) {
      qd.subscription.unsubscribe();
    }
    qd.queue.complete();
    this.queueMap.delete(containerId);
  }

  private getQueue(containerId: string): MessageQueueDescriptor | undefined {
    return this.queueMap.get(containerId);
  }

  private attachToQueue(qd: MessageQueueDescriptor, callback: MessageCallback) {
    qd.subscription = qd.queue.subscribe(callback);
  }

  registerContainer(containerId: string, callback: MessageCallback): boolean {
    this.addQueue(containerId);

    const qd = this.getQueue(containerId);
    if (qd) {
      this.attachToQueue(qd, callback);
      return true;
    }
    return false;
  }

  unregisterContainer(containerId: string) {
    this.removeQueue(containerId);
  }

  showMessage(containerId: string, message: MessageDescriptor['message'], type: MessageType = 'informational') {
    const qd = this.getQueue(containerId);
    if (!qd) { return; }

    qd.queue.next({
      type,
      message,
      uuid: uuidv4(),
      timestamp: Date.now(),
    });
  }
}

export const messageManager = new MessageManager();
