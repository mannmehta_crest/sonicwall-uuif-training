import SwMessageContainer from './sw-message-container.vue';
import { messageManager } from './message-manager';

export { SwMessageContainer };
export { MessageType, MessageDescriptor } from './types';

export const messageService = {
  messageManager,
};
