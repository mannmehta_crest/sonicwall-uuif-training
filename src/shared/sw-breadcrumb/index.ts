import SwBreadcrumb from './sw-breadcrumb.vue';

export * from './types';

export {
  SwBreadcrumb,
};
