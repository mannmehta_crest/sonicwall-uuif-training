import { VueRenderFunction } from '../common-types';

export interface BreadcrumbItem {
  id?: string;
  icon?: string;
  text?: string | VueRenderFunction;
  clickable?: boolean;
}
