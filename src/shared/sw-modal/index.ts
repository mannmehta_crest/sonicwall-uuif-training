import SwModal from './sw-modal.vue';
import SwModalBody from './sw-modal-body.vue';
import SwModalFooter from './sw-modal-footer.vue';

export {
  SwModal,
  SwModalBody,
  SwModalFooter,
};
