export interface ProvidedModalParam {
  mode: 'regular' | 'regular-light' | 'pillar';
}
