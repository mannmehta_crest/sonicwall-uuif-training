import { VueRenderFunction } from '../common-types';

export interface ContentToolbarExpandFilterBlock {
  title: string;
  content: string | VueRenderFunction;
  width?: number;
  minWidth?: number;
  noPadding?: boolean;
}

export interface ProvidedToolbarParam {
  floatExpand: boolean;
  theme: 'light' | 'dark';
}
