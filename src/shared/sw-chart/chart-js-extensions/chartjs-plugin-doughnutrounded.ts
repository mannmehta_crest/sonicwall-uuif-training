/**
 * Modified version of answer from stackoverflow about Doughnut rounded edges:
 * https://stackoverflow.com/a/37393862
 */

import Chart from 'chart.js';

export interface DoughnutroundedOptions {
  applyToDatasets: RoundedDatasetDesc[] | boolean;
}

interface RoundedDatasetDesc {
  index: number;
  dataIndices: number[] | boolean;
}

interface RecordedArcInfo {
  x: number;
  y: number;
  radius: number;
  thickness: number;
  backgroundColor: string;
}

function recordArcInfo(chartInst: Chart, datasetIndex: number, dataIndex: number): void {
  const arc = chartInst.getDatasetMeta(datasetIndex).data[dataIndex];
  /** we append the additional info for the arc */
  (arc as any).round = {
    x: (chartInst.chartArea.left + chartInst.chartArea.right) / 2,
    y: (chartInst.chartArea.top + chartInst.chartArea.bottom) / 2,
    radius: (chartInst as any).outerRadius - (chartInst as any).radiusLength / 2 - (datasetIndex * (chartInst as any).radiusLength),
    thickness: (chartInst as any).radiusLength / 2 - (typeof arc._model.borderWidth === 'number' ? arc._model.borderWidth : 2) / 2,
    backgroundColor: arc._model.backgroundColor,
  } as RecordedArcInfo;
}

function drawRoundedForArc(chartInst: Chart, datasetIndex: number, dataIndex: number): void {
  const arc = chartInst.getDatasetMeta(datasetIndex).data[dataIndex];
  if (!(arc as any).round) { return; }

  const ctx = chartInst.ctx!;
  const startAngle = Math.PI / 2 - (arc._view as any).startAngle;
  const endAngle = Math.PI / 2 - (arc._view as any).endAngle;
  const arcRoundedInfo: RecordedArcInfo = (arc as any).round;

  ctx.save();
  ctx.translate(arcRoundedInfo.x, arcRoundedInfo.y);
  ctx.fillStyle = arc._model.backgroundColor || arcRoundedInfo.backgroundColor;
  ctx.beginPath();
  ctx.arc(arcRoundedInfo.radius * Math.sin(startAngle), arcRoundedInfo.radius * Math.cos(startAngle), arcRoundedInfo.thickness, 0, 2 * Math.PI);
  ctx.arc(arcRoundedInfo.radius * Math.sin(endAngle), arcRoundedInfo.radius * Math.cos(endAngle), arcRoundedInfo.thickness, 0, 2 * Math.PI);
  ctx.closePath();
  ctx.fill();
  ctx.restore();
}

export default {
  id: 'doughnutrounded',
  afterUpdate(chartInst, opts?: DoughnutroundedOptions) {
    if (!opts || opts.applyToDatasets === false) { return; }
    if (Array.isArray(opts.applyToDatasets)) {
      /** apply to specified datasets */
      opts.applyToDatasets.forEach((dd) => {
        if (dd.dataIndices === false) { return; }
        if (Array.isArray(dd.dataIndices)) {
          /** apply to specified data points */
          dd.dataIndices.forEach(dataIndex => (recordArcInfo(chartInst, dd.index, dataIndex)));
        } else if (dd.dataIndices === true) {
          /** or apply to all data points */
          const length = chartInst.data.datasets![dd.index].data!.length;
          for (let i = 0; i < length; i += 1) {
            recordArcInfo(chartInst, dd.index, i);
          }
        }
      });
    } else if (opts.applyToDatasets === true) {
      /** apply to all datasets and all data points */
      chartInst.data.datasets!.forEach((ds, index) => {
        const length = ds.data!.length;
        for (let i = 0; i < length; i += 1) {
          recordArcInfo(chartInst, index, i);
        }
      });
    }
  },
  afterDatasetsDraw(chartInst: Chart, easing: string, opts?: DoughnutroundedOptions) {// tslint:disable-line
    if (!opts || opts.applyToDatasets === false) { return; }
    if (Array.isArray(opts.applyToDatasets)) {
      /** apply to specified datasets */
      opts.applyToDatasets.forEach((dd) => {
        if (dd.dataIndices === false) { return; }
        if (Array.isArray(dd.dataIndices)) {
          /** apply to specified data points */
          dd.dataIndices.forEach(dataIndex => (drawRoundedForArc(chartInst, dd.index, dataIndex)));
        } else if (dd.dataIndices === true) {
          /** or apply to all data points */
          const length = chartInst.data.datasets![dd.index].data!.length;
          for (let i = 0; i < length; i += 1) {
            drawRoundedForArc(chartInst, dd.index, i);
          }
        }
      });
    } else if (opts.applyToDatasets === true) {
      /** apply to all datasets and all data points */
      chartInst.data.datasets!.forEach((ds, index) => {
        const length = ds.data!.length;
        for (let i = 0; i < length; i += 1) {
          drawRoundedForArc(chartInst, index, i);
        }
      });
    }
  },
} as Exclude<Chart.ChartConfiguration['plugins'], undefined>[0];
