import { Chart } from 'chart.js';

export interface ChartOptions extends Chart.ChartOptions {
  annotation: AnnotationConfig;
}

export interface AnnotationConfig {
  drawTime?: string; // default: afterDatasetsDraw
  events?: string[];
  dblClickSpeed?: number;
  annotations: (LineAnnotationSet | BoxAnnotationSet)[];
}

interface BasicAnnotationSet {
  drawTime: string;
  id: string;
  borderColor: string;
  borderWidth: number;
  onMouseenter: Function;
  onMouseleave: Function;
  onMouseout: Function;
  onMousemove: Function;
  onMousedown: Function;
  onMouseup: Function;
  onClick: Function;
  onDblclick: Function;
  onContextmenu: Function;
  onWheel: Function;
}

export interface LineAnnotationSet extends Partial<BasicAnnotationSet> {
  // default: 'vertical'. ['vertical' | 'horizontal']
  mode?: 'vertical' | 'horizontal';
  type: 'line';
  scaleID: 'y-axis-0' | 'x-axis-0';
  // Data value to draw the line at
  value: number | string;
  // Optional value at which the line draw should end
  endValue?: number | string;
  borderDash?: [number, number];
  borderDashOffset?: number;
  label?: Partial<LabelConfig>;
}

export interface BoxAnnotationSet extends Partial<BasicAnnotationSet> {
  // box Annotation options
  type: 'box';
  xScaleID?: 'x-axis-0';
  yScaleID?: 'y-axis-0';
  xMin?: number | string;
  xMax?: number | string;
  // Top edge of the box in units along the y axis
  yMax?: number | string;
  // Bottom edge of the box
  yMin?: number | string;
  backgroundColor?: string;
}

export interface LabelConfig {
  enabled: boolean; // default: false
  backgroundColor: string; // default 'rgba(0,0,0,0.8)'
  fontFamily: string;
  fontSize: number;
  fontStyle: string;
  xPadding: number;
  yPadding: number;
  cornerRadius: number;
  position: 'center' | 'top' | 'bottom' | 'left' | 'right';
  xAdjust: number;
  yAdjust: number;
  content: string;
}
