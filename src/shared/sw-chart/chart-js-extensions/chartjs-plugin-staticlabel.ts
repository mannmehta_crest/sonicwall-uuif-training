import Chart from 'chart.js';

export interface StaticLabelOptions {
  enable: boolean;
  labelTemplate?: string;
  theme: string;
}

function drawStaticLabel(chartInst: Chart, theme: string) {
  const ctx = chartInst.ctx;
  chartInst.data.datasets!.forEach((dataset: Chart.ChartDataSets, i: number) => {
    const meta = chartInst.getDatasetMeta(i);
    if (!meta.hidden && ctx) {
      meta.data.forEach((element, index: number) => {
        // Draw the text in black, with the specified font
        ctx.fillStyle = 'rgb(0, 0, 0)';
        const fontSize = 14;
        const fontStyle = 'normal';
        const fontFamily = 'Nunito, sans-serif';
        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
        // extract data unit from dataset.label
        const labelWithoutUnit = dataset.label!.replace(/\[.+\]|\(.+\)/g, '');
        const labelToShow = labelWithoutUnit ? `${labelWithoutUnit}:` : '';
        const units = dataset.label!.match(/\[.+\]|\(.+\)/g);
        const dataUnit = units ? units[0].toString()
          .replace(/\[|\]|\(|\)/g, '') : '';
        const dataString = `${labelToShow} ${dataset.data![index]} ${dataUnit}`;

        // Make sure alignment settings are correct
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = theme === 'dark' ? '#c9c9c9' : '#333333';
        const padding = 5;
        const position = (element as any).tooltipPosition();
        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
      });
    }
  });
}

export default {
  id: 'staticlabel',
  afterDatasetsDraw(chartInst: Chart, easing: string, opts?: StaticLabelOptions) {
    if (!opts || !opts.enable) { return easing; }
    drawStaticLabel(chartInst, opts.theme);
  },
} as Exclude<Chart.ChartConfiguration['plugins'], undefined>[0];
