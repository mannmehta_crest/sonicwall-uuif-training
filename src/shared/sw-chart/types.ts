export * from './chart-js-extensions/chart-annotation';
export * from './chart-js-extensions/chartjs-plugin-staticlabel';
export * from './chart-js-extensions/chartjs-plugin-doughnutrounded';

export type ChartConfig = {
  title?: string,
  accessibleTitle?: string,
  labelsEnabled?: boolean,
  hoverAlpha?: number,
  outlineAlpha?: number,
  fillAlphas?: number,
  lineThickness?: number,
  bullet?: string,
  cornerRadiusTop?: number,
  type: 'pie'|'donut'|'area'|'line'|'column',
  balloon?: balloonConfig,
  pullOutRadius?: string | number,
  pullOutDuration?: number,
  pullOutOnlyOne?: boolean,
  baseColor?: string,
  colors?: string[],
  startDuration?: number,
  categoryField?: string,
  titleField?: string,
  valueField?: string,
  chartCursor?: object,
  valueAxes?: valueAxe[],
};

type balloonConfig = {
  enabled?: boolean,
};

type valueAxe = {
  title: string,
};

export interface LineData {
  data: number[];
  label: string;
}

export interface CompareBarData {
  categoryLabels: string[];
  dataset: {
    label: string;
    data: number[];
  }[];
}

export type DonutCenterLabelsFormatter = (
  labels: {
    value: number;
    label: string;
    total: number;
  },
) => string[];

export interface GaugeLollipopValue {
  value: number;
  limit?: number;
}

export interface GaugeLollipopLabelConfig {
  enable: boolean;
  formatter?: GaugeLollipopDatasetLabelFormatter;
}

export type GaugeLollipopDatasetLabelFormatter = (label: string | undefined, data: number[], active?: boolean) => string;
