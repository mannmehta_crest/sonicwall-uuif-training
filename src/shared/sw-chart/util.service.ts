import { utilService } from '../util-service';

function hex2rgba(hex: string, alpha: number = .4): string {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? `rgba(${parseInt(result[1], 16)},${parseInt(result[2], 16)},${parseInt(result[3], 16)}, ${alpha})` : hex;
}

function chartTextColor(theme: 'light' | 'dark') {
  return theme === 'dark' ? utilService.colorConst.dark_text : utilService.colorConst.light_text;
}

function pointBackgroundColor(theme: 'light' | 'dark') {
  return theme === 'dark' ? utilService.colorConst.dark_highlight2 : utilService.colorConst.statusInfoMsg;
}

function elementBorderColor(theme: 'light' | 'dark') {
  return theme === 'dark' ? utilService.colorConst.dark_mat : utilService.colorConst.backgroundMat;
}

function chartGridColor(theme: 'light' | 'dark') {
  return theme === 'dark' ? hex2rgba(utilService.colorConst.dark_text) : hex2rgba(utilService.colorConst.defaultColor, .1);
}

function chartAuxiliaryColor(theme: 'light' | 'dark') {
  return theme === 'dark' ? hex2rgba(utilService.colorConst.dark_text) : hex2rgba(utilService.colorConst.defaultColor);
}

function updateChartRegularData(chartInstance: Chart, normalizedLabels: string[], normalizedDatasets: Chart.ChartDataSets[]) {
  const data = chartInstance.data;
  data.labels = normalizedLabels;
  if (data.datasets) {
    normalizedDatasets.forEach((dataset, index) => {
      if (data.datasets![index]) {
        Object.assign(data.datasets![index], dataset);
      } else {
        data.datasets!.splice(index, 0, dataset);
      }
    });
    if (data.datasets.length > normalizedDatasets.length) {
      data.datasets.splice(normalizedDatasets.length);
    }
  } else {
    data.datasets = normalizedDatasets;
  }
}

export default {
  hex2rgba,
  chartTextColor,
  pointBackgroundColor,
  elementBorderColor,
  chartGridColor,
  chartAuxiliaryColor,
  updateChartRegularData,
};
