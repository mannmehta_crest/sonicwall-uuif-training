export interface ConfirmButton {
  key: string;
  name: string;
  isDefault?: boolean;
}
