import Vue, { VNode } from 'vue';
import { CombinedVueInstance } from 'vue/types/vue';
import { StatusName, VueRenderFunction } from '../common-types';
import { ConfirmButton } from './types';
import SwConfirmModal from './sw-confirm-modal.vue';
import { domService } from '../dom-service';
import { i18nService } from '../i18n-service';

export interface ConfirmOptions {
  status: StatusName;
  title?: string | boolean;
  message: string | string[] | VueRenderFunction;
  buttons?: ConfirmButton[];
}

interface ModalVueData {
  options: ConfirmOptions;
  visible: boolean;
  bodyStyleTicket: string;
}

type ModalVueInstance = CombinedVueInstance<Vue, ModalVueData, {}, {}, {}>;

class ConfirmService {
  private modalInstance: ModalVueInstance | undefined = undefined;

  private setupInstance(options: ConfirmOptions): void {
    if (this.modalInstance) {
      this.destroyInstance();
    }

    const self = this;

    /**
     * @event answer `(buttonKey: string)`
     */
    this.modalInstance = new Vue({
      props: [],
      data() {
        return {
          options,
          visible: true,
          bodyStyleTicket: '',
        } as ModalVueData;
      },
      methods: {
        onAnswer(key: string) {
          this.visible = false;
          this.$emit('answer', key);
        },
        onReadyToDestroy() { if (!this.visible) { self.destroyInstance(); } },
      },
      i18n: i18nService.i18n,
      render(h): VNode {// tslint:disable-line
        return (
          <SwConfirmModal
            props-dialog-visible={this.visible}
            props-status={this.options.status}
            props-title={this.options.title}
            props-message={this.options.message}
            props-buttons={this.options.buttons}
            on-answer={this.onAnswer}
            on-ready-to-destroy={this.onReadyToDestroy}
          />
        ) as VNode;
      },
      mounted() {
        this.bodyStyleTicket = domService.stylesManager.apply(document.body, { overflowX: 'hidden', overflowY: 'hidden' });
      },
      beforeDestroy() {
        if (this.bodyStyleTicket) {
          domService.stylesManager.cancel(document.body, this.bodyStyleTicket);
          this.bodyStyleTicket = '';
        }
      },
    });

    this.modalInstance.$mount();
    document.body.appendChild(this.modalInstance.$el);
  }

  private destroyInstance(): void {
    if (!this.modalInstance) { return; }
    document.body.removeChild(this.modalInstance.$el);
    this.modalInstance.$destroy();
    this.modalInstance = undefined;
  }

  private refreshInstance(options: ConfirmOptions) {
    if (!this.modalInstance) { return; }
    this.modalInstance.visible = true;
    this.modalInstance.options = options;
  }

  public confirm(options: ConfirmOptions): Promise<string> {
    return new Promise<string>((resolve) => {
      if (this.modalInstance) {
        this.refreshInstance(options);
      } else {
        this.setupInstance(options);
      }

      this.modalInstance!.$once('answer', (key: string) => {
        resolve(key);
      });
    });
  }
}

export const confirmService = new ConfirmService();
