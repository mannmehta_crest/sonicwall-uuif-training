
/* tslint:disable */
const data: {[code: string]: {name: string; imageName: string; myCode: string; isoCode: string;}} = {
  "0": {
    "name": "Unknown",
    "imageName": "Unknown.png",
    "myCode": "0",
    "isoCode": "0"
  },
  "$A1": {
    "name": "Anonymous Proxy",
    "imageName": "Anonymous-Proxy.png",
    "myCode": "",
    "isoCode": "$A1",
  },
  "$A2": {
    "name": "Satellite Provider",
    "imageName": "Satellite-Provider.png",
    "myCode": "",
    "isoCode": "$A2",
  },
  "AD": {
    "name": "Andorra",
    "imageName": "Andorra.png",
    "myCode": "3",
    "isoCode": "AD"
  },
  "AE": {
    "name": "United Arab Emirates",
    "imageName": "United-Arab-Emirates.png",
    "myCode": "4",
    "isoCode": "AE"
  },
  "AF": {
    "name": "Afghanistan",
    "imageName": "Afghanistan.png",
    "myCode": "5",
    "isoCode": "AF"
  },
  "AG": {
    "name": "Antigua and Barbuda",
    "imageName": "Antigua-and-Barbuda.png",
    "myCode": "6",
    "isoCode": "AG"
  },
  "AI": {
    "name": "Anguilla",
    "imageName": "Anguilla.png",
    "myCode": "7",
    "isoCode": "AI"
  },
  "AL": {
    "name": "Albania",
    "imageName": "Albania.png",
    "myCode": "8",
    "isoCode": "AL"
  },
  "AM": {
    "name": "Armenia",
    "imageName": "Armenia.png",
    "myCode": "9",
    "isoCode": "AM"
  },
  "AN": {
    "name": "Netherlands Antilles",
    "imageName": "Netherlands-Antilles.png",
    "myCode": "10",
    "isoCode": "AN"
  },
  "AO": {
    "name": "Angola",
    "imageName": "Angola.png",
    "myCode": "11",
    "isoCode": "AO"
  },
  "$AP": {
    "name": "Asia/Pacific Region",
    "imageName": "Unknown.png",
    "myCode": "",
    "isoCode": "$AP"
  },
  "AQ": {
    "name": "Antarctica",
    "imageName": "Antarctica.png",
    "myCode": "13",
    "isoCode": "AQ"
  },
  "AR": {
    "name": "Argentina",
    "imageName": "Argentina.png",
    "myCode": "14",
    "isoCode": "AR"
  },
  "AS": {
    "name": "American Samoa",
    "imageName": "American-Samoa.png",
    "myCode": "15",
    "isoCode": "AS"
  },
  "AT": {
    "name": "Austria",
    "imageName": "Austria.png",
    "myCode": "16",
    "isoCode": "AT"
  },
  "AU": {
    "name": "Australia",
    "imageName": "Australia.png",
    "myCode": "17",
    "isoCode": "AU"
  },
  "AW": {
    "name": "Aruba",
    "imageName": "Aruba.png",
    "myCode": "18",
    "isoCode": "AW"
  },
  "AX": {
    "name": "Åland Islands",
    "imageName": "Aland.png",
    "myCode": "19",
    "isoCode": "AX"
  },
  "AZ": {
    "name": "Azerbaijan",
    "imageName": "Azerbaijan.png",
    "myCode": "20",
    "isoCode": "AZ"
  },
  "BA": {
    "name": "Bosnia and Herzegovina",
    "imageName": "Bosnia-and-Herzegovina.png",
    "myCode": "21",
    "isoCode": "BA"
  },
  "BB": {
    "name": "Barbados",
    "imageName": "Barbados.png",
    "myCode": "22",
    "isoCode": "BB"
  },
  "BD": {
    "name": "Bangladesh",
    "imageName": "Bangladesh.png",
    "myCode": "23",
    "isoCode": "BD"
  },
  "BE": {
    "name": "Belgium",
    "imageName": "Belgium.png",
    "myCode": "24",
    "isoCode": "BE"
  },
  "BF": {
    "name": "Burkina Faso",
    "imageName": "Burkina-Faso.png",
    "myCode": "25",
    "isoCode": "BF"
  },
  "BG": {
    "name": "Bulgaria",
    "imageName": "Bulgaria.png",
    "myCode": "26",
    "isoCode": "BG"
  },
  "BH": {
    "name": "Bahrain",
    "imageName": "Bahrain.png",
    "myCode": "27",
    "isoCode": "BH"
  },
  "BI": {
    "name": "Burundi",
    "imageName": "Burundi.png",
    "myCode": "28",
    "isoCode": "BI"
  },
  "BJ": {
    "name": "Benin",
    "imageName": "Benin.png",
    "myCode": "29",
    "isoCode": "BJ"
  },
  "BL": {
    "name": "Saint Barthélemy",
    "imageName": "Saint-Barthelemy.png",
    "myCode": "",
    "isoCode": "BL"
  },
  "BM": {
    "name": "Bermuda",
    "imageName": "Bermuda.png",
    "myCode": "30",
    "isoCode": "BM"
  },
  "BN": {
    "name": "Brunei Darussalam",
    "imageName": "Brunei.png",
    "myCode": "31",
    "isoCode": "BN"
  },
  "BO": {
    "name": "Bolivia (Plurinational State of)",
    "imageName": "Bolivia.png",
    "myCode": "32",
    "isoCode": "BO"
  },
  "BQ": {
    "name": "Bonaire, Sint Eustatius and Saba",
    "imageName": "Bonaire-Saint-Eustatius-and-Saba.png",
    "myCode": "",
    "isoCode": "BQ"
  },
  "BR": {
    "name": "Brazil",
    "imageName": "Brazil.png",
    "myCode": "33",
    "isoCode": "BR"
  },
  "BS": {
    "name": "Bahamas",
    "imageName": "Bahamas.png",
    "myCode": "34",
    "isoCode": "BS"
  },
  "BT": {
    "name": "Bhutan",
    "imageName": "Bhutan.png",
    "myCode": "35",
    "isoCode": "BT"
  },
  "BV": {
    "name": "Bouvet Island",
    "imageName": "Bouvet-Island.png",
    "myCode": "",
    "isoCode": "BV"
  },
  "BW": {
    "name": "Botswana",
    "imageName": "Botswana.png",
    "myCode": "37",
    "isoCode": "BW"
  },
  "BY": {
    "name": "Belarus",
    "imageName": "Belarus.png",
    "myCode": "38",
    "isoCode": "BY"
  },
  "BZ": {
    "name": "Belize",
    "imageName": "Belize.png",
    "myCode": "39",
    "isoCode": "BZ"
  },
  "CA": {
    "name": "Canada",
    "imageName": "Canada.png",
    "myCode": "40",
    "isoCode": "CA"
  },
  "CC": {
    "name": "Cocos (Keeling) Islands",
    "imageName": "Cocos-Keeling-Islands.png",
    "myCode": "41",
    "isoCode": "CC"
  },
  "CD": {
    "name": "Congo, Democratic Republic of the",
    "imageName": "Democratic-Republic-of-the-Congo.png",
    "myCode": "42",
    "isoCode": "CD"
  },
  "CF": {
    "name": "Central African Republic",
    "imageName": "Central-African-Republic.png",
    "myCode": "43",
    "isoCode": "CF"
  },
  "CG": {
    "name": "Congo",
    "imageName": "Republic-of-the-Congo.png",
    "myCode": "44",
    "isoCode": "CG"
  },
  "CH": {
    "name": "Switzerland",
    "imageName": "Switzerland.png",
    "myCode": "45",
    "isoCode": "CH"
  },
  "CI": {
    "name": "Côte d'Ivoire",
    "imageName": "Cote-dIvoire.png",
    "myCode": "46",
    "isoCode": "CI"
  },
  "CK": {
    "name": "Cook Islands",
    "imageName": "Cook-Islands.png",
    "myCode": "47",
    "isoCode": "CK"
  },
  "CL": {
    "name": "Chile",
    "imageName": "Chile.png",
    "myCode": "48",
    "isoCode": "CL"
  },
  "CM": {
    "name": "Cameroon",
    "imageName": "Cameroon.png",
    "myCode": "49",
    "isoCode": "CM"
  },
  "CN": {
    "name": "China",
    "imageName": "China.png",
    "myCode": "50",
    "isoCode": "CN"
  },
  "CO": {
    "name": "Colombia",
    "imageName": "Colombia.png",
    "myCode": "51",
    "isoCode": "CO"
  },
  "CR": {
    "name": "Costa Rica",
    "imageName": "Costa-Rica.png",
    "myCode": "52",
    "isoCode": "CR"
  },
  "CU": {
    "name": "Cuba",
    "imageName": "Cuba.png",
    "myCode": "53",
    "isoCode": "CU"
  },
  "CV": {
    "name": "Cabo Verde",
    "imageName": "Cape-Verde.png",
    "myCode": "54",
    "isoCode": "CV"
  },
  "CX": {
    "name": "Christmas Island",
    "imageName": "Christmas-Island.png",
    "myCode": "55",
    "isoCode": "CX"
  },
  "CY": {
    "name": "Cyprus",
    "imageName": "Cyprus.png",
    "myCode": "56",
    "isoCode": "CY"
  },
  "CZ": {
    "name": "Czechia",
    "imageName": "Czech-Republic.png",
    "myCode": "57",
    "isoCode": "CZ"
  },
  "DE": {
    "name": "Germany",
    "imageName": "Germany.png",
    "myCode": "58",
    "isoCode": "DE"
  },
  "DJ": {
    "name": "Djibouti",
    "imageName": "Djibouti.png",
    "myCode": "59",
    "isoCode": "DJ"
  },
  "DK": {
    "name": "Denmark",
    "imageName": "Denmark.png",
    "myCode": "60",
    "isoCode": "DK"
  },
  "DM": {
    "name": "Dominica",
    "imageName": "Dominica.png",
    "myCode": "61",
    "isoCode": "DM"
  },
  "DO": {
    "name": "Dominican Republic",
    "imageName": "Dominican-Republic.png",
    "myCode": "62",
    "isoCode": "DO"
  },
  "DZ": {
    "name": "Algeria",
    "imageName": "Algeria.png",
    "myCode": "63",
    "isoCode": "DZ"
  },
  "EC": {
    "name": "Ecuador",
    "imageName": "Ecuador.png",
    "myCode": "64",
    "isoCode": "EC"
  },
  "EE": {
    "name": "Estonia",
    "imageName": "Estonia.png",
    "myCode": "65",
    "isoCode": "EE"
  },
  "EG": {
    "name": "Egypt",
    "imageName": "Egypt.png",
    "myCode": "66",
    "isoCode": "EG"
  },
  "EH": {
    "name": "Western Sahara",
    "imageName": "Western-Sahara.png",
    "myCode": "67",
    "isoCode": "EH"
  },
  "ER": {
    "name": "Eritrea",
    "imageName": "Eritrea.png",
    "myCode": "68",
    "isoCode": "ER"
  },
  "ES": {
    "name": "Spain",
    "imageName": "Spain.png",
    "myCode": "69",
    "isoCode": "ES"
  },
  "ET": {
    "name": "Ethiopia",
    "imageName": "Ethiopia.png",
    "myCode": "70",
    "isoCode": "ET"
  },
  "EU": {
    "name": "European Union",
    "imageName": "European-Union.png",
    "myCode": "71",
    "isoCode": "EU"
  },
  "FI": {
    "name": "Finland",
    "imageName": "Finland.png",
    "myCode": "72",
    "isoCode": "FI"
  },
  "FJ": {
    "name": "Fiji",
    "imageName": "Fiji.png",
    "myCode": "73",
    "isoCode": "FJ"
  },
  "FK": {
    "name": "Falkland Islands (Malvinas)",
    "imageName": "Falkland-Islands.png",
    "myCode": "74",
    "isoCode": "FK"
  },
  "FM": {
    "name": "Micronesia (Federated States of)",
    "imageName": "Micronesia.png",
    "myCode": "75",
    "isoCode": "FM"
  },
  "FO": {
    "name": "Faroe Islands",
    "imageName": "Faroes.png",
    "myCode": "76",
    "isoCode": "FO"
  },
  "FR": {
    "name": "France",
    "imageName": "France.png",
    "myCode": "77",
    "isoCode": "FR"
  },
  "GA": {
    "name": "Gabon",
    "imageName": "Gabon.png",
    "myCode": "78",
    "isoCode": "GA"
  },
  "GB": {
    "name": "United Kingdom of Great Britain and Northern Ireland",
    "imageName": "United-Kingdom.png",
    "myCode": "79",
    "isoCode": "GB"
  },
  "GD": {
    "name": "Grenada",
    "imageName": "Grenada.png",
    "myCode": "80",
    "isoCode": "GD"
  },
  "GE": {
    "name": "Georgia",
    "imageName": "Georgia.png",
    "myCode": "81",
    "isoCode": "GE"
  },
  "GF": {
    "name": "French Guiana",
    "imageName": "French-Guiana.png",
    "myCode": "",
    "isoCode": "GF"
  },
  "GG": {
    "name": "Guernsey",
    "imageName": "Guernsey.png",
    "myCode": "83",
    "isoCode": "GG"
  },
  "GH": {
    "name": "Ghana",
    "imageName": "Ghana.png",
    "myCode": "84",
    "isoCode": "GH"
  },
  "GI": {
    "name": "Gibraltar",
    "imageName": "Gibraltar.png",
    "myCode": "85",
    "isoCode": "GI"
  },
  "GL": {
    "name": "Greenland",
    "imageName": "Greenland.png",
    "myCode": "86",
    "isoCode": "GL"
  },
  "GM": {
    "name": "Gambia",
    "imageName": "Gambia.png",
    "myCode": "87",
    "isoCode": "GM"
  },
  "GN": {
    "name": "Guinea",
    "imageName": "Guinea.png",
    "myCode": "88",
    "isoCode": "GN"
  },
  "GP": {
    "name": "Guadeloupe",
    "imageName": "Guadeloupe.png",
    "myCode": "",
    "isoCode": "GP"
  },
  "GQ": {
    "name": "Equatorial Guinea",
    "imageName": "Equatorial-Guinea.png",
    "myCode": "90",
    "isoCode": "GQ"
  },
  "GR": {
    "name": "Greece",
    "imageName": "Greece.png",
    "myCode": "91",
    "isoCode": "GR"
  },
  "GS": {
    "name": "South Georgia and the South Sandwich Islands",
    "imageName": "South-Georgia-and-the-South-Sandwich-Islands.png",
    "myCode": "92",
    "isoCode": "GS"
  },
  "GT": {
    "name": "Guatemala",
    "imageName": "Guatemala.png",
    "myCode": "93",
    "isoCode": "GT"
  },
  "GU": {
    "name": "Guam",
    "imageName": "Guam.png",
    "myCode": "94",
    "isoCode": "GU"
  },
  "GW": {
    "name": "Guinea-Bissau",
    "imageName": "Guinea-Bissau.png",
    "myCode": "95",
    "isoCode": "GW"
  },
  "GY": {
    "name": "Guyana",
    "imageName": "Guyana.png",
    "myCode": "96",
    "isoCode": "GY"
  },
  "HK": {
    "name": "Hong Kong",
    "imageName": "Hong-Kong.png",
    "myCode": "97",
    "isoCode": "HK"
  },
  "HM": {
    "name": "Heard Island and McDonald Islands",
    "imageName": "Heard-Island-and-McDonald-Island.png",
    "myCode": "",
    "isoCode": "HM"
  },
  "HN": {
    "name": "Honduras",
    "imageName": "Honduras.png",
    "myCode": "99",
    "isoCode": "HN"
  },
  "HR": {
    "name": "Croatia",
    "imageName": "Croatia.png",
    "myCode": "100",
    "isoCode": "HR"
  },
  "HT": {
    "name": "Haiti",
    "imageName": "Haiti.png",
    "myCode": "101",
    "isoCode": "HT"
  },
  "HU": {
    "name": "Hungary",
    "imageName": "Hungary.png",
    "myCode": "102",
    "isoCode": "HU"
  },
  "ID": {
    "name": "Indonesia",
    "imageName": "Indonesia.png",
    "myCode": "103",
    "isoCode": "ID"
  },
  "IE": {
    "name": "Ireland",
    "imageName": "Ireland.png",
    "myCode": "104",
    "isoCode": "IE"
  },
  "IL": {
    "name": "Israel",
    "imageName": "Israel.png",
    "myCode": "105",
    "isoCode": "IL"
  },
  "IM": {
    "name": "Isle of Man",
    "imageName": "Isle-of-Man.png",
    "myCode": "106",
    "isoCode": "IM"
  },
  "IN": {
    "name": "India",
    "imageName": "India.png",
    "myCode": "107",
    "isoCode": "IN"
  },
  "IO": {
    "name": "British Indian Ocean Territory",
    "imageName": "British-Indian-Ocean-Territory.png",
    "myCode": "",
    "isoCode": "IO"
  },
  "IQ": {
    "name": "Iraq",
    "imageName": "Iraq.png",
    "myCode": "109",
    "isoCode": "IQ"
  },
  "IR": {
    "name": "Iran (Islamic Republic of)",
    "imageName": "Iran.png",
    "myCode": "110",
    "isoCode": "IR"
  },
  "IS": {
    "name": "Iceland",
    "imageName": "Iceland.png",
    "myCode": "111",
    "isoCode": "IS"
  },
  "IT": {
    "name": "Italy",
    "imageName": "Italy.png",
    "myCode": "112",
    "isoCode": "IT"
  },
  "JE": {
    "name": "Jersey",
    "imageName": "Jersey.png",
    "myCode": "113",
    "isoCode": "JE"
  },
  "JM": {
    "name": "Jamaica",
    "imageName": "Jamaica.png",
    "myCode": "114",
    "isoCode": "JM"
  },
  "JO": {
    "name": "Jordan",
    "imageName": "Jordan.png",
    "myCode": "115",
    "isoCode": "JO"
  },
  "JP": {
    "name": "Japan",
    "imageName": "Japan.png",
    "myCode": "116",
    "isoCode": "JP"
  },
  "KE": {
    "name": "Kenya",
    "imageName": "Kenya.png",
    "myCode": "117",
    "isoCode": "KE"
  },
  "KG": {
    "name": "Kyrgyzstan",
    "imageName": "Kyrgyzstan.png",
    "myCode": "118",
    "isoCode": "KG"
  },
  "KH": {
    "name": "Cambodia",
    "imageName": "Cambodia.png",
    "myCode": "119",
    "isoCode": "KH"
  },
  "KI": {
    "name": "Kiribati",
    "imageName": "Kiribati.png",
    "myCode": "120",
    "isoCode": "KI"
  },
  "KM": {
    "name": "Comoros",
    "imageName": "Comoros.png",
    "myCode": "121",
    "isoCode": "KM"
  },
  "KN": {
    "name": "Saint Kitts and Nevis",
    "imageName": "Saint-Kitts-and-Nevis.png",
    "myCode": "122",
    "isoCode": "KN"
  },
  "KP": {
    "name": "Korea (Democratic People's Republic of)",
    "imageName": "North-Korea.png",
    "myCode": "123",
    "isoCode": "KP"
  },
  "KR": {
    "name": "Korea, Republic of",
    "imageName": "South-Korea.png",
    "myCode": "124",
    "isoCode": "KR"
  },
  "KW": {
    "name": "Kuwait",
    "imageName": "Kuwait.png",
    "myCode": "125",
    "isoCode": "KW"
  },
  "KY": {
    "name": "Cayman Islands",
    "imageName": "Cayman-Islands.png",
    "myCode": "126",
    "isoCode": "KY"
  },
  "KZ": {
    "name": "Kazakhstan",
    "imageName": "Kazakhstan.png",
    "myCode": "127",
    "isoCode": "KZ"
  },
  "LA": {
    "name": "Lao People's Democratic Republic",
    "imageName": "Laos.png",
    "myCode": "128",
    "isoCode": "LA"
  },
  "LB": {
    "name": "Lebanon",
    "imageName": "Lebanon.png",
    "myCode": "129",
    "isoCode": "LB"
  },
  "LC": {
    "name": "Saint Lucia",
    "imageName": "Saint-Lucia.png",
    "myCode": "130",
    "isoCode": "LC"
  },
  "LI": {
    "name": "Liechtenstein",
    "imageName": "Liechtenstein.png",
    "myCode": "131",
    "isoCode": "LI"
  },
  "LK": {
    "name": "Sri Lanka",
    "imageName": "Sri-Lanka.png",
    "myCode": "132",
    "isoCode": "LK"
  },
  "LR": {
    "name": "Liberia",
    "imageName": "Liberia.png",
    "myCode": "133",
    "isoCode": "LR"
  },
  "LS": {
    "name": "Lesotho",
    "imageName": "Lesotho.png",
    "myCode": "134",
    "isoCode": "LS"
  },
  "LT": {
    "name": "Lithuania",
    "imageName": "Lithuania.png",
    "myCode": "135",
    "isoCode": "LT"
  },
  "LU": {
    "name": "Luxembourg",
    "imageName": "Luxembourg.png",
    "myCode": "136",
    "isoCode": "LU"
  },
  "LV": {
    "name": "Latvia",
    "imageName": "Latvia.png",
    "myCode": "137",
    "isoCode": "LV"
  },
  "LY": {
    "name": "Libya",
    "imageName": "Libya.png",
    "myCode": "138",
    "isoCode": "LY"
  },
  "MA": {
    "name": "Morocco",
    "imageName": "Morocco.png",
    "myCode": "139",
    "isoCode": "MA"
  },
  "MC": {
    "name": "Monaco",
    "imageName": "Monaco.png",
    "myCode": "140",
    "isoCode": "MC"
  },
  "MD": {
    "name": "Moldova, Republic of",
    "imageName": "Moldova.png",
    "myCode": "141",
    "isoCode": "MD"
  },
  "ME": {
    "name": "Montenegro",
    "imageName": "Montenegro.png",
    "myCode": "142",
    "isoCode": "ME"
  },
  "MF": {
    "name": "Saint Martin (French part)",
    "imageName": "Saint-Martin.png",
    "myCode": "",
    "isoCode": "MF"
  },
  "MG": {
    "name": "Madagascar",
    "imageName": "Madagascar.png",
    "myCode": "143",
    "isoCode": "MG"
  },
  "MH": {
    "name": "Marshall Islands",
    "imageName": "Marshall-Islands.png",
    "myCode": "144",
    "isoCode": "MH"
  },
  "MK": {
    "name": "North Macedonia",
    "imageName": "Macedonia.png",
    "myCode": "145",
    "isoCode": "MK"
  },
  "ML": {
    "name": "Mali",
    "imageName": "Mali.png",
    "myCode": "146",
    "isoCode": "ML"
  },
  "MM": {
    "name": "Myanmar",
    "imageName": "Myanmar.png",
    "myCode": "147",
    "isoCode": "MM"
  },
  "MN": {
    "name": "Mongolia",
    "imageName": "Mongolia.png",
    "myCode": "148",
    "isoCode": "MN"
  },
  "MO": {
    "name": "Macao",
    "imageName": "Macau.png",
    "myCode": "149",
    "isoCode": "MO"
  },
  "MP": {
    "name": "Northern Mariana Islands",
    "imageName": "Northern-Mariana-Islands.png",
    "myCode": "150",
    "isoCode": "MP"
  },
  "MQ": {
    "name": "Martinique",
    "imageName": "Martinique.png",
    "myCode": "151",
    "isoCode": "MQ"
  },
  "MR": {
    "name": "Mauritania",
    "imageName": "Mauritania.png",
    "myCode": "152",
    "isoCode": "MR"
  },
  "MS": {
    "name": "Montserrat",
    "imageName": "Montserrat.png",
    "myCode": "153",
    "isoCode": "MS"
  },
  "MT": {
    "name": "Malta",
    "imageName": "Malta.png",
    "myCode": "154",
    "isoCode": "MT"
  },
  "MU": {
    "name": "Mauritius",
    "imageName": "Mauritius.png",
    "myCode": "155",
    "isoCode": "MU"
  },
  "MV": {
    "name": "Maldives",
    "imageName": "Maldives.png",
    "myCode": "156",
    "isoCode": "MV"
  },
  "MW": {
    "name": "Malawi",
    "imageName": "Malawi.png",
    "myCode": "157",
    "isoCode": "MW"
  },
  "MX": {
    "name": "Mexico",
    "imageName": "Mexico.png",
    "myCode": "158",
    "isoCode": "MX"
  },
  "MY": {
    "name": "Malaysia",
    "imageName": "Malaysia.png",
    "myCode": "159",
    "isoCode": "MY"
  },
  "MZ": {
    "name": "Mozambique",
    "imageName": "Mozambique.png",
    "myCode": "160",
    "isoCode": "MZ"
  },
  "NA": {
    "name": "Namibia",
    "imageName": "Namibia.png",
    "myCode": "161",
    "isoCode": "NA"
  },
  "NC": {
    "name": "New Caledonia",
    "imageName": "New-Caledonia.png",
    "myCode": "162",
    "isoCode": "NC"
  },
  "NE": {
    "name": "Niger",
    "imageName": "Niger.png",
    "myCode": "163",
    "isoCode": "NE"
  },
  "NF": {
    "name": "Norfolk Island",
    "imageName": "Norfolk-Island.png",
    "myCode": "164",
    "isoCode": "NF"
  },
  "NG": {
    "name": "Nigeria",
    "imageName": "Nigeria.png",
    "myCode": "165",
    "isoCode": "NG"
  },
  "NI": {
    "name": "Nicaragua",
    "imageName": "Nicaragua.png",
    "myCode": "166",
    "isoCode": "NI"
  },
  "NL": {
    "name": "Netherlands",
    "imageName": "Netherlands.png",
    "myCode": "167",
    "isoCode": "NL"
  },
  "NO": {
    "name": "Norway",
    "imageName": "Norway.png",
    "myCode": "168",
    "isoCode": "NO"
  },
  "NP": {
    "name": "Nepal",
    "imageName": "Nepal.png",
    "myCode": "169",
    "isoCode": "NP"
  },
  "NR": {
    "name": "Nauru",
    "imageName": "Nauru.png",
    "myCode": "170",
    "isoCode": "NR"
  },
  "NU": {
    "name": "Niue",
    "imageName": "Niue.png",
    "myCode": "171",
    "isoCode": "NU"
  },
  "NZ": {
    "name": "New Zealand",
    "imageName": "New-Zealand.png",
    "myCode": "172",
    "isoCode": "NZ"
  },
  "OM": {
    "name": "Oman",
    "imageName": "Oman.png",
    "myCode": "174",
    "isoCode": "OM"
  },
  "$O1": {
    "name": "Other Country/Region",
    "imageName": "Unknown.png",
    "myCode": "",
    "isoCode": "$O1"
  },
  "PA": {
    "name": "Panama",
    "imageName": "Panama.png",
    "myCode": "175",
    "isoCode": "PA"
  },
  "PE": {
    "name": "Peru",
    "imageName": "Peru.png",
    "myCode": "176",
    "isoCode": "PE"
  },
  "PF": {
    "name": "French Polynesia",
    "imageName": "French-Polynesia.png",
    "myCode": "177",
    "isoCode": "PF"
  },
  "PG": {
    "name": "Papua New Guinea",
    "imageName": "Papua-New-Guinea.png",
    "myCode": "178",
    "isoCode": "PG"
  },
  "PH": {
    "name": "Philippines",
    "imageName": "Philippines.png",
    "myCode": "179",
    "isoCode": "PH"
  },
  "PK": {
    "name": "Pakistan",
    "imageName": "Pakistan.png",
    "myCode": "180",
    "isoCode": "PK"
  },
  "PL": {
    "name": "Poland",
    "imageName": "Poland.png",
    "myCode": "181",
    "isoCode": "PL"
  },
  "PM": {
    "name": "Saint Pierre and Miquelon",
    "imageName": "Saint-Pierre-and-Miquelon.png",
    "myCode": "",
    "isoCode": "PM"
  },
  "PR": {
    "name": "Puerto Rico",
    "imageName": "Puerto-Rico.png",
    "myCode": "184",
    "isoCode": "PR"
  },
  "PS": {
    "name": "Palestine, State of",
    "imageName": "Palestine.png",
    "myCode": "185",
    "isoCode": "PS"
  },
  "PT": {
    "name": "Portugal",
    "imageName": "Portugal.png",
    "myCode": "186",
    "isoCode": "PT"
  },
  "PW": {
    "name": "Palau",
    "imageName": "Palau.png",
    "myCode": "187",
    "isoCode": "PW"
  },
  "PY": {
    "name": "Paraguay",
    "imageName": "Paraguay.png",
    "myCode": "188",
    "isoCode": "PY"
  },
  "QA": {
    "name": "Qatar",
    "imageName": "Qatar.png",
    "myCode": "189",
    "isoCode": "QA"
  },
  "RE": {
    "name": "Réunion",
    "imageName": "Reunion.png",
    "myCode": "190",
    "isoCode": "RE"
  },
  "RO": {
    "name": "Romania",
    "imageName": "Romania.png",
    "myCode": "191",
    "isoCode": "RO"
  },
  "RS": {
    "name": "Serbia",
    "imageName": "Serbia.png",
    "myCode": "192",
    "isoCode": "RS"
  },
  "RU": {
    "name": "Russian Federation",
    "imageName": "Russia.png",
    "myCode": "193",
    "isoCode": "RU"
  },
  "RW": {
    "name": "Rwanda",
    "imageName": "Rwanda.png",
    "myCode": "194",
    "isoCode": "RW"
  },
  "SA": {
    "name": "Saudi Arabia",
    "imageName": "Saudi-Arabia.png",
    "myCode": "195",
    "isoCode": "SA"
  },
  "SB": {
    "name": "Solomon Islands",
    "imageName": "Solomon-Islands.png",
    "myCode": "196",
    "isoCode": "SB"
  },
  "SC": {
    "name": "Seychelles",
    "imageName": "Seychelles.png",
    "myCode": "197",
    "isoCode": "SC"
  },
  "SD": {
    "name": "Sudan",
    "imageName": "Sudan.png",
    "myCode": "198",
    "isoCode": "SD"
  },
  "SE": {
    "name": "Sweden",
    "imageName": "Sweden.png",
    "myCode": "199",
    "isoCode": "SE"
  },
  "SG": {
    "name": "Singapore",
    "imageName": "Singapore.png",
    "myCode": "200",
    "isoCode": "SG"
  },
  "SH": {
    "name": "Saint Helena, Ascension and Tristan da Cunha",
    "imageName": "Saint-Helena.png",
    "myCode": "201",
    "isoCode": "SH"
  },
  "SI": {
    "name": "Slovenia",
    "imageName": "Slovenia.png",
    "myCode": "202",
    "isoCode": "SI"
  },
  "SJ": {
    "name": "Svalbard and Jan Mayen",
    "imageName": "Svalbard-and-Jan-Mayen.png",
    "myCode": "",
    "isoCode": "SJ"
  },
  "SK": {
    "name": "Slovakia",
    "imageName": "Slovakia.png",
    "myCode": "204",
    "isoCode": "SK"
  },
  "SL": {
    "name": "Sierra Leone",
    "imageName": "Sierra-Leone.png",
    "myCode": "205",
    "isoCode": "SL"
  },
  "SM": {
    "name": "San Marino",
    "imageName": "San-Marino.png",
    "myCode": "206",
    "isoCode": "SM"
  },
  "SN": {
    "name": "Senegal",
    "imageName": "Senegal.png",
    "myCode": "207",
    "isoCode": "SN"
  },
  "SO": {
    "name": "Somalia",
    "imageName": "Somalia.png",
    "myCode": "208",
    "isoCode": "SO"
  },
  "SR": {
    "name": "Suriname",
    "imageName": "Suriname.png",
    "myCode": "209",
    "isoCode": "SR"
  },
  "SS": {
    "name": "South Sudan",
    "imageName": "South-Sudan.png",
    "myCode": "",
    "isoCode": "SS"
  },
  "ST": {
    "name": "Sao Tome and Principe",
    "imageName": "Sao-Tome-and-Principe.png",
    "myCode": "210",
    "isoCode": "ST"
  },
  "SV": {
    "name": "El Salvador",
    "imageName": "El-Salvador.png",
    "myCode": "211",
    "isoCode": "SV"
  },
  "SY": {
    "name": "Syrian Arab Republic",
    "imageName": "Syria.png",
    "myCode": "212",
    "isoCode": "SY"
  },
  "SZ": {
    "name": "Eswatini",
    "imageName": "Swaziland.png",
    "myCode": "213",
    "isoCode": "SZ"
  },
  "TC": {
    "name": "Turks and Caicos Islands",
    "imageName": "Turks-and-Caicos-Islands.png",
    "myCode": "214",
    "isoCode": "TC"
  },
  "TD": {
    "name": "Chad",
    "imageName": "Chad.png",
    "myCode": "215",
    "isoCode": "TD"
  },
  "TF": {
    "name": "French Southern Territories",
    "imageName": "French-Southern-Territories.png",
    "myCode": "216",
    "isoCode": "TF"
  },
  "TG": {
    "name": "Togo",
    "imageName": "Togo.png",
    "myCode": "217",
    "isoCode": "TG"
  },
  "TH": {
    "name": "Thailand",
    "imageName": "Thailand.png",
    "myCode": "218",
    "isoCode": "TH"
  },
  "TJ": {
    "name": "Tajikistan",
    "imageName": "Tajikistan.png",
    "myCode": "219",
    "isoCode": "TJ"
  },
  "TK": {
    "name": "Tokelau",
    "imageName": "Tokelau.png",
    "myCode": "220",
    "isoCode": "TK"
  },
  "TL": {
    "name": "Timor-Leste",
    "imageName": "Timor-Leste.png",
    "myCode": "",
    "isoCode": "TL"
  },
  "TM": {
    "name": "Turkmenistan",
    "imageName": "Turkmenistan.png",
    "myCode": "222",
    "isoCode": "TM"
  },
  "TN": {
    "name": "Tunisia",
    "imageName": "Tunisia.png",
    "myCode": "223",
    "isoCode": "TN"
  },
  "TO": {
    "name": "Tonga",
    "imageName": "Tonga.png",
    "myCode": "224",
    "isoCode": "TO"
  },
  "TR": {
    "name": "Turkey",
    "imageName": "Turkey.png",
    "myCode": "225",
    "isoCode": "TR"
  },
  "TT": {
    "name": "Trinidad and Tobago",
    "imageName": "Trinidad-and-Tobago.png",
    "myCode": "226",
    "isoCode": "TT"
  },
  "TV": {
    "name": "Tuvalu",
    "imageName": "Tuvalu.png",
    "myCode": "227",
    "isoCode": "TV"
  },
  "TW": {
    "name": "Taiwan, Province of China",
    "imageName": "Taiwan.png",
    "myCode": "228",
    "isoCode": "TW"
  },
  "TZ": {
    "name": "Tanzania, United Republic of",
    "imageName": "Tanzania.png",
    "myCode": "229",
    "isoCode": "TZ"
  },
  "UA": {
    "name": "Ukraine",
    "imageName": "Ukraine.png",
    "myCode": "230",
    "isoCode": "UA"
  },
  "UG": {
    "name": "Uganda",
    "imageName": "Uganda.png",
    "myCode": "231",
    "isoCode": "UG"
  },
  "UM": {
    "name": "United States Minor Outlying Islands",
    "imageName": "United-States-Minor-Outlying-Island.png",
    "myCode": "",
    "isoCode": "UM"
  },
  "US": {
    "name": "United States of America",
    "imageName": "United-States.png",
    "myCode": "233",
    "isoCode": "US"
  },
  "UY": {
    "name": "Uruguay",
    "imageName": "Uruguay.png",
    "myCode": "234",
    "isoCode": "UY"
  },
  "UZ": {
    "name": "Uzbekistan",
    "imageName": "Uzbekistan.png",
    "myCode": "235",
    "isoCode": "UZ"
  },
  "VA": {
    "name": "Holy See",
    "imageName": "Vatican-City.png",
    "myCode": "236",
    "isoCode": "VA"
  },
  "VC": {
    "name": "Saint Vincent and the Grenadines",
    "imageName": "Saint-Vincent-and-the-Grenadines.png",
    "myCode": "237",
    "isoCode": "VC"
  },
  "VE": {
    "name": "Venezuela (Bolivarian Republic of)",
    "imageName": "Venezuela.png",
    "myCode": "238",
    "isoCode": "VE"
  },
  "VG": {
    "name": "Virgin Islands (British)",
    "imageName": "British-Virgin-Islands.png",
    "myCode": "239",
    "isoCode": "VG"
  },
  "VI": {
    "name": "Virgin Islands (U.S.)",
    "imageName": "US-Virgin-Islands.png",
    "myCode": "240",
    "isoCode": "VI"
  },
  "VN": {
    "name": "Viet Nam",
    "imageName": "Vietnam.png",
    "myCode": "241",
    "isoCode": "VN"
  },
  "VU": {
    "name": "Vanuatu",
    "imageName": "Vanuatu.png",
    "myCode": "242",
    "isoCode": "VU"
  },
  "WF": {
    "name": "Wallis and Futuna",
    "imageName": "Wallis-And-Futuna.png",
    "myCode": "243",
    "isoCode": "WF"
  },
  "WS": {
    "name": "Samoa",
    "imageName": "Samoa.png",
    "myCode": "244",
    "isoCode": "WS"
  },
  "YE": {
    "name": "Yemen",
    "imageName": "Yemen.png",
    "myCode": "245",
    "isoCode": "YE"
  },
  "YT": {
    "name": "Mayotte",
    "imageName": "Mayotte.png",
    "myCode": "246",
    "isoCode": "YT"
  },
  "ZA": {
    "name": "South Africa",
    "imageName": "South-Africa.png",
    "myCode": "247",
    "isoCode": "ZA"
  },
  "ZM": {
    "name": "Zambia",
    "imageName": "Zambia.png",
    "myCode": "248",
    "isoCode": "ZM"
  },
  "ZW": {
    "name": "Zimbabwe",
    "imageName": "Zimbabwe.png",
    "myCode": "249",
    "isoCode": "ZW"
  },
  "SX": {
    "name": "Sint Maarten (Dutch part)",
    "imageName": "Saint-Martin.png",
    "myCode": "250",
    "isoCode": "SX"
  },
  "PN": {
    "name": "Pitcairn",
    "imageName": "Pitcairn-Islands.png",
    "myCode": "251",
    "isoCode": "PN"
  },
  "CW": {
    "name": "Curaçao",
    "imageName": "Curacao.png",
    "myCode": "253",
    "isoCode": "CW"
  }
};
export default data;
