export interface SliderCategoryOption {
  id: string;
  label: string;
}

export type SliderChangeSrcOperation = 'drag' | 'step';
