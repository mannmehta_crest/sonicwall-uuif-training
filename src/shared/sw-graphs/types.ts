export interface NodesConfiguration {
  id: string;
  title: string;
  enlargementFactor: number;
  fontFamily: string;
  titleFontColor: string;
  fontIcon: string;
  fontSize: number;
  iconColor: string;
  data: any;
}
export interface LinkConfiguration {
  id: string;
  from: string;
  to: string;
  linkColor: string;
  linkStyle: string;
  linkWidth: number;
  arrowheadTo: boolean;
  arrowHeadFrom: boolean;
  data: any;
}
export interface ChartConfiguration {
  arrows: any;
  backColor: string;
  maxZoom: number;
  minZoom: number;
  iconFontFamily: string;
  selectionColor: string;
  navigation: boolean;
  selfLinks: boolean;
  selectedFontColor: string;
}
export interface PingConfiguration {
  pingColor: string;
  pingRadius: number;
  pingWidth: number;
  pingTimes: number;
  pingRepeat: number;
  pingLinkAnimEffectWidth: number;
}
export interface AnimationConfig {
  animate: boolean;
  time: number;
}
export interface ZoomOption {
  animate: boolean;
  time: number;
  ids?: string |string[];
}
/* get thte type of easing and the type of level, orinetation, spacing */
export interface LayoutOptions {
  animate?: boolean;
  consistent?: boolean;
  easing?: EaseType;
  fit?: boolean;
  fixed?: string[];
  flatten?: boolean;
  level?: string;
  orientation?: Orientation;
  straighten?: boolean;
  tidy?: boolean;
  time?: number;
  top?: string|string[];
  tightness?: number;
}
export interface FilterEventParams {
  filterFunction: Function;
  knowResult: boolean;
}
export interface PingEventParams {
  pingOptions: PingConfiguration;
  pingNodes: string[];
}
export type EaseType = 'linear' | 'cubicle';
export type Orientation = 'down' | 'right' |'left' |'up';
export type SeelctionMode = 'drag' | 'select';
export type LayoutMode = 'lens'|'tweak' |'structural' | 'standard' | 'hierarchy' |'radial';
