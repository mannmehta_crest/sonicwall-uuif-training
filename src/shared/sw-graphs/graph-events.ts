import Vue from 'vue';
export const GRAPH_EVENT = 'graph';
export const GRAPH_FILTER_EVENT = 'onFilter';
export const FILTER_COMPLETE = 'onFilterComplete';
export const GRAPH_PING_EVENT = 'onPing';
export const GRAPH_DATAURL_EVENT = 'onDataURL';
export const GRAPH_ARRANGE_EVENT = 'onArrange';
export const GRAPH_GET_EVENT = 'onGetItem';

export const graphEventBus = new Vue();
