export interface OverflowEvent {
  axis: 'x' | 'y';
  overflow: boolean;
}
