import { Subject } from 'rxjs';
import { IconConfig } from '../sw-icon';

export type MyClientRect = Pick<ClientRect, 'top' | 'left' | 'right' | 'bottom' | 'width' | 'height'>;

export interface HighlightReferrerInfo {
  navId: string;
  boundingRect: MyClientRect;
}

export type ExpandedGroupRegistry = string[][];

export interface ProvidedNavParam {
  eventbus: Subject<string>;
  accordion: boolean;
  theme: string;
  selectedHierarchy: string[];
  expandedGroups: ExpandedGroupRegistry;
  collapsed: boolean;
  navColor: string;
  size: 'default' | 'compact';
}

export interface TopNavItem {
  id: string;
  icon?: IconConfig;
  label?: string;
  disabled?: boolean;
  divider?: boolean;
}

export interface ProvidedTopNavParam {
  eventbus: Subject<string>;
  mode: 'expand' | 'dropdown';
}
