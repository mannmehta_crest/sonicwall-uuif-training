export type TransitionHook = (el: HTMLElement) => void;

export interface TransitionHookSet {
  'before-enter': TransitionHook;
  'enter': TransitionHook;
  'after-enter': TransitionHook;
  'before-leave': TransitionHook;
  'leave': TransitionHook;
  'after-leave': TransitionHook;
}

function mergedHooks(hooks: TransitionHookSet, userHooks: Partial<TransitionHookSet>): TransitionHookSet {
  return {
    'before-enter': (el) => {
      hooks['before-enter'](el);
      if (typeof userHooks['before-enter'] === 'function') {
        userHooks['before-enter']!(el);
      }
    },
    enter: (el) => {
      hooks.enter(el);
      if (typeof userHooks.enter === 'function') {
        userHooks.enter(el);
      }
    },
    'after-enter': (el) => {
      hooks['after-enter'](el);
      if (typeof userHooks['after-enter'] === 'function') {
        userHooks['after-enter']!(el);
      }
    },
    'before-leave': (el) => {
      hooks['before-leave'](el);
      if (typeof userHooks['before-leave'] === 'function') {
        userHooks['before-leave']!(el);
      }
    },
    leave: (el) => {
      hooks.leave(el);
      if (typeof userHooks.leave === 'function') {
        userHooks.leave(el);
      }
    },
    'after-leave': (el) => {
      hooks['after-leave'](el);
      if (typeof userHooks['after-leave'] === 'function') {
        userHooks['after-leave']!(el);
      }
    },
  };
}

export default {
  mergedHooks,
};
