import SwTitlePane from './sw-title-pane.vue';
import SwTitlePaneGroup from './sw-title-pane-group.vue';

export{
  SwTitlePane,
  SwTitlePaneGroup,
};
