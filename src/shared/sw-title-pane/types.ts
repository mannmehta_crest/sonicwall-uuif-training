export type TitlePaneTheme = 'light' | 'light-grey' | 'dark' | 'grey';

export interface ProvidedPaneParam {
  dragPaneId: string;
  panesInfo: PaneItem[];
}

export interface PaneItem {
  paneId: string;
  ele: HTMLElement | null;
  theme: TitlePaneTheme;
  height: number;
  top: number;
}
