import { VueRenderFunction } from '../common-types';

type DropMenuItemContent = string | VueRenderFunction;

export interface DropMenuItem {
  id: string;
  content?: DropMenuItemContent;
  disabled?: boolean;
  clickable?: boolean;
  divider?: boolean;
}
