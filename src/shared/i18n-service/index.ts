import Vue from 'vue';
import VueI18n from 'vue-i18n';

import { I18nLangExport } from '../../assets/i18n/types';
import { lang as defaultLang } from '@/assets/i18n/locales/en-US.lang';

/**
 * Set up localization whenever import this service.
 * Language files will be loaded from other places.
 */

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: defaultLang.localeName,
  fallbackLocale: defaultLang.localeName,
  messages: {
    [defaultLang.localeName]: defaultLang.messageObj,
  },
  /** FIXME: keep these warnings silent for now */
  silentFallbackWarn: true,
  silentTranslationWarn: true,
});

/**
 * Change the active language to use the specified one.
 * This may involve loading language file from backend and set it into the
 * i18n engine.
 */
function changeLang(localeName: string): Promise<boolean> {
  if (!!i18n.messages[localeName]) {
    return Promise.resolve()
      .then(() => {
        i18n.locale = localeName;
        return true;
      });
  }
  /** since the requested language message is not there, we have to load it from backend */
  return import(
    /* webpackChunkName: "lang-[request]" */
    /* webpackInclude: /\.lang\.ts$/ */
    `@/assets/i18n/locales/${localeName}.lang` // tslint:disable-line
  )
    .then((langModule: { lang: I18nLangExport; }) => {
      i18n.setLocaleMessage(langModule.lang.localeName, langModule.lang.messageObj);
      i18n.locale = localeName;
      return true;
    })
    .catch((error: any) => {
      console.error(`Failed to load language file dynamically: ${localeName}`);
      throw error;
    });
}

export const i18nService = {
  i18n,
  changeLang,
  defaultLocaleName: defaultLang.localeName,
};
