/** An FlexLayoutUnit could be a flex container and/or flex item */
export interface FlexLayoutUnit {
  id: string;
  className?: string;
  container?: FlexLayoutContainer;
  item?: FlexLayoutItem;
}

interface FlexLayoutContainer {
  /** @default 'row' */
  direction?: 'row' | 'column';
  /** @default false */
  wrap?: boolean;
  /** @default 'start' */
  justifyContent?: 'start' | 'end' | 'center' | 'between' | 'around' | 'evenly';
  /** @default 'stretch' */
  alignItems?: 'start' | 'end' | 'center' | 'stretch' | 'baseline';
  /** @default 'start' */
  alignContent?: 'start' | 'end' | 'center' | 'stretch' | 'between' | 'around';

  items: FlexLayoutUnit[];
}

interface FlexLayoutItem {
  /** @default 0 */
  order?: number;
  /** @default true */
  flex?: boolean;
  explicitFlex?: {
    grow?: number;
    shrink?: number;
    basis?: string;
  };
}
