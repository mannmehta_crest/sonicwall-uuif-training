export interface DiscreetProgressValue {
  color: string;
  value: number;
}
