export * from './generate-directive';

import { generateDirective } from './generate-directive';

export const popperService = {
  generateDirective,
};
