import Vue, { VNode, DirectiveOptions, CreateElement, VNodeDirective } from 'vue';
import { CombinedVueInstance } from 'vue/types/vue';
import VueRouter from 'vue-router';
import { Store } from 'vuex';
import Popper, { Placement } from 'popper.js';
import _, { Cancelable } from 'lodash';
import { domService } from '../dom-service';
import { utilService, SlotId } from '../util-service';
import { NonPartial, Omit } from '../common-types';
import { SwExtendedVue, SwExtendedVueShim } from '../sw-extended-vue';
import { i18nService } from '../i18n-service';

export interface PopperGenDirConfig<ValuePropName, Values, ModifierPropName> {
  dirId: string;
  component: typeof SwExtendedVueShim | typeof SwExtendedVue;
  componentPropNames: ValuePropName[];
  valueDefault: Values;
  modifierNames?: ModifierPropName[];
  visibleTransitionName?: string;
}

type PopperContentRenderFunction = (h: CreateElement) => (VNode | VNode[]);
interface PopperComponentData {
  content: string | PopperContentRenderFunction | undefined;
}

/** Basically these values are not reactive!!!
 * `visible` is controllable value.
 */
export interface PopperDirValues extends PopperComponentData {
  enabled: boolean; // master control of enabling/disabling the popper
  doAnimation: boolean;
  persistent: boolean;
  placement: Placement;
  trigger: 'hover' | 'controlled';
  visible: boolean; // valid only if `tigger === controlled`
  includePopperForHover: boolean; // valid only if `trigger === hover`, to determine if hovering on popper itself counts as hover
  delayForHover: number; // valid only if `trigger === hover`, delay milliseconds to show the popper
  onClickOutsidePopper: PopperClickOutsideCallbackFunc | null;
  parentComponentInstance: SwExtendedVue | undefined; // by nature, this is not reactive
}
type PopperClickOutsideCallbackFunc = (event: MouseEvent) => void;

export interface PopperDirModifiers {
  noTransition: boolean;
}

type ExtractDirValuePropName<DV> = keyof (Omit<DV, keyof PopperDirValues>);
type ExtractDirModifierPropName<DM> = keyof (Omit<DM, keyof PopperDirModifiers>);

const datasetKeyPrefix = 'popperDirData';

/**
 * DirModifiers are those options that would only take effect at the creation.
 */
export function generateDirective<
  DirValues extends Partial<PopperDirValues>,
  DirModifiers extends Partial<PopperDirModifiers>
>(config: PopperGenDirConfig<ExtractDirValuePropName<DirValues>, DirValues, ExtractDirModifierPropName<DirModifiers>>) {
  /**
   * Define all the internal types
   */
  type PopperVueInstance = CombinedVueInstance<Vue, WrapperVueData, WrapperVueComputed, {}, {}>;
  type DirOptions = DirValues & DirModifiers;
  type FullDirOptions = NonPartial<DirOptions>;
  type DirValuePropName = ExtractDirValuePropName<DirValues>;
  type DirCompProps = { [P in DirValuePropName]: DirValues[P] };

  interface DirContext {
    router?: VueRouter;
    store?: Store<any>;
    delayedShowing?: (() => void) & Cancelable;
    delayedHiding?: ((() => void) & Cancelable);
    captureClick?: (event: MouseEvent) => void;
  }

  interface DirStore {
    popper: Popper;
    instance: PopperVueInstance;
    options: FullDirOptions;
    context: DirContext;
  }

  type WrapperVueInternalData = PopperComponentData & {
    isVisible__: boolean;
  };
  type WrapperVueData = DirCompProps & WrapperVueInternalData;

  interface WrapperVueComputed {}

  /** dataset key used for this directive */
  const effectiveDatasetKey = `${datasetKeyPrefix}_${config.dirId}`;

  /**
   * Set default config
   */
  const effectiveConfig: NonPartial<typeof config> = Object.assign(
    {
      dirId: '',
      component: Vue as any,
      componentPropNames: [],
      valueDefault: {} as NonNullable<DirValues>,
      modifierNames: [],
      visibleTransitionName: 'sw-fade',
    } as NonPartial<typeof config>,
    config,
  );

  /**
   * Define all internal functions
   */
  function setupPopper(refEl: HTMLElement, slotId: SlotId, force: boolean = false): void {
    const options = utilService.memStore.get<DirStore, 'options'>(slotId, 'options');
    const context = utilService.memStore.get<DirStore, 'context'>(slotId, 'context');
    if (!options || !context) { return; }

    let vueInstance: PopperVueInstance | undefined = undefined;

    if (!force) {
      /** we will check if there is existing popper that can be reused */
      vueInstance = utilService.memStore.get<DirStore, 'instance'>(slotId, 'instance');
      if (vueInstance) {
        vueInstance.isVisible__ = true;
        return;
      }
    }

    /** first get the component DOM built */
    vueInstance = new Vue({
      data: (() => {
        const obj = {} as WrapperVueData;
        effectiveConfig.componentPropNames.forEach(name => ((obj as DirCompProps)[name] = options[name]));
        obj.content = options.content;
        obj.isVisible__ = true;
        return obj;
      })(),
      i18n: i18nService.i18n,
      methods: {
        onAfterLeave() {
          destroyPopper(slotId, true);
        },
        onAfterEnter(el: HTMLElement) {
          /** temporary fix */
          el.classList.remove(`${effectiveConfig.visibleTransitionName}-leave-to`);
        },
        onEnterPopper() {
          if (options.trigger === 'hover' && options.includePopperForHover) {
            showPopper(true, refEl, slotId);
          }
        },
        onLeavePopper() {
          if (options.trigger === 'hover') {
            showPopper(false, refEl, slotId);
          }
        },
      },
      render(h): VNode {/* tslint:disable-line */
        const PopperComponent: any = effectiveConfig.component;/* tslint:disable-line */

        const props: WrapperVueData = {} as WrapperVueData;
        effectiveConfig.componentPropNames.forEach(name => (props[name] = this[name]));

        let contentNode: any = null;
        const self = this as WrapperVueInternalData;
        if (typeof self.content === 'string') {
          contentNode = self.content;
        } else if (typeof self.content === 'function') {
          contentNode = self.content(h);
        }

        const compNode = (
          <PopperComponent
            {...{ props }}
            v-show={this.isVisible__}
            nativeOn-mouseenter={this.onEnterPopper}
            nativeOn-mouseleave={this.onLeavePopper}
          >{contentNode}</PopperComponent>
        );

        const rootNode = !!options.doAnimation ? (
          <transition
            name={effectiveConfig.visibleTransitionName}
            onAfterLeave={this.onAfterLeave}
            onAfterEnter={this.onAfterEnter}
            appear={true}
          >{ compNode }</transition>
        ) : compNode;
        return rootNode as VNode;
      },
      updated() {
        if (this.isVisible__) { setTimeout(popper.update, 0); }
      },
      router: context ? context.router : undefined,
      store: context ? context.store : undefined,
      parent: options.parentComponentInstance,
    });

    vueInstance.$mount();
    document.body.appendChild(vueInstance.$el);
    utilService.memStore.set<DirStore, 'instance'>(slotId, 'instance', vueInstance);

    /** monitor clicking outside the popper if needed */
    if (options.onClickOutsidePopper) {
      const cb = options.onClickOutsidePopper;
      context.captureClick = (event: MouseEvent) => {
        onCaptureClick(event, vueInstance!.$el as HTMLElement, refEl, cb);
      };
      document.addEventListener('click', context.captureClick, { capture: true });
    }

    /** then get the Popper to manage the component */
    const popper = new Popper(refEl, vueInstance.$el, {
      placement: options.placement,
      onCreate() {
        vueInstance!.$nextTick(() => {
          setTimeout(popper.scheduleUpdate, 0);
        });
      },
      modifiers: {
        preventOverflow: {
          boundariesElement: 'window',
        },
        keepTogether: {
          enabled: true,
        },
        flip: {
          boundariesElement: 'viewport',
        },
        hide: {
          enabled: false,
        },
      },
    });
    utilService.memStore.set<DirStore, 'popper'>(slotId, 'popper', popper);
  }

  function destroyPopper(slotId: SlotId, force: boolean = false) {
    const slotObj = utilService.memStore.getSlot<DirStore>(slotId);
    if (!slotObj) { return; }

    if (!force) {
      /** we will check if there is existing popper that can be reused */
      const vueInstance = slotObj.instance;
      if (vueInstance) {
        vueInstance.isVisible__ = false;
        return;
      }
    }

    if (slotObj.context && slotObj.context.captureClick) {
      document.removeEventListener('click', slotObj.context.captureClick, { capture: true });
      delete slotObj.context.captureClick;
    }

    if (slotObj.popper) {
      slotObj.popper.destroy();
      delete slotObj.popper;
    }
    if (slotObj.instance) {
      document.body.removeChild(slotObj.instance.$el);
      slotObj.instance.$destroy();
      delete slotObj.instance;
    }
  }

  function showPopper(isShowing: boolean, refEl: HTMLElement, slotId: SlotId): void {
    const context = utilService.memStore.get<DirStore, 'context'>(slotId, 'context');
    const options = utilService.memStore.get<DirStore, 'options'>(slotId, 'options');
    if (!context || !options) { return; }

    if (options.persistent) { return; }

    if (isShowing) {
      if (context.delayedHiding) {
        context.delayedHiding.cancel();
        context.delayedHiding = undefined;
      }
      /**
       * Delay to show the popper usually for better performance if required
       */
      if (options.delayForHover !== undefined && options.delayForHover > 0) {
        if (!context.delayedShowing) {
          context.delayedShowing = _.debounce(
            () => {
              setupPopper(refEl, slotId);
            },
            options.delayForHover,
          );
        }
        context.delayedShowing();
      } else {
        setupPopper(refEl, slotId);
      }
    } else {
      if (context.delayedShowing) {
        context.delayedShowing.cancel();
        context.delayedShowing = undefined;
      }
      /** for hiding, we want to delay a little bit the execution as there could be chances
       * when right after trying to hide, we want to show it again, so that we could cancel
       * the previous hiding action.
       */
      if (!context.delayedHiding) {
        context.delayedHiding = _.debounce(
          () => {
            context.delayedHiding = undefined;
            destroyPopper(slotId, !options.doAnimation);
          },
          32,
        );
      }
      context.delayedHiding();
    }
  }

  /** This function has to be of high performance */
  function syncOptions(slotId: SlotId, dirValues: DirValues, dirModifiers: DirModifiers) {
    const oldOptions = utilService.memStore.get<DirStore, 'options'>(slotId, 'options');
    const newOptions: FullDirOptions = {
      enabled: (dirValues.enabled !== undefined) ? dirValues.enabled :
        ((effectiveConfig.valueDefault.enabled !== undefined) ? effectiveConfig.valueDefault.enabled : true),
      doAnimation: (dirValues.doAnimation !== undefined) ? dirValues.doAnimation :
        ((effectiveConfig.valueDefault.doAnimation !== undefined) ? effectiveConfig.valueDefault.doAnimation : true),
      persistent: (dirValues.persistent !== undefined) ? dirValues.persistent :
        ((effectiveConfig.valueDefault.persistent !== undefined) ? effectiveConfig.valueDefault.persistent : false),
      placement: dirValues.placement || effectiveConfig.valueDefault.placement || 'bottom',
      trigger: dirValues.trigger || effectiveConfig.valueDefault.trigger || 'hover',
      visible: (dirValues.visible !== undefined) ? dirValues.visible :
        ((effectiveConfig.valueDefault.visible !== undefined) ? effectiveConfig.valueDefault.visible : false),
      includePopperForHover: (dirValues.includePopperForHover !== undefined) ? dirValues.includePopperForHover :
        ((effectiveConfig.valueDefault.includePopperForHover !== undefined) ? effectiveConfig.valueDefault.includePopperForHover : true),
      delayForHover: (dirValues.delayForHover !== undefined) ? dirValues.delayForHover :
        ((effectiveConfig.valueDefault.delayForHover !== undefined) ? effectiveConfig.valueDefault.delayForHover : 0),
      content: dirValues.content || effectiveConfig.valueDefault.content || 'No Contents?',
      onClickOutsidePopper: dirValues.onClickOutsidePopper || effectiveConfig.valueDefault.onClickOutsidePopper || null,
      noTransition: dirModifiers.noTransition || false,
      parentComponentInstance: dirValues.parentComponentInstance,
    } as FullDirOptions;
    /** sync from values */
    effectiveConfig.componentPropNames.forEach((name) => {
      (newOptions as any)[name] = dirValues[name] !== undefined ? dirValues[name] : effectiveConfig.valueDefault[name]!;
    });
    /** sync from modifiers */
    effectiveConfig.modifierNames!.forEach((name) => {
      (newOptions as any)[name] = dirModifiers[name] || false;
    });
    /** keep a few of options fixed */
    if (oldOptions) {
      Object.assign(newOptions, {
        placement: oldOptions.placement,
        trigger: oldOptions.trigger,
        noTransition: oldOptions.noTransition,
      } as FullDirOptions);
    }
    utilService.memStore.set<DirStore, 'options'>(slotId, 'options', newOptions);

    const instance = utilService.memStore.get<DirStore, 'instance'>(slotId, 'instance');
    if (!instance) { return; }

    /** update these new options to the instance */
    instance.content = newOptions.content;
    effectiveConfig.componentPropNames.forEach((name) => {
      (instance as DirCompProps)[name] = newOptions[name];
    });
  }

  function initContext(slotId: SlotId, node: VNode) {
    let context: DirContext = {};

    if (node.componentInstance) {
      context = {
        router: node.componentInstance.$router,
        store: node.componentInstance.$store,
      };
    } else if (node.context) {
      context = {
        router: node.context.$router,
        store: node.context.$store,
      };
    } else {
      console.warn('Cannot deduce the router and store for vue instance!');
      return;
    }

    utilService.memStore.set<DirStore, 'context'>(slotId, 'context', context);
  }

  function setupTriggerEvents(el: HTMLElement, trigger: PopperDirValues['trigger']) {
    if (trigger === 'hover') {
      el.addEventListener('mouseenter', onEnterTarget);
      el.addEventListener('mouseleave', onLeaveTarget);
    }
  }

  function tearDownTriggerEvents(el: HTMLElement) {
    el.removeEventListener('mouseenter', onEnterTarget);
    el.removeEventListener('mouseleave', onLeaveTarget);
  }

  function onEnterTarget(event: MouseEvent): void {
    const slotId: string = domService.dataset.get(event.currentTarget as HTMLElement, effectiveDatasetKey);
    if (!slotId) {
      console.error('The slotId for sharing data of directive on element is missing!');
      return;
    }

    showPopper(true, event.currentTarget as HTMLElement, slotId);
  }

  function onLeaveTarget(event: MouseEvent): void {
    const slotId: string = domService.dataset.get(event.currentTarget as HTMLElement, effectiveDatasetKey);
    if (!slotId) {
      console.error('The slotId for sharing data of directive on element is missing!');
      return;
    }

    showPopper(false, event.currentTarget as HTMLElement, slotId);
  }

  function onCaptureClick(event: MouseEvent, popperEl: HTMLElement, refEl: HTMLElement, cb: PopperClickOutsideCallbackFunc) {
    if (popperEl.contains(event.target as Node) || refEl.contains(event.target as Node)) { return; }
    cb(event);
  }

  function performBindInitialization(el: HTMLElement, binding: VNodeDirective, vnode: VNode) {
    const oldSlotId: SlotId = domService.dataset.get(el, effectiveDatasetKey);
    if (oldSlotId) { performUnbindDestroy(el); }

    const slotId = utilService.memStore.createSlot();
    domService.dataset.set(el, effectiveDatasetKey, slotId);

    syncOptions(slotId, binding.value || {}, binding.modifiers as any);

    /** set context */
    initContext(slotId, vnode);
  }

  function performInsertedSetup(el: HTMLElement, slotId: string) {
    const options = utilService.memStore.get<DirStore, 'options'>(slotId, 'options');
    if (!options) { return; }

    if (options.persistent || (options.trigger === 'controlled' && options.visible)) {
      destroyPopper(slotId, true);
      setupPopper(el, slotId, true);
    }

    setupTriggerEvents(el, options.trigger!);
  }

  function performComponentUpdated(el: HTMLElement, binding: VNodeDirective) {
    const slotId: string = domService.dataset.get(el, effectiveDatasetKey);
    if (!slotId) {
      console.error('The slotId for sharing data of directive on element is missing!');
      return;
    }

    syncOptions(slotId, binding.value || {}, binding.modifiers as any);

    const options = utilService.memStore.get<DirStore, 'options'>(slotId, 'options');
    if (!options) { return; }

    if (options.trigger === 'controlled') {
      showPopper(options.visible!, el, slotId);
    }
  }

  function performUnbindDestroy(el: HTMLElement) {
    tearDownTriggerEvents(el);

    const slotId: SlotId | undefined = domService.dataset.get(el, effectiveDatasetKey);
    domService.dataset.unset(el, effectiveDatasetKey);
    if (slotId) {
      destroyPopper(slotId, true);
      utilService.memStore.destroySlot(slotId);
    }
  }

  /**
   * The real Directive starts here...
   */
  const directive: DirectiveOptions = {
    bind(el, binding, vnode) {
      const dirValues: DirValues = binding.value;
      if (dirValues.enabled === false) { return; }
      performBindInitialization(el, binding, vnode);
    },
    inserted(el) {
      const slotId: string = domService.dataset.get(el, effectiveDatasetKey);
      if (!slotId) {
        // console.error('The slotId for sharing data of directive on element is missing!');
        return;
      }
      performInsertedSetup(el, slotId);
    },
    componentUpdated(el, binding, vnode) {
      const dirValues: DirValues = binding.value;
      let slotId: string = domService.dataset.get(el, effectiveDatasetKey);

      if (dirValues.enabled === false) {
        if (slotId) {
          performUnbindDestroy(el);
          return;
        }
      } else {
        if (!slotId) {
          performBindInitialization(el, binding, vnode);
          slotId = domService.dataset.get(el, effectiveDatasetKey);
          if (!slotId) {
            console.error('The slotId for sharing data of directive on element is missing!');
            return;
          }
          performInsertedSetup(el, slotId);
        }
        performComponentUpdated(el, binding);
      }
    },
    unbind(el) {
      performUnbindDestroy(el);
    },
  };

  return directive;
}
