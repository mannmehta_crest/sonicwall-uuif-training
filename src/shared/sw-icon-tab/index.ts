import SwIconTab from './sw-icon-tab.vue';
import SwIconTabDivider from './sw-icon-tab-divider.vue';
import SwIconTabGroup from './sw-icon-tab-group.vue';

export * from './types';
export {
  SwIconTab,
  SwIconTabDivider,
  SwIconTabGroup,
};
