import { VueRenderFunction } from '../common-types';
import { IconConfig } from '../sw-icon';

export interface IconTabItem {
  tabId: string;
  icon: string | IconConfig;
  text?: string | VueRenderFunction;
  selected?: boolean;
  disabled?: boolean;
  tabColor?: string;
  noBorder?: boolean;
  tooltip?: boolean;
}

export interface IconTabDividerItem {
  width?: number;
  height?: number;
  thickness?: number;
  color?: string;
}

export interface IconTabGroupItem {
  items: (IconTabItem | IconTabDividerItem)[];
  label?: string;
}
