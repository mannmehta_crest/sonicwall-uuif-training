import { resizeObserver } from './resize-observer';
import { mutationObserverManager } from './mutation-observer-manager';
import { stylesManager } from './styles-manager';
import { visibilityObserver } from './visibility-observer';
import { dataset } from './dataset';

let cachedScrollbarSize: number | undefined = undefined;
function getScrollbarSize(recalc: boolean = false) {
  if (recalc || cachedScrollbarSize === undefined) {
    const inner = document.createElement('div');
    inner.style.width = '100%';
    inner.style.height = '200px';

    const outer = document.createElement('div');
    const outerStyle = outer.style;

    outerStyle.position = 'absolute';
    outerStyle.top = '0';
    outerStyle.left = '0';
    outerStyle.pointerEvents = 'none';
    outerStyle.visibility = 'hidden';
    outerStyle.width = '200px';
    outerStyle.height = '150px';
    outerStyle.overflow = 'hidden';

    outer.appendChild(inner);

    document.body.appendChild(outer);

    const widthContained = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    let widthScroll = inner.offsetWidth;

    if (widthContained === widthScroll) {
      widthScroll = outer.clientWidth;
    }

    document.body.removeChild(outer);

    cachedScrollbarSize = widthContained - widthScroll;
  }
  return cachedScrollbarSize;
}

/**
 * BE CAREFUL, this could be the bottleneck
 */
function forceStylesApplied(el: HTMLElement): void {
  const style = getComputedStyle(el);
  style.height;
}

/**
 * Get the offset `{top, left}` of a specified element relative to the `document`.
 *
 * I know the approach we use here is old-school, which is traversing the `offsetParent`/`parentElement`
 * up to the `body` of DOM tree and accumulating all the offset along the way, while nowadays the most
 * popular approach is using `getBoundingClientRect()` to do the job behind the scene. However, this
 * approache is still very usefull in many cases considering its difference with `getBoundingClientRect()`.
 * Specifically, the rect returned by `getBoundingClientRect()` takes transformation into account, while
 * this approach is based on  elements' original layout positions.
 *
 * In a library that provides reusable components as building blocks, we may have to use this old-school
 * approach as much as possible, because these components could be used by developers in some containers
 * that have transformation in place, in such cases, the internal layout of components using
 * `getBoundingClientRect()` would not be as expected.
 */
function getOffset(el: HTMLElement): { top: number; left: number; } {
  let left = 0;
  let top = 0;

  let currElement: HTMLElement | null = el;
  let prevOffsetParent: HTMLElement | null = currElement;

  while (currElement && prevOffsetParent) {
    if (currElement.isSameNode(prevOffsetParent)) {
      left += prevOffsetParent.offsetLeft + prevOffsetParent.clientLeft;
      top += prevOffsetParent.offsetTop + prevOffsetParent.clientTop;
    }
    left -= currElement.scrollLeft;
    top -= currElement.scrollTop;

    prevOffsetParent = currElement.offsetParent as HTMLElement;
    currElement = currElement.parentElement;
  }

  if (process.env.NODE_ENV === 'development') {
    /** in dev mode, we would like to compare the result of this approach with
     * the one from `getBoundingClientRect()`, and warn if the results are significantly
     * different.
     */
    const rect = el.getBoundingClientRect();
    const absRect = { top: rect.top + window.pageYOffset, left: rect.left + window.pageXOffset };
    if (!isWithinThreshold(top, absRect.top) || !isWithinThreshold(left, absRect.left)) {
      console.warn(
        `Maybe it is transitioning =>
getOffset(): top = ${top}, left = ${left}
getBoundingClientRect(): top = ${absRect.top}, left = ${absRect.left}`,
      );
    }
  }

  return { top, left };
}

function getOriginalBoundingRect(el: HTMLElement): Pick<ClientRect, 'top' | 'left' | 'width' | 'height'> {
  const offset = getOffset(el);
  return {
    ...offset,
    width: el.offsetWidth,
    height: el.offsetHeight,
  };
}

function isWithinThreshold(my: number, native: number) {
  const threshold = 10;
  const roundMy = Math.round(my);
  const roundNative = Math.round(native);

  return roundMy !== 0 ? Math.abs(Math.abs(roundMy - roundNative) / roundMy) <= (threshold / 100) :
    Math.abs(roundNative) <= 1;
}

export const domService = {
  resizeObserver,
  getScrollbarSize,
  mutationObserverManager,
  stylesManager,
  visibilityObserver,
  dataset,
  forceStylesApplied,
  getOffset,
  getOriginalBoundingRect,
};
