import ResizeObserver from 'resize-observer-polyfill';

export type ResizeEventHandler = () => void;
export type ResizeWatchedElement = HTMLElement & {
  __z_resizeListeners: { handler: ResizeEventHandler }[],
};

const observer = new ResizeObserver((entries) => {
  entries.forEach((entry) => {
    const el = entry.target as ResizeWatchedElement;
    if (el.__z_resizeListeners) {
      el.__z_resizeListeners.forEach(listener => (listener.handler.call(el)));
    }
  });
});

function removeHandler(ele: ResizeWatchedElement, handler: ResizeEventHandler) {
  const listeners = ele.__z_resizeListeners;
  if (listeners) {
    const size = listeners.length;
    for (let i = 0; i < size; i += 1) {
      const h = listeners[i];
      if (h.handler === handler) {
        listeners.splice(i, 1);
        return;
      }
    }
  }
}

function addHandler(ele: ResizeWatchedElement, handler: ResizeEventHandler) {
  let listeners = ele.__z_resizeListeners;
  if (!listeners) {
    listeners = [];
    ele.__z_resizeListeners = listeners;
  }
  listeners.push({ handler });
}

export const resizeObserver = {
  on(resEle: HTMLElement, handler: ResizeEventHandler) {
    addHandler(resEle as ResizeWatchedElement, handler);
    observer.observe(resEle);
  },
  off(resEle: HTMLElement, handler: ResizeEventHandler) {
    removeHandler(resEle as ResizeWatchedElement, handler);
    observer.unobserve(resEle);
  },
};
