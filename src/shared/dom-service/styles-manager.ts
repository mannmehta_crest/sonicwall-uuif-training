import uuidv4 from 'uuid/v4';
import { CSSStyleOptions } from '../common-types';
import { mutationObserverManager } from './mutation-observer-manager';

/**
 * This will help manage styles on specific HTMLElement.
 * Since this utility will manipulate DOM element directly, it will conflict with Vue-managed
 * element in many ways, so don't use this on any Vue-managed elements.
 */

type StylesQueue = { ticket: string, styles: CSSStyleOptions }[];

const elementsMap: Map<HTMLElement, StylesQueue> = new Map();
let mutationObserver: MutationObserver | null = null;

function getElement(target: HTMLElement | string): HTMLElement | null {
  let targetEl: HTMLElement | null = null;

  if (typeof target === 'string') {
    /** as selector */
    targetEl = document.querySelector(target);
  } else {
    targetEl = target;
  }

  return targetEl;
}

function render(el: HTMLElement, queue: StylesQueue) {
  let styles: CSSStyleOptions = {};
  queue.forEach((item) => {
    styles = Object.assign(styles, item.styles);
  });
  el.removeAttribute('style');
  Object.assign(el.style, styles);
}

function apply(target: HTMLElement | string, styles: CSSStyleOptions): string {
  const targetEl = getElement(target);
  if (!targetEl) { return ''; }

  const ticket = uuidv4();

  let queue = elementsMap.get(targetEl);
  if (!queue) {
    queue = [{ ticket, styles }];
    elementsMap.set(targetEl, queue);
  } else {
    queue.push({ ticket, styles });
  }

  render(targetEl, queue);

  updateMutationObserver();

  return ticket;
}

function update(target: HTMLElement | string, ticket: string, styles: CSSStyleOptions): void {
  const targetEl = getElement(target);
  if (!targetEl) { return; }

  const queue = elementsMap.get(targetEl);
  if (!queue) { return; }

  const item = queue.find(i => (i.ticket === ticket));
  if (!item) { return; }

  item.styles = styles;

  render(targetEl, queue);
}

function cancel(target: HTMLElement | string, ticket: string): void {
  const targetEl = getElement(target);
  if (!targetEl) { return; }

  const queue = elementsMap.get(targetEl);
  if (!queue) { return; }

  const itemIndex = queue.findIndex(i => (i.ticket === ticket));
  if (itemIndex >= 0) {
    queue.splice(itemIndex, 1);
    render(targetEl, queue);

    if (queue.length <= 0) {
      elementsMap.delete(targetEl);
    }
  }

  updateMutationObserver();
}

function updateMutationObserver() {
  if (mutationObserver && elementsMap.size <= 0) {
    mutationObserver.disconnect();
    mutationObserver = null;
  } else if (!mutationObserver && elementsMap.size > 0) {
    mutationObserver = mutationObserverManager.setup(
      document.body,
      { childList: true, subtree: true },
      () => {
        const allEls = Array.from(elementsMap.keys());
        allEls.forEach((e) => {
          if (!document.body.contains(e)) {
            elementsMap.delete(e);
          }
        });
        updateMutationObserver();
      },
    );
  }
}

export const stylesManager = {
  apply,
  update,
  cancel,
};
