function setup(
  targetEl: Node,
  options: MutationObserverInit,
  callback: MutationCallback,
) {
  if (!(window as any).MutationObserver || !targetEl) { return null; }

  const eOpts: MutationObserverInit = Object.assign(
    {
      childList: false,
      attributes: false,
      characterData: false,
      subtree: false,
    },
    options,
  );
  const observer = new MutationObserver(callback);

  observer.observe(targetEl, eOpts);

  return observer;
}

export const mutationObserverManager = {
  setup,
};
