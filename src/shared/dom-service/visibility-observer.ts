import _ from 'lodash';
import { mutationObserverManager } from './mutation-observer-manager';

/**
 * We use MutationObserver on the `document` to achieve the goal that observing the
 * visibility of given elements. This is not the perfect solution, the better way could be
 * using `IntersectionObserver` supported by browsers natively. However, `IntersectionObserver`
 * hasn't reached stable state, we have to use this temporary solution for now.
 */

type VisibilityChangeCallback = (visible: boolean) => void;

const elementsMap: Map<HTMLElement, { cb: VisibilityChangeCallback, currentVisibility: boolean; }> = new Map();
let mutationObserver: MutationObserver | null = null;

function isVisible(target: HTMLElement) {
  return !!target.offsetParent && document.body.contains(target);
}

function handleBodyMutated() {
  elementsMap.forEach((info, target) => {
    const visible = isVisible(target);
    if (info.currentVisibility !== visible) {
      info.currentVisibility = visible;
      info.cb(visible);
    }
  });
}

function setupObserver() {
  if (mutationObserver) { return; }
  mutationObserver = mutationObserverManager.setup(
    document.body,
    { childList: true, subtree: true, attributes: true, characterData: true },
    _.debounce(handleBodyMutated, 100),
  );
}

function teardownObserver() {
  if (!mutationObserver) { return; }
  mutationObserver.disconnect();
  mutationObserver = null;
}

export const visibilityObserver = {
  on(target: HTMLElement, callback: VisibilityChangeCallback) {
    if (elementsMap.size <= 0) {
      setupObserver();
    }
    const visible = isVisible(target);
    elementsMap.set(target, { cb: callback, currentVisibility: visible });
  },
  off(target: HTMLElement) {
    elementsMap.delete(target);
    if (elementsMap.size <= 0) {
      teardownObserver();
    }
  },
};
