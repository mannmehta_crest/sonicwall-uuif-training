export const dataset = {
  set(el: HTMLElement, key: string, value: object | string | number | boolean): void {
    if (el) {
      el.dataset[key] = JSON.stringify(value);
    }
  },
  get(el: HTMLElement, key: string): any {
    if (el) {
      const stringData: string | undefined = el.dataset[key];
      try {
        if (stringData) {
          return JSON.parse(stringData);
        }
      } catch (e) {
        return undefined;
      }
    }
    return undefined;
  },
  unset(el: HTMLElement, key: string): void {
    if (el) {
      delete el.dataset[key];
    }
  },
};
