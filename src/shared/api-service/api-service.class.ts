import { ApiError, ApiErrorCode } from './api-error.class';
import { ApiAdapterAxios } from './api-adapter-axios.class';
import { LooseObject } from '../common-types';
import { DigestAuthChallengeInfo } from './digest-auth-util.service';

export type ApiMethod = 'get' | 'put' | 'post' | 'delete' | 'patch';

export interface ApiRequestConfig {
  timeout?: number;
  requestType?: 'json' | 'form-urlencoded';
  responseType?: 'json' | 'form-urlencoded';
  headers?: LooseObject;
  params?: LooseObject;
  data?: LooseObject;
  auth?: ApiRequestConfigAuth;
}

export interface ApiRequestConfigAuth {
  type: 'iam' | 'auth-token' | 'digest-auth';
  authToken?: string;
  iamInfo?: ApiRequestConfigAuthIamInfo;
  digestAuthInfo?: ApiRequestConfigAuthDigestAuthInfo;
}

export interface ApiRequestConfigAuthIamInfo {
  region: string;
  service: string;
  credentials: {
    secretAccessKey: string;
    accessKeyId: string;
    sessionToken?: string;
  };
}

export interface ApiRequestConfigAuthDigestAuthInfo {
  username: string;
  password: string;
  forEveryRequest?: boolean;
  challengeInfo?: DigestAuthChallengeInfo; // caching for current cycle
  cnonce?: string; // caching for current cycle
  nc?: number; // caching for current cycle
}

export interface SuggestedNormalizedServerResponse<D> {
  success: boolean;
  code?: number;
  message?: string;
  data?: D;
}

export interface ApiAdapterInitOptions {
  allowedStatusCodes?: number[];
}

function handleGeneralError(error: ApiError | Error | any, apiMethod: ApiMethod, url: string): never {
  if (error instanceof ApiError) {
    throw error;
  }
  if (error instanceof Error) {
    console.error(error);
    throw new ApiError({
      code: ApiErrorCode.GENERAL,
      message: `Error when doing [${apiMethod}] to [${url}]: ${error.message}`,
      source: error,
    });
  }
  console.error(error);
  throw new ApiError({
    code: ApiErrorCode.GENERAL,
    message: `Error when doing [${apiMethod}] to [${url}]: ${error}`,
  });
}

/**
 * Detect if server indicates business error occurred in the backend.
 * Normalized server responsed data should be of the shape as below in order to get this work:
 * ```
 * {
 * success: boolean,
 * code?: number, // present when success === false
 * message?: string, // present when success === false
 * data?: any, // the actual business data we want to receive when success === true
 * }
 * ```
 */
function handleServerBusinessError<R extends SuggestedNormalizedServerResponse<any>>(respData: R, apiMethod: ApiMethod, url: string): R | never {
  if (respData && !respData.success) {
    throw new ApiError({
      code: ApiErrorCode.SERVER_ERROR,
      serverCode: respData.code || 0,
      message: `Server error when doing [${apiMethod}] to [${url}]: ${respData.message}`,
    });
  }
  return respData;
}

/**
 * Low-level API class for invoking network adapter to initiate request to servers,
 * it also does some primitive error handlings for general cases, while some of these
 * error handlings are performed in network adapter.
 *
 * `config` argument used in some of the member functions are in flavor of Axios config
 * object (https://github.com/axios/axios#request-config), with some additional properties:
```
{
  requestType: string, // 'json'(default), 'form-urlencoded'
}
```
 * `ApiService` would not do much with `config` but pass it to adapter to allow adapters to
 * translate and handle respectively.
 */
class ApiService {
  private adapter = new ApiAdapterAxios();

  constructor(
    private baseUrl = '',
    adapterInitOpts: ApiAdapterInitOptions = {},
    private defaultConfig: ApiRequestConfig = {
      timeout: 180000,
      requestType: 'json',
      responseType: 'json',
      headers: {},
    },
  ) {
    this.adapter.init(adapterInitOpts);
  }

  private getUrl(endpoint: string): string {
    return `${this.baseUrl}${endpoint}`;
  }

  private normalizeConfig(inConfig: ApiRequestConfig): ApiRequestConfig {
    function getContentTypeFromRequestType(requestType: ApiRequestConfig['requestType']): string {
      if (requestType === 'json') {
        return 'application/json';
      }
      if (requestType === 'form-urlencoded') {
        return 'application/x-www-form-urlencoded;charset=UTF-8';
      }
      return 'text/plain';
    }

    const preNormalizedConfig: ApiRequestConfig = Object.assign({}, this.defaultConfig, inConfig);

    /** we want to merge header config */
    const headers: LooseObject = Object.assign(
      {},
      this.defaultConfig.headers,
      {
        'Content-Type': getContentTypeFromRequestType(preNormalizedConfig.requestType),
      },
      inConfig.headers || {},
    );

    return Object.assign({}, preNormalizedConfig, {
      headers,
      /** there could be more to-merged config */
    });
  }

  setAuth(authParam: ApiRequestConfigAuth) {
    this.defaultConfig.auth = authParam;
  }

  get<R>(endpoint: string, config: ApiRequestConfig = {}, detectServerBusinessError: boolean = false): Promise<R> {
    const apiConfig = this.normalizeConfig(config);
    const url = this.getUrl(endpoint);

    let result = this.adapter
      .get<R>(url, apiConfig)
      .catch((error) => {
        return handleGeneralError(error, 'get', url);
      });
    if (detectServerBusinessError) {
      result = result.then(respData => handleServerBusinessError(respData as any, 'get', url));
    }
    return result;
  }

  post<R>(endpoint: string, data: any, config: ApiRequestConfig = {}, detectServerBusinessError: boolean = false): Promise<R> {
    const apiConfig = this.normalizeConfig(config);
    const url = this.getUrl(endpoint);

    let result = this.adapter
      .post<R>(url, data, apiConfig)
      .catch((error) => {
        return handleGeneralError(error, 'post', url);
      });
    if (detectServerBusinessError) {
      result = result.then(respData => handleServerBusinessError(respData as any, 'post', url));
    }
    return result;
  }

  put<R>(endpoint: string, data: any, config: ApiRequestConfig = {}, detectServerBusinessError: boolean = false): Promise<R> {
    const apiConfig = this.normalizeConfig(config);
    const url = this.getUrl(endpoint);

    let result = this.adapter
      .put<R>(url, data, apiConfig)
      .catch((error) => {
        return handleGeneralError(error, 'put', url);
      });
    if (detectServerBusinessError) {
      result = result.then(respData => handleServerBusinessError(respData as any, 'put', url));
    }
    return result;
  }

  patch<R>(endpoint: string, data: any, config: ApiRequestConfig = {}, detectServerBusinessError: boolean = false): Promise<R> {
    const apiConfig = this.normalizeConfig(config);
    const url = this.getUrl(endpoint);

    let result = this.adapter
      .patch<R>(url, data, apiConfig)
      .catch((error) => {
        return handleGeneralError(error, 'patch', url);
      });
    if (detectServerBusinessError) {
      result = result.then(respData => handleServerBusinessError(respData as any, 'patch', url));
    }
    return result;
  }

  delete<R>(endpoint: string, config: ApiRequestConfig = {}, detectServerBusinessError: boolean = false): Promise<R> {
    const apiConfig = this.normalizeConfig(config);
    const url = this.getUrl(endpoint);

    let result = this.adapter
      .delete<R>(url, apiConfig)
      .catch((error) => {
        return handleGeneralError(error, 'delete', url);
      });
    if (detectServerBusinessError) {
      result = result.then(respData => handleServerBusinessError(respData as any, 'delete', url));
    }
    return result;
  }
}

export { ApiService };
