import { LooseObject } from '../common-types';

export enum ApiErrorCode {
  GENERAL,
  PROT_RESP_STATUS_CODE,
  PROT_RESP_MALFORMAT_DATA,
  PROT_RESP_NO_RESP,
  PROT_REQ_NOT_SENT,
  MALFORMAT_DATA,
  SERVER_ERROR,
}

export type ApiErrorInit = Partial<Pick<ApiError, 'code' | 'serverCode' | 'message' | 'source' | 'response'>>;

export interface ApiErrorResponse {
  status: number;
  headers: LooseObject;
  data: any;
}

class ApiError {
  public code: ApiErrorCode = ApiErrorCode.GENERAL;
  /** valid only if code === SERVER_ERROR */
  public serverCode: number | string | null = null;
  public message: string | null = null;
  /** Error from which this ApiError originates */
  public source: Error | null = null;
  /** Server response associated with this error */
  public response: ApiErrorResponse | null = null;

  constructor(initObj: ApiErrorInit = {}) {
    Object.assign(this, initObj);
  }
}

export { ApiError };
