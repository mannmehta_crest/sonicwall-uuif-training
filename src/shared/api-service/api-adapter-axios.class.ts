import qs from 'qs';
import axios, { AxiosResponse, AxiosError, AxiosRequestConfig, AxiosInstance } from 'axios';
import aws4 from 'aws4';
import { ApiError, ApiErrorCode } from './api-error.class';
import { ApiRequestConfig, ApiAdapterInitOptions } from './api-service.class';
import { digestAuthUtil } from './digest-auth-util.service';

/**
 * Normalized the protocol errors to `ApiError`.
 * This function will throw an new ApiError no matter what.
 */
function handleProtocolError(error: AxiosError, axiosRunningConfig: AxiosRequestConfig): never {
  if (error.response) {
    throw new ApiError({
      code: ApiErrorCode.PROT_RESP_STATUS_CODE,
      message: `Response of [${axiosRunningConfig.method}] to [${axiosRunningConfig.url}] has status code [${error.response.status}]`,
      response: {
        status: error.response.status,
        headers: error.response.headers,
        data: error.response.data,
      },
    });
  }
  if (error.request) {
    throw new ApiError({
      code: ApiErrorCode.PROT_RESP_NO_RESP,
      message: `No response for [${axiosRunningConfig.method}] to [${axiosRunningConfig.url}]`,
    });
  }
  throw new ApiError({
    code: ApiErrorCode.PROT_REQ_NOT_SENT,
    message: `Request for [${axiosRunningConfig.method}] to [${axiosRunningConfig.url}] failed to be sent`,
    source: (error instanceof Error) ? error : null,
  });
}

/**
 * Extract adapter config that is used for driving low-level axios, from the normalized api config.
 */
function getAdapterConfigFromApiConfig(apiRequestConfig: ApiRequestConfig): AxiosRequestConfig {
  return {
    responseType: apiRequestConfig.responseType,
    timeout: apiRequestConfig.timeout,
    headers: apiRequestConfig.headers,
    params: apiRequestConfig.params,
  };
}

function normalizeRequestData(data: any, apiRequestConfig: ApiRequestConfig): any {
  let nData = null;
  if (apiRequestConfig.requestType === 'json') {
    nData = data;
  } else if (apiRequestConfig.requestType === 'form-urlencoded') {
    nData = qs.stringify(data);
  } else {
    nData = String(data);
  }
  return nData;
}

/**
 * Axios adapter as data communication layer for ApiService
 */
class ApiAdapterAxios {
  private axiosInst: AxiosInstance = axios.create();
  /** special config to tweak this adapter internal */
  private specialConfig: AxiosRequestConfig = {};
  private allowedResponseStatus: number[] = [200, 204];
  private possibleEmptyResponseStatus: number[] = [204];

  private getRunningConfig(apiRequestConfig: ApiRequestConfig, forceConfig: AxiosRequestConfig): AxiosRequestConfig {
    const adapterConfig = getAdapterConfigFromApiConfig(apiRequestConfig);
    return Object.assign({}, this.specialConfig, adapterConfig, forceConfig);
  }

  /**
   * Detect protocol-level malformatted response.
   */
  private screenResponse<R>(response: AxiosResponse<R>): AxiosResponse<R> {
    const axiosRunningConfig = response.config;

    if (!this.allowedResponseStatus.includes(response.status)) {
      throw new ApiError({
        code: ApiErrorCode.PROT_RESP_MALFORMAT_DATA,
        message: `Malformat data: Response of [${axiosRunningConfig.method}] to [${axiosRunningConfig.url}] has undesired status code [${response.status}]`,
      });
    }
    if (
      !this.possibleEmptyResponseStatus.includes(response.status) &&
      (response.data === null || response.data === undefined)
    ) {
      /** force to have effective data */
      throw new ApiError({
        code: ApiErrorCode.PROT_RESP_MALFORMAT_DATA,
        message: `Null Response of [${axiosRunningConfig.method}] to [${axiosRunningConfig.url}]`,
      });
    }

    return response;
  }

  private initiate<R>(apiConfig: ApiRequestConfig, runningConfig: AxiosRequestConfig): Promise<R> {

    this.processAuth(apiConfig, runningConfig);

    return this.axiosInst.request<R>(runningConfig)
      .catch((error: AxiosError) => {
        /** check for Digest Auth necessity */
        if (error.response && error.response.status === 401) {
          if (!apiConfig.auth) { throw error; }

          const authParam = apiConfig.auth;
          if (authParam.type !== 'digest-auth' || !authParam.digestAuthInfo) { throw error; }

          /** we try standard header first, then sonicwall specific header */
          let authHeader: string = error.response.headers['WWW-Authenticate'.toLowerCase()];
          if (!authHeader) {
            authHeader = error.response.headers['X-SNWL-Authenticate'.toLowerCase()];
          }
          if (!authHeader) { throw error; }

          const challengeInfo = digestAuthUtil.parseAuthenticateHeader(authHeader);
          if (!challengeInfo) {
            console.error('Failed to parse WWW-Authenticate header for Digest Auth');
            throw error;
          }

          const cnonce = digestAuthUtil.genClientNonce();
          const nc = 1;
          const authorizationHeader = digestAuthUtil.calcAuthorizationHeader(
            authParam.digestAuthInfo.username,
            authParam.digestAuthInfo.password,
            cnonce,
            nc,
            runningConfig.method!.toUpperCase(),
            this.getAbsolutePathFromRunningConfig(runningConfig),
            challengeInfo,
          );

          if (!authorizationHeader) {
            console.error('Failed to get Authorization header for Digest Auth');
            throw error;
          }

          authParam.digestAuthInfo.challengeInfo = challengeInfo;
          authParam.digestAuthInfo.cnonce = cnonce;
          authParam.digestAuthInfo.nc = nc;

          Object.assign(runningConfig.headers, { Authorization: authorizationHeader });

          return this.axiosInst.request<R>(runningConfig);
        }
        throw error;
      })
      .catch((error: AxiosError) => {
        return handleProtocolError(error, runningConfig);
      })
      .then(response => (this.screenResponse(response)))
      .then(response => response.data);
  }

  /** this may alter properties in axiosRunningConfig */
  private processAuth(apiRequestConfig: ApiRequestConfig, axiosRunningConfig: AxiosRequestConfig) {
    const authParam = apiRequestConfig.auth;
    if (!authParam || !axiosRunningConfig) { return; }
    if (authParam.type === 'iam' && !!authParam.iamInfo) {
      const urlObj = new URL(axiosRunningConfig.url!);
      const headersToSign = Object.assign({}, axiosRunningConfig.headers);

      const optToSign = {
        host: urlObj.host,
        method: axiosRunningConfig.method!.toUpperCase(),
        path: this.getAbsolutePathFromRunningConfig(axiosRunningConfig),
        service: authParam.iamInfo.service,
        region: authParam.iamInfo.region,
        headers: headersToSign,
        body: undefined,
      };

      if (
        axiosRunningConfig.method === 'post' ||
        axiosRunningConfig.method === 'put' ||
        axiosRunningConfig.method === 'patch'
      ) {
        /** body is required in these cases */
        optToSign.body = apiRequestConfig.requestType === 'json' ? JSON.stringify(axiosRunningConfig.data) : axiosRunningConfig.data;
      } else {
        /** content-type is not necessary */
        delete optToSign.headers['Content-Type'];
      }

      aws4.sign(optToSign, {
        secretAccessKey: authParam.iamInfo.credentials.secretAccessKey,
        accessKeyId: authParam.iamInfo.credentials.accessKeyId,
        sessionToken: authParam.iamInfo.credentials.sessionToken,
      });

      /** now we can populate required fields back to config */
      delete optToSign.headers['Host'];
      delete optToSign.headers['Content-Length'];
      Object.assign(axiosRunningConfig, {
        headers: optToSign.headers,
      });
    } else if (authParam.type === 'auth-token' && !!authParam.authToken) {
      const headers = Object.assign({}, axiosRunningConfig.headers, { Authorization: authParam.authToken });
      Object.assign(axiosRunningConfig, {
        headers,
      });
    } else if (authParam.type === 'digest-auth' && !!authParam.digestAuthInfo) {
      if (authParam.digestAuthInfo.forEveryRequest === true) {
        /** will be setting `Authorization` header for every request if possible */
        const digestAuthInfo = authParam.digestAuthInfo;
        if (digestAuthInfo.nc && digestAuthInfo.cnonce && digestAuthInfo.challengeInfo) {
          const authorizationHeader = digestAuthUtil.calcAuthorizationHeader(
            authParam.digestAuthInfo.username,
            authParam.digestAuthInfo.password,
            digestAuthInfo.cnonce,
            digestAuthInfo.nc,
            axiosRunningConfig.method!.toUpperCase(),
            this.getAbsolutePathFromRunningConfig(axiosRunningConfig),
            digestAuthInfo.challengeInfo,
          );

          if (!authorizationHeader) {
            const headers = Object.assign({}, axiosRunningConfig.headers, { Authorization: authorizationHeader });
            Object.assign(axiosRunningConfig, { headers });
          }
        }
      }
    }
  }

  private getAbsolutePathFromRunningConfig(axiosRunningConfig: AxiosRequestConfig): string {
    const urlObj = new URL(axiosRunningConfig.url!);
    const extraQuery = axiosRunningConfig.params ? qs.stringify(axiosRunningConfig.params) : '';

    let query = '';
    if (urlObj.search || extraQuery) {
      query += urlObj.search || '?';
      query += urlObj.search ? `&${extraQuery}` : extraQuery;
    }

    return `${urlObj.pathname}${query}`;
  }

  init(initOpts: ApiAdapterInitOptions) {
    if (initOpts.allowedStatusCodes && initOpts.allowedStatusCodes.length > 0) {
      this.allowedResponseStatus = initOpts.allowedStatusCodes.slice(0);
    }
  }

  get<R = any>(url: string, config: ApiRequestConfig = {}): Promise<R> {
    const axiosRunningConfig = this.getRunningConfig(config, {
      url,
      method: 'get',
    });

    return this.initiate<R>(config, axiosRunningConfig);
  }

  post<R = any>(url: string, data: any, config: ApiRequestConfig = {}): Promise<R> {
    const axiosRunningConfig = this.getRunningConfig(config, {
      url,
      method: 'post',
      data: normalizeRequestData(data, config),
    });

    return this.initiate<R>(config, axiosRunningConfig);
  }

  put<R>(url: string, data: any, config: ApiRequestConfig = {}): Promise<R> {
    const axiosRunningConfig = this.getRunningConfig(config, {
      url,
      method: 'put',
      data: normalizeRequestData(data, config),
    });

    return this.initiate<R>(config, axiosRunningConfig);
  }

  patch<R>(url: string, data: any, config: ApiRequestConfig = {}): Promise<R> {
    const axiosRunningConfig = this.getRunningConfig(config, {
      url,
      method: 'patch',
      data: normalizeRequestData(data, config),
    });

    return this.initiate<R>(config, axiosRunningConfig);
  }

  delete<R>(url: string, config: ApiRequestConfig = {}) {
    const forcedConfig: AxiosRequestConfig = {
      url,
      method: 'delete',
    };

    /** specially allow to specify `data` for `DELETE` verb as there are some uncommon uses of this */
    if (config.data !== undefined) {
      forcedConfig.data = normalizeRequestData(config.data, config);
    }

    const axiosRunningConfig = this.getRunningConfig(config, forcedConfig);

    return this.initiate<R>(config, axiosRunningConfig);
  }
}

export { ApiAdapterAxios };
