import sha256 from 'crypto-js/sha256';
import md5 from 'crypto-js/md5';
import base64 from 'crypto-js/enc-base64';
import { lib } from 'crypto-js';
import _ from 'lodash';

/** All information are in unquoted format */
export interface DigestAuthChallengeInfo {
  realm: string;
  nonce: string;
  opaque: string;
  algorithm: string;
  qop: string;
  ha1?: string; // for caching HA1 result
}

export interface DigestAuthResponseInfo {
  response: string;
  username: string;
  realm: string;
  uri: string;
  qop: string;
  cnonce: string;
  nc: number;
}

const DIGEST_SCHEME = 'Digest';

/**
 * Utility functions for RFC7616 Digest Auth simple process.
 *
 * **NOTE**
 * For now, we only support `qop="auth"`, `algorithm=<algorithm>`, and the algorithms we support
 * are `MD5` and `SHA-256`.
 */
class DigestAuthUtil {
  tokenizeHeader(headerValue: string): string[] | null {
    const ws = '(?:(?:\\r\\n)?[ \\t])+';
    const token = '(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2E\\x30-\\x39\\x3F\\x41-\\x5A\\x5E-\\x7A\\x7C\\x7E]+)';
    const quotedString = `"(?:[\\x00-\\x0B\\x0D-\\x21\\x23-\\x5B\\\\x5D-\\x7F]|${ws}|\\\\[\\x00-\\x7F])*"`;
    const tokenizerRegex = RegExp(`${token}(?:=(?:${quotedString}|${token}))?`, 'g');

    if (!headerValue) { return null; }
    return headerValue.match(tokenizerRegex);
  }

  normalizeHeaderString(header: string): string | string[] {
    const index = header.indexOf(', Digest ');
    if (index < 0) { return header; }
    return [
      header.substring(0, index),
      header.substring(index + 2),
    ];
  }

  parseAuthenticateHeader(authHeader: string): DigestAuthChallengeInfo | null {
    const normalizedHeader = this.normalizeHeaderString(authHeader);
    // we simply pick the first one if multiple scheme is provided
    const header = Array.isArray(normalizedHeader) ? normalizedHeader[0] : normalizedHeader;
    const tokens = this.tokenizeHeader(header);
    if (!tokens || tokens[0] !== DIGEST_SCHEME) { return null; }

    const challengeInfo: DigestAuthChallengeInfo = {} as DigestAuthChallengeInfo;
    const realmRegex = /^realm=(.*)$/;
    const nonceRegex = /^nonce=(.*)$/;
    const opaqueRegex = /^opaque=(.*)$/;
    const algorithmRegex = /^algorithm=(.*)$/;
    const qopRegex = /^qop=(.*)$/;

    tokens.slice(1)
      .forEach((token) => {
        let matched = realmRegex.exec(token);
        if (matched) {
          challengeInfo.realm = this.unq(matched[1]);
          return;
        }

        matched = nonceRegex.exec(token);
        if (matched) {
          challengeInfo.nonce = this.unq(matched[1]);
          return;
        }

        matched = opaqueRegex.exec(token);
        if (matched) {
          challengeInfo.opaque = this.unq(matched[1]);
          return;
        }

        matched = algorithmRegex.exec(token);
        if (matched) {
          challengeInfo.algorithm = matched[1];
          return;
        }

        matched = qopRegex.exec(token);
        if (matched) {
          const qops = this.unq(matched[1])
            .split(',');
          challengeInfo.qop = _.trim(qops[0]);
          return;
        }
      });

    if (
      !challengeInfo.realm || !challengeInfo.nonce || !challengeInfo.opaque ||
      !challengeInfo.algorithm || !challengeInfo.qop
    ) {
      console.error('Authenticate Header is not sufficient');
      return null;
    }

    return challengeInfo;
  }

  calcAuthorizationHeader(
    username: string,
    password: string,
    cnonce: string,
    nc: number,
    method: string,
    uri: string,
    challengeInfo: DigestAuthChallengeInfo,
  ): string | null {
    let hash: ((m: string) => CryptoJS.WordArray) | null = null;
    if (challengeInfo.algorithm === 'SHA-256') {
      hash = sha256;
    } else if (challengeInfo.algorithm === 'MD5') {
      hash = md5;
    } else {
      return null;
    }

    let ha1 = challengeInfo.ha1;
    if (!ha1) {
      ha1 = hash(`${username}:${challengeInfo.realm}:${password}`)
        .toString();
      /** we cache the result for speed */
      challengeInfo.ha1 = ha1;
    }

    let ha2: string = '';
    if (challengeInfo.qop === 'auth') {
      ha2 = hash(`${method}:${uri}`)
        .toString();
    } else {
      return null;
    }

    const ncParam = nc.toString(16).padStart(8, '0'); // tslint:disable-line
    const response: string = hash(`${ha1}:${challengeInfo.nonce}:${ncParam}:${cnonce}:${challengeInfo.qop}:${ha2}`)
      .toString();

    const header = `Digest username="${username}", realm="${challengeInfo.realm}", uri="${uri}", algorithm=${challengeInfo.algorithm}, ` +
      `nonce=${challengeInfo.nonce}, nc=${ncParam}, cnonce="${cnonce}", qop=${challengeInfo.qop}, opaque="${challengeInfo.opaque}", ` +
      `response="${response}"`;

    return header;
  }

  genClientNonce(): string {
    return base64.stringify(lib.WordArray.random(16));
  }

  private unq(quotedString: string): string {
    return quotedString.substr(1, quotedString.length - 2)
      .replace(/(?:(?:\r\n)?[ \t])+/g, ' ');
  }
}

export const digestAuthUtil = new DigestAuthUtil();
