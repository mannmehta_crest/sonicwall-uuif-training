/**
 * Reference Pure CSS Circle Menu from
 * https://codepen.io/hadarweiss/pen/WvEXeK
 * by Hadar Weiss
 *
 * We put this reusable component here in SSC temporarily for quick implementation.
 */
import SwCircleMenu from './sw-circle-menu.vue';

export * from './types';

export {
  SwCircleMenu,
};
