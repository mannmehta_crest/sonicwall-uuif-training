export interface CircleMenuItem {
  menuId: string;
  icon: string;
  href?: string;
  desc?: string;
}
