export interface ToggleOption {
  id: string;
  text?: string;
  icon?: string;
}

export type ToggleOptions = ToggleOption[];
