import SwToggle from './sw-toggle.vue';
import SwGroupToggle from './sw-group-toggle.vue';

export * from './types';

export {
  SwToggle,
  SwGroupToggle,
};
