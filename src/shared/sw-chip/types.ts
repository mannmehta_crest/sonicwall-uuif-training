import { VueRenderFunction } from '../common-types';

export interface ChipItem {
  id: string;
  label: string | VueRenderFunction;
  withClose?: boolean;
}
