export interface ItemSelectRequestData {
  itemId: string;
  selecting: boolean;
}

export interface ItemExpandRequestData {
  itemId: string;
  expanding: boolean;
}

export interface ProvidedListParam {
  selectedItems: string[];
  expandedItems: string[];
  itemOverflow: boolean;
  triggerStyle: 'arrow' | 'plus-minus';
  flat: boolean;
  initialPadding: number[];
  indent: number;
  withItemDivider: boolean;
}

export interface InternalItemHieDescriptor {
  level: number;
  ancestorsNextSiblingFlags: boolean[];
  isLastSibling: boolean;
}
