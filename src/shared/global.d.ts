import Vue, { VueConstructor } from "vue";

declare global {
  namespace JSX {
    interface IntrinsicElements {
      [elemName: string]: any;
    }
    interface Element {
      [elemName: string]: any;
    }
    interface ElementAttributesProperty {
      __attr_props__: any;
    }
  }
}

export type dummy = number;
