import Vue from 'vue';
import { Component } from 'vue-property-decorator';

/** used only in vue-shim.d.ts for typing */
export class SwExtendedVueShim extends Vue {
  protected __attr_props__: any; // tslint:disable-line
  [index: string]: any;
}

@Component
export class SwExtendedVue extends Vue {
  protected __attr_props__: any; // tslint:disable-line
}
