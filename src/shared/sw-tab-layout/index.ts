import SwTabGroup from './sw-tab-group.vue';
import SwTabPage from './sw-tab-page.vue';

export * from './types';

export {
  SwTabGroup,
  SwTabPage,
};
