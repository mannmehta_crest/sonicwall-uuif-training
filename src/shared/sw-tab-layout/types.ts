import { CreateElement, VNode } from 'vue';

export interface TabRenderData {
  props: TabProps;
  active: boolean;
}

export type TabRenderFunction = (h: CreateElement, data: TabRenderData) => VNode;

export interface TabProps {
  tabId: string;
  label?: string;
  icon?: string;
  disabled?: boolean;
  closable?: boolean;
  innerRender?: TabRenderFunction; // once specified, this will take precedence over the default rendering
}

export interface ProvidedTabParam {
  level: number;
  vertical: boolean;
  tabGutter: number;
  arrowIndicator: boolean;
  theme: 'light' | 'dark';
  centered: boolean;
}
