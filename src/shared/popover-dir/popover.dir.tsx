import { popperService, PopperDirValues, PopperDirModifiers } from '../popper-service';
import SwPopover from './sw-popover.vue';
import { VueRenderFunction } from '../common-types';

export type PopoverRequestCloseFunc = () => void;

interface PopoverDirValuesInternal extends Partial<PopperDirValues> {
  title?: string | VueRenderFunction;
  withCloseIcon?: boolean;
  noPadding?: boolean;
  maxWidth?: number;
  maxHeight?: number;
  onRequestClose?: PopoverRequestCloseFunc; // valid only if `tigger === controlled`
}

export type PopoverDirValues = Pick<PopoverDirValuesInternal,
  'persistent' | 'placement' | 'content' | 'trigger' | 'visible' | 'onClickOutsidePopper' |
  'title' | 'withCloseIcon' | 'noPadding' | 'maxWidth' | 'maxHeight' | 'onRequestClose'>;

export interface PopoverDirModifiers extends Partial<PopperDirModifiers> {}

const popover = popperService.generateDirective<PopoverDirValues, PopoverDirModifiers>({
  dirId: 'popover',
  component: SwPopover,
  componentPropNames: ['title', 'withCloseIcon', 'noPadding', 'maxWidth', 'maxHeight', 'onRequestClose'],
  valueDefault: {
    title: undefined,
    withCloseIcon: true,
    noPadding: false,
    maxWidth: 500,
    maxHeight: 250,
    onRequestClose: undefined,
    /** below are pre-defined value options */
    content: 'No Popover?',
    persistent: false,
    placement: 'bottom',
    trigger: 'hover',
    visible: false,
    onClickOutsidePopper: null,
  },
  modifierNames: [],
});

export default popover;
