import { popperService, PopperDirValues, PopperDirModifiers } from '../popper-service';
import SwDropdownContent from './sw-dropdown.vue';

interface DropdownDirValuesInternal extends Partial<PopperDirValues> {
  withArrow?: boolean;
  maxWidth?: number;
  maxHeight?: number;
  width?: number;
  customClasses?: string[];
}

export type DropdownDirValues = Pick<DropdownDirValuesInternal,
  'placement' | 'content' | 'trigger' | 'visible'| 'enabled' | 'onClickOutsidePopper' | 'parentComponentInstance' |
  'withArrow' | 'maxWidth' | 'maxHeight' | 'width' | 'customClasses'>;

export interface DropdownDirModifiers extends Partial<PopperDirModifiers> {}

const dropdown = popperService.generateDirective<DropdownDirValues, DropdownDirModifiers>({
  dirId: 'dropdown',
  component: SwDropdownContent,
  componentPropNames: ['withArrow', 'maxWidth', 'maxHeight', 'width', 'customClasses'],
  valueDefault: {
    withArrow: true,
    maxWidth: 500,
    maxHeight: 800,
    width: undefined,
    customClasses: [],
    /** below are pre-defined value options */
    enabled: true,
    content: 'No Dropdown?',
    placement: 'bottom',
    trigger: 'controlled',
    visible: false,
    onClickOutsidePopper: null,
    parentComponentInstance: undefined,
  },
  modifierNames: [],
});

export default dropdown;
