import { SwExtendedVue } from '../sw-extended-vue';

export enum validationType {
  alphanumeric,
  any,
  date,
  decimalNumber,
  emailAddress,
  hostName,
  includeExclude,
  ipV4Address,
  ipV6Address,
  ipV4V6Address,
  macAddress,
  md5,
  multicastIp,
  port,
  time,
  url,
}
export interface ValidationConfigItem {
  instant: boolean;
  maxLength?: number;
  mustExclude?: (string | number) [];
  mustInclude?: (string | number) [];
  path: string;
  required: boolean;
  type: validationType;
}
export interface ValidationGroupConfig {
  context: SwExtendedVue;
  name: string;
  watcher: SwExtendedVue['$watch'];
  errorsObj: ValidationErrorObj;
  items: ValidationConfigItem[];
}
export interface ValidationErrorObj {
  [name: string]: string[] | undefined;
}
