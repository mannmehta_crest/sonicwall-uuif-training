import { validator } from './validator';

export * from './types';
export const validationService = {
  validator,
};
