import Vue from 'vue';
import _ from 'lodash';
import { validationType, ValidationGroupConfig, ValidationConfigItem, ValidationErrorObj } from './types';

class Validator {

  private validationGroups: ValidationGroupConfig[] = [];

  public init(validationGroupsConfig: ValidationGroupConfig[]) {
    this.validationGroups = validationGroupsConfig;

    this.validationGroups.forEach((group) => {
      const context = group.context;
      const watcher = group.watcher.bind(context);
      const items = group.items;

      this.initOutputErrorObj(group, group.errorsObj);

      items.forEach((item) => {
        const doImmediate = _.get(context, item.path, '') !== '';
        // Adding watchers
        context.$nextTick(() => {
          watcher(
            item.path,
            (value) => {
              this.delegateValidation(item, value, group);
            },
            { immediate: doImmediate },
          );
        });
      });
    });
  }

  /**
   * Initialize the error object so that it contains every keys from validation items that will
   * be watched for possible validation errors via reactivity.
   */
  private initOutputErrorObj(group: ValidationGroupConfig, obj: ValidationErrorObj) {
    group.items.forEach((item) => {
      Vue.set(obj, item.path, undefined);
    });
  }

  private delegateValidation(field: ValidationConfigItem, newValue: string, group: ValidationGroupConfig) { // tslint:disable-line
    // Start with an empty list of errors
    group.errorsObj[field.path] = [];

    let requiredCheck = false;
    if (field.required) {
      if (this.isEmpty(newValue)) {
        group.errorsObj[field.path]!.push('Field cannot be empty');
        requiredCheck = false;
      } else {
        requiredCheck = true;
      }
    } else {
      requiredCheck = true;
    }

    let maxLengthCheck = true;
    if (field.hasOwnProperty('maxLength')) {
      if (newValue.length > field.maxLength!) {
        maxLengthCheck = false;
        group.errorsObj[field.path]!.push('Exceeds maximum allowable character count');
      }
    }

    if (requiredCheck && maxLengthCheck) {
      switch (field.type) {
        case validationType.any:
          this.validateAny(field, newValue, group);
          break;
        case validationType.emailAddress:
          this.validateEmailAddress(field, newValue, group);
          break;
        case validationType.includeExclude:
          this.validateIncludeExclude(field, newValue, group);
          break;
        case validationType.ipV4Address:
          this.validateIPV4address(field, newValue, group);
          break;
        case validationType.ipV6Address:
          this.validateIPV6address(field, newValue, group);
          break;
        case validationType.ipV4V6Address:
          this.validateIPV4V6address(field, newValue, group);
          break;
        case validationType.hostName:
          this.validateHostname(field, newValue, group);
          break;
        case validationType.url:
          this.validateUrl(field, newValue, group);
          break;
        case validationType.date:
          this.validateDate(field, newValue, group);
          break;
        case validationType.alphanumeric:
          this.validateAlphanumeric(field, newValue, group);
          break;
        case validationType.decimalNumber:
          this.validateDecimalNumber(field, newValue, group);
          break;
        case validationType.md5:
          this.validateMd5(field, newValue, group);
          break;
        case validationType.macAddress:
          this.validateMacAddress(field, newValue, group);
          break;
        case validationType.time:
          this.validateTime(field, newValue, group);
          break;
        case validationType.multicastIp:
          this.validateMulticastIp(field, newValue, group);
          break;
        case validationType.port:
          this.validatePort(field, newValue, group);
          break;
        default:
          break;
      }
    }

    if (group.errorsObj[field.path]!.length === 0) {
      group.errorsObj[field.path] = undefined;
    }
  }

  /** On-demand validation */
  public validateForm() {
    this.validationGroups.forEach((group) => {
      const context = group.context;
      group.items.forEach((item) => {
        this.delegateValidation(item, _.get(context, item.path, ''), group);
      });
    });
    let verdict = true;
    this.validationGroups.forEach((group) => {
      const errorsObj = group.errorsObj;
      group.items.forEach((item) => {
        const errors :(string[]|undefined) = errorsObj[item.path];
        if (typeof errors !== 'undefined') {
          verdict = false;
        }
      });
    });
    return verdict;
  }

  private isEmpty(value: any) {
    /** FIXME: simple check for empty */
    return !String(value);
  }

  private validateAny(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) { // tslint:disable-line: no-unused
    return undefined;
  }

  private validateIncludeExclude(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;

    if (!field.hasOwnProperty('mustInclude') && !field.hasOwnProperty('mustExclude')) {
      throw new Error(`Validator for ${field.path} is incorrectly configured`);
    }

    const mustIncludeArr: (string|number)[] = field.hasOwnProperty('mustInclude') ? field['mustInclude']! : [];
    const mustExcludeArr: (string|number)[] = field.hasOwnProperty('mustExclude') ? field['mustExclude']! : [];

    let convertedItem: (string | number) = '';
    let convertedValue: (string | number) = '';

    let mustIncludeVerdict: boolean = false;
    let mustExcludeVerdict: boolean = true;
    let verdict: boolean = true;

    if (mustIncludeArr.length) {
      mustIncludeArr.forEach((item: (string|number)) => {
        if (typeof(item) === 'string') {
          convertedItem = item.toLowerCase();
          convertedValue = value.toLowerCase();
        } else {
          convertedItem = item;
          convertedValue = value;
        }
        if (convertedValue === convertedItem) {
          mustIncludeVerdict = true;
        }
      });
    }

    if (mustExcludeArr.length) {
      mustExcludeArr.forEach((item: (string|number)) => {
        if (typeof(item) === 'string') {
          convertedItem = item.toLowerCase();
          convertedValue = value.toLowerCase();
        } else {
          convertedItem = item;
          convertedValue = value;
        }
        if (convertedValue === convertedItem) {
          mustExcludeVerdict = false;
        }
      });
    }

    if ((mustIncludeArr.length) && (!mustIncludeVerdict)) {
      verdict = false;
    }

    if ((mustExcludeArr.length) && (!mustExcludeVerdict)) {
      verdict = false;
    }

    if (!verdict) {
      group.errorsObj[field.path]!.push('Invalid value');
    }
  }

  private validateHostname(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    var re = /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/; // tslint:disable-line
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid Hostname');
    }
  }

  private validateEmailAddress(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // tslint:disable-line
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid Email Address');
    }
  }

  private validateIPV4address(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /^(?:(?:^|\.)(?:2(?:5[0-5]|[0-4]\d)|1?\d?\d)){4}$/; // tslint:disable-line
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid IPV4 Address');
    }
  }

  private validateIPV6address(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))/; // tslint:disable-line
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid IPV6 Address');
    }
  }

  private validateIPV4V6address(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))|(^\s*((?=.{1,255}$)(?=.*[A-Za-z].*)[0-9A-Za-z](?:(?:[0-9A-Za-z]|\b-){0,61}[0-9A-Za-z])?(?:\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|\b-){0,61}[0-9A-Za-z])?)*)\s*$)/; // tslint:disable-line
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid IPV4 or IPV6 Address');
    }
  }

  private validateUrl(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))|(^\s*((?=.{1,255}$)(?=.*[A-Za-z].*)[0-9A-Za-z](?:(?:[0-9A-Za-z]|\b-){0,61}[0-9A-Za-z])?(?:\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|\b-){0,61}[0-9A-Za-z])?)*)\s*$)/; // tslint:disable-line
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid URL');
    }
  }

  private validateDate(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /(^(([0-9]{2})-([0-9]{2})-([0-9]{4}))$|(^([0-9]{2})\/([0-9]{2})\/([0-9]{4}))$|(^([0-9]{2}):([0-9]{2}):([0-9]{4}))$)/;
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid Date');
    }
  }

  private validateAlphanumeric(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /(^[a-zA-Z0-9]+$)/;
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid Entry');
    }
  }

  private validateDecimalNumber(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /([0-9](\.[0-9]+))/;
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid Decimal Number');
    }
  }

  private validateTime(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /^([0-1][0-9]|[2][0-3]):((([0-5][0-9]):([0-5][0-9]))|(([0-5][0-9])))$/;
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid Time');
    }
  }

  private validateMulticastIp(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /(^2(?:2[4-9]|3\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d?|0)){3}$)/;
    if (!re.test(value)) {
      group.errorsObj[field.path]!.push('Invalid  Multicast IP');
    }
  }

  private validatePort(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /^[0-9]+$/;
    if (re.test(value)) {
      const val: number = Number(value);
      if (val < 0 && val > 65535) {
        group.errorsObj[field.path]!.push('Invalid  Port');
      }
    } else {
      group.errorsObj[field.path]!.push('Invalid  Port');
    }
  }

  private validateMd5(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /(^[a-f0-9]{32}$)/;
    if (!re.test(value.toLowerCase())) {
      group.errorsObj[field.path]!.push('Invalid MD5 Hash');
    }
  }

  private validateMacAddress(field: ValidationConfigItem, newValue: any, group: ValidationGroupConfig) {
    const value: string = `${newValue}`;
    const re = /^(([A-Fa-f0-9]{2}[:]){5}[A-Fa-f0-9]{2}[,]?)+$/;
    if (!re.test(value.toLowerCase())) {
      group.errorsObj[field.path]!.push('Invalid MAC Address');
    }
  }
}

export const validator = new Validator();
