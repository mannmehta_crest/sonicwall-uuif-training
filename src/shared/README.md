## Shared components library

Do **NOT** use any predefined paths in the `import` statements, as anything in this shared folder could be used as sources for other projects.
