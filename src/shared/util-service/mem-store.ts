import uuidv4 from 'uuid/v4';
import { LooseObject } from '../common-types';

/**
 * Centralized in-memory uuid-based key-value store.
 */

export type SlotId = string;

const slotMap = new Map<SlotId, LooseObject>();

function createSlot(): SlotId {
  const slotId: SlotId = uuidv4();
  slotMap.set(slotId, {});
  return slotId;
}

function slotExists(slotId: SlotId): boolean {
  return slotMap.has(slotId);
}

function set<S, K extends keyof S>(id: SlotId, key: K, value: S[K]): void {
  const slotObj = slotMap.get(id);
  if (slotObj) {
    slotObj[key as string] = value;
  }
}

function unset<S>(id: SlotId, key: keyof S): void {
  const slotObj = slotMap.get(id);
  if (slotObj) {
    delete slotObj[key as string];
  }
}

function get<S, K extends keyof S>(id: SlotId, key: K): S[K] | undefined {
  const slotObj = slotMap.get(id);
  if (slotObj) {
    return slotObj[key as string];
  }
  return undefined;
}

function getSlot<S>(id: SlotId): S | undefined {
  const slotObj = slotMap.get(id) as S;
  return slotObj ? slotObj : undefined;
}

function destroySlot(id: SlotId): void {
  slotMap.delete(id);
}

export const memStore = {
  createSlot,
  slotExists,
  set,
  unset,
  get,
  getSlot,
  destroySlot,
};
