export const sizeMap: {[size: string]: number} = {
  xxsmall: 8,
  xsmall: 10,
  small: 14,
  medium: 18,
  xmedium: 20,
  large: 24,
  mlarge:29,
  xlarge: 36,
  xxlarge: 48,
};
