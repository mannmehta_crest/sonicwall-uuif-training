import { StatusName } from '../common-types';
import * as colorConst from './color-const';

interface StatusIconDetail {
  icon: string;
  color: string;
  textI18nPath: string;
}

const i18nPathPrefix = 'libShared.SwStatusInfo';

export const statusInfoMap: Record<StatusName, StatusIconDetail> = {
  informational: {
    icon: 'informational',
    color: colorConst.statusInformational,
    textI18nPath: `${i18nPathPrefix}.informational`,
  },
  failed: {
    icon: 'severity-critical',
    color: colorConst.statusCritical,
    textI18nPath: `${i18nPathPrefix}.failed`,
  },
  success: {
    icon: 'checkmark',
    color: colorConst.statusSuccess,
    textI18nPath: `${i18nPathPrefix}.success`,
  },
  critical: {
    icon: 'severity-critical',
    color: colorConst.statusCritical,
    textI18nPath: `${i18nPathPrefix}.critical`,
  },
  high: {
    icon: 'severity-high',
    color: colorConst.statusHigh,
    textI18nPath: `${i18nPathPrefix}.high`,
  },
  major: {
    icon: 'severity-major',
    color: colorConst.statusMajor,
    textI18nPath: `${i18nPathPrefix}.major`,
  },
  minor: {
    icon: 'severity-minor',
    color: colorConst.statusMinor,
    textI18nPath: `${i18nPathPrefix}.minor`,
  },
  low: {
    icon: 'severity-low',
    color: colorConst.statusLow,
    textI18nPath: `${i18nPathPrefix}.low`,
  },
  unknown: {
    icon: 'status-unknown',
    color: colorConst.statusUnknown,
    textI18nPath: `${i18nPathPrefix}.unknown`,
  },
  normal: {
    icon: 'checkmark',
    color: colorConst.statusNormal,
    textI18nPath: `${i18nPathPrefix}.normal`,
  },
  inqueue: {
    icon: 'in-queue',
    color: colorConst.statusInqueue,
    textI18nPath: `${i18nPathPrefix}.inqueue`,
  },
  inprogress: {
    icon: 'more',
    color: colorConst.statusInprogress,
    textI18nPath: `${i18nPathPrefix}.inprogress`,
  },
  up: {
    icon: 'arrow-left2',
    color: colorConst.statusSuccess,
    textI18nPath: `${i18nPathPrefix}.up`,
  },
  down: {
    icon: 'arrow-left2',
    color: colorConst.statusHigh,
    textI18nPath: `${i18nPathPrefix}.down`,
  },
  debug: {
    icon: 'quarantine',
    color: colorConst.dark_base,
    textI18nPath: `${i18nPathPrefix}.debug`,
  },
  users: {
    icon: 'user',
    color: colorConst.chartPrimaryPalette[4],
    textI18nPath: `${i18nPathPrefix}.users`,
  },
  apps: {
    icon: 'cloud-stack',
    color: colorConst.chartPrimaryPalette[3],
    textI18nPath: `${i18nPathPrefix}.apps`,
  },
  activated: {
    icon: 'shield',
    color: colorConst.selected,
    textI18nPath: `${i18nPathPrefix}.activated`,
  },
  alert: {
    icon: 'active-threats',
    color: colorConst.statusHigh,
    textI18nPath: `${i18nPathPrefix}.alert`,
  },
};
