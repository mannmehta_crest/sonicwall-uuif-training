/**
 * Define all color constants in JS/TS context, for cases when we would like
 * to use these colors in JS/TS, e.g. charts.
 * These colors should match with those in _color-vars.scss.
 */

export const selected = '#ff791a';
export const selectedText = '#333333';
export const active = '#47afe8';
export const backgroundMat = '#ffffff';
export const defaultColor = '#000000';

/* tslint:disable:variable-name */
export const dark_base = '#68696b';
export const dark_base2 = '#65656a';
export const dark_background = '#333333';
export const dark_mat = '#000000';
export const dark_border = selected;
export const dark_border2 = '#7a7979';
export const dark_highlight = '#515151';
export const dark_highlight2 = '#434343';
export const dark_text = '#c9c9c9';
export const dark_textDisabled = '#65656a';
export const dark_slider = '#666666';
export const dark_icon = '#cccccc';
export const dark_iconHover = '#ffffff';
export const dark_iconSelected = selected;

export const light_base = '#fafafa';
export const light_background = light_base;
export const light_border = '#cccccc';
export const light_highlight = '#f2f2f2';
export const light_text = '#333333';
export const light_textDisabled = '#cccccc';
export const light_slider = '#999999';
export const light_icon = '#868686';
export const light_iconHover = light_text;
export const light_iconSelected = selected;
/* tslint:enable:variable-name */

export const statusCritical = '#cc0000';
export const statusHigh = '#ff0000';
export const statusMajor = '#ff9900';
export const statusMinor = '#ffcc00';
export const statusLow = '#efe47e';
export const statusNormal = '#99cc00';
export const statusUnknown = '#cccccc';
export const statusInformational = '#47afe8';
export const statusSuccess = '#99cc00';
export const statusFailed = '#cc0000';
export const statusInqueue = '#ffcc00';
export const statusInprogress = '#cccccc';
export const statusInfoMsg = '#E4E4E4';

export const filterItemLight = '#029CDC';

export const chartPrimaryPalette = [
  '#0DB6A3',
  '#CE8ABE',
  '#FDA348',
  '#E90076',
  '#787DB6',
  '#47AFE8',
  '#BBDB70',
  '#FF4229',
  '#FCCEC6',
  '#7DD7E0',
];

export const chartSecondaryPalette = [
  '#532456',
  '#EFDDC0',
  '#00778C',
  '#006CB6',
  '#EE5B62',
  '#00778C',
  '#E0B63B',
  '#D7BEDC',
  '#00A38A',
  '#BEE2CA',
  '#BBD3EB',
  '#815283',
  '#D23A95',
  '#CEBEA4',
  '#EDEBE4',
  '#3788B0',
  '#F9B19A',
  '#FFE982',
  '#80CFCC',
  '#EDADBB',
];
