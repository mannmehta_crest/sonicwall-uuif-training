export * from './mem-store';

import Vue, { VNode, VNodeData } from 'vue';
import _ from 'lodash';
import Color from 'color';
import { memStore } from './mem-store';
import * as colorConst from './color-const';
import { statusInfoMap } from './status-info-map';
import { sizeMap } from './size-map';
import { LooseObject } from '../common-types';

export const utilService = {
  numberWithCommas(x: number): string {
    const parts = x.toString()
      .split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
  },
  getVueScopedSlot(scopedSlots: Vue['$scopedSlots'], slotName: string = 'default', props: any = {}): VNode[] {
    if (!scopedSlots) { return []; }
    const slot = scopedSlots[slotName];
    if (!slot) { return []; }
    const children = slot(props);
    return children || [];
  },
  extractFunctionalCompRootNodeData(contextData: VNodeData, isRootComponent: boolean = false): VNodeData {
    const data: VNodeData = {
      key: contextData.key,
      ref: contextData.ref,
      refInFor: contextData.refInFor,
      staticClass: contextData.staticClass,
      class: contextData.class,
      staticStyle: contextData.staticStyle,
      style: contextData.style,
      attrs: contextData.attrs,
      directives: contextData.directives,
    };
    if (isRootComponent) {
      data.nativeOn = contextData.nativeOn;
    } else {
      data.on = contextData.nativeOn;
    }
    return data;
  },
  assignWithoutUndefined<T, S1, S2, S3>(target: T, source1?: S1, source2?: S2, source3?: S3): T & S1 & S2 & S3 {
    const res = _.assignWith(target, source1!, source2!, source3!, (objValue, sourceValue) => {
      return _.isUndefined(sourceValue) ? objValue : sourceValue;
    });
    Object.keys(res)
      .forEach((key) => {
        if ((res as LooseObject)[key] === undefined) {
          delete (res as LooseObject)[key];
        }
      });
    return res;
  },
  getContrastTextColor(bgColor: string, pure: boolean = false) {
    const color = Color(bgColor);
    return color.isDark() ? (pure ? '#fff' : colorConst.dark_text) : (pure ? '#000' : colorConst.light_text);
  },
  memStore,
  colorConst,
  statusInfoMap,
  sizeMap,
};
