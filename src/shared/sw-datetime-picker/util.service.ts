const i18nPathPrefix = 'libShared.SwDatetimePicker';

function getText() {
  const text = {
    daysOfWeek: [
      `${i18nPathPrefix}.daysOfWeek[0]`,
      `${i18nPathPrefix}.daysOfWeek[1]`,
      `${i18nPathPrefix}.daysOfWeek[2]`,
      `${i18nPathPrefix}.daysOfWeek[3]`,
      `${i18nPathPrefix}.daysOfWeek[4]`,
      `${i18nPathPrefix}.daysOfWeek[5]`,
      `${i18nPathPrefix}.daysOfWeek[6]`,
    ],
    limit: 'Limit reached ({{limit}} items max).',
    loading: 'Loading...',
    minLength: 'Min. Length',
    months: [
      `${i18nPathPrefix}.months[0]`,
      `${i18nPathPrefix}.months[1]`,
      `${i18nPathPrefix}.months[2]`,
      `${i18nPathPrefix}.months[3]`,
      `${i18nPathPrefix}.months[4]`,
      `${i18nPathPrefix}.months[5]`,
      `${i18nPathPrefix}.months[6]`,
      `${i18nPathPrefix}.months[7]`,
      `${i18nPathPrefix}.months[8]`,
      `${i18nPathPrefix}.months[9]`,
      `${i18nPathPrefix}.months[10]`,
      `${i18nPathPrefix}.months[11]`,
    ],
    notSelected: 'Nothing Selected',
    required: 'Required',
    search: 'Search',
  };
  return text;
}

function getDayCount(year: number, month: number) {
  const dict = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  if (month === 1) {
    if ((year % 400 === 0) || (year % 4 === 0 && year % 100 !== 0)) {
      return 29;
    }
  }
  return dict[month];
}

function getYearMonth(year: number, month: number) {
  let retYear = year;
  let retMonth = month;
  if (month > 11) {
    retYear += 1;
    retMonth = 0;
  } else if (month < 0) {
    retYear -= 1;
    retMonth = 11;
  }
  return { year: retYear, month: retMonth };
}

function stringify(date: Date, format: string = 'MM/dd/yyyy hh:mm:ss') {
  if (!date) return '';
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();
  return format
  .replace(/yyyy/g, year.toString())
  .replace(/MM/g, (`0${month}`).slice(-2))
  .replace(/dd/g, (`0${day}`).slice(-2))
  .replace(/yy/g, year.toString())
  .replace(/M(?!a)/g, month.toString())
  .replace(/d/g, day.toString())
  .replace(/hh/g, hour.toString())
  .replace(/mm/g, minute.toString())
  .replace(/ss/g, second.toString());
}

export default {
  getDayCount,
  getYearMonth,
  getText,
  stringify,
};
