export interface DateRangeEle {
  text: number;
  date: Date;
  sclass: string;
  allowSelect?: boolean;
}

export interface DateRangePane {
  dates: DateRangeEle[];
}

export interface TimeObj {
  hour: number;
  min: number;
  sec: number;
}

export interface TimeRangeObj {
  hour: number;
  min: number;
  sec: number;
  hourEnd: number;
  minEnd: number;
  secEnd: number;
}

export type PickerType = 'date' | 'datetime' | 'time';

export interface DateTimePickerConfig {
  type?: PickerType;
  placeholder?: string;
  format?: string;
  pane?: number;
  enableRange?: boolean;
  onDayClick?: Function;
  disableSec?: boolean;
  disableMin?: boolean;
  disabledDaysOfWeek?: string[];
}
