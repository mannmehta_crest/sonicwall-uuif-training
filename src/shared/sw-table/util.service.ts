import {
  TableColumnConfig, TableEntry, TableColumnRenderFunc, TableRowExpandMode, TableData, TableDataItem, TableEntryIndexInfo,
} from './types';
import { CSSStyleOptions } from '../common-types';

const GENERAL_COL_WIDTH = 100;
const INDEX_COL_WIDTH = 41;
const SELECTION_COL_WIDTH = 41;
const EXPAND_COL_WIDTH = 31;
const HANDLE_COL_WIDTH = 20;

const AUTO_COL_FLEX_MULTIPLIER = GENERAL_COL_WIDTH;

function getNormalizedColWidth(colConfig: TableColumnConfig, index: number, nextColConfig?: TableColumnConfig): number | string {
  if (colConfig.type) {
    if (colConfig.type === 'handle') {
      return HANDLE_COL_WIDTH;
    }
    if (colConfig.type === 'index') {
      return INDEX_COL_WIDTH;
    }
    if (colConfig.type === 'selection') {
      /** special case where selection col is followed by expand col */
      return SELECTION_COL_WIDTH
        - (index > 0 ? 10 : 0)
        - (!!nextColConfig && nextColConfig.type === 'expand' ? 10 : 0);
    }
    if (colConfig.type === 'expand') {
      return EXPAND_COL_WIDTH;
    }
    /** width for `placeholder` is specified by `width` as normal column */
    if (colConfig.type !== 'placeholder') {
      return GENERAL_COL_WIDTH;
    }
  }
  if (colConfig.width !== undefined && colConfig.width !== 'auto') {
    // if (typeof colConfig.width === 'string' && colConfig.width.indexOf('%') >= 0) {
    //   return colConfig.width;
    // }
    /** treat all other cases as pixel */
    return colConfig.width;
  }
  return 'auto';
}

function getColStyles(colConfig: TableColumnConfig, index: number, nextColConfig?: TableColumnConfig): CSSStyleOptions {
  const styles: CSSStyleOptions = {};
  const width = getNormalizedColWidth(colConfig, index, nextColConfig);
  styles.width = (typeof width === 'number') ? `${width}px` : width;
  return styles;
}

function calcTableWidth(columnsConfig: TableColumnConfig[]): number {
  const candidates: number[] = [];
  let fixedWidth = 0;
  const percentageValues: string[] = [];

  columnsConfig.forEach((col, index, arr) => {
    let width = getNormalizedColWidth(col, index, arr[index + 1]);
    if (width === 'auto') {
      width = GENERAL_COL_WIDTH;
    }
    if (typeof width === 'number') {
      fixedWidth += width;
    } else {
      percentageValues.push(width);
    }
  });

  const totalPercent: number = percentageValues.reduce((acc, cur) => acc + parseInt(cur, 10), 0);
  fixedWidth = (fixedWidth * 100) / (100 - totalPercent);
  candidates.push(fixedWidth);

  percentageValues.forEach((p) => {
    const percent = parseInt(p, 10);
    const requiredWidth = (GENERAL_COL_WIDTH * 100) / (100 - percent);
    candidates.push(requiredWidth);
  });

  return candidates.reduce((max, cur) => Math.max(max, cur), -Infinity);
}

function calcColumnAdjustment(param: {
  columnsConfig: TableColumnConfig[],
  targetCol: TableColumnConfig,
  offset: number,
}) {
  interface AdjustmentMap {
    [colKey: string]: { width: number };
  }

  const { columnsConfig, targetCol, offset } = param;
  /** for now, we only support flat columns */
  const isTargetColAuto: boolean = targetCol.width === 'auto' || targetCol.width === undefined;
  let hasAutoColAhead = false;
  let hasAutoColAfter = false;
  let foundTargetCol = false;
  let targetColIndex = -1;
  let nextCol: TableColumnConfig | null = null;
  const adjustment: AdjustmentMap = {};
  const minimumWidth = 16;

  columnsConfig.forEach((col, index) => {
    if (col.key === targetCol.key) {
      foundTargetCol = true;
      targetColIndex = index;
      return;
    }
    if (foundTargetCol && index === targetColIndex + 1) {
      nextCol = col;
    }
    if (col.width === 'auto' || col.width === undefined) {
      if (foundTargetCol) {
        hasAutoColAfter = true;
      } else {
        hasAutoColAhead = true;
      }
    }
  });

  if (isTargetColAuto) {
    if (nextCol) {
      adjustment[nextCol!.key] = {
        width: Math.max(parseInt(nextCol!.width! as string, 10) - offset, minimumWidth),
      };
    }
  } else if (hasAutoColAfter) {
    adjustment[targetCol.key] = {
      width: Math.max(parseInt(targetCol.width! as string, 10) + offset, minimumWidth),
    };
  } else if (hasAutoColAhead) {
    adjustment[nextCol!.key] = {
      width: Math.max(parseInt(nextCol!.width! as string, 10) - offset, minimumWidth),
    };
  }

  return adjustment;
}

function genDefaultSortFunc(dataKey: string) {
  return function (a: TableEntry, b: TableEntry) {
    const va = a[dataKey];
    const vb = b[dataKey];
    return (va > vb ? 1 : (va < vb ? -1 : 0));
  };
}

function calcEffectiveExpandMode(entry: TableEntry, expandMode: TableRowExpandMode): TableRowExpandMode {
  return (!!entry._children ? 'in-row' : expandMode);
}

function isRowExpandable(
  effectiveColumnsConfig: TableColumnConfig[],
  entry: TableEntry,
  expandMode: TableRowExpandMode,
  expandRender: TableColumnRenderFunc,
): boolean {
  if (entry._expandable === false) { return false; }
  if (expandMode === 'slide' && !!expandRender) { return true; }
  const expandColIndex = effectiveColumnsConfig.findIndex(col => (col.type === 'expand'));
  return (expandColIndex >= 0 || !!entry._children);
}

function searchEntry(data: TableData, key: string): TableEntry | null {
  const info = tableDataFindIndex(data, e => (e._key === key));
  if (info.entryIndex < 0) { return null; }

  if (isTableEntryArray(data)) {
    return data[info.entryIndex];
  }
  if (info.groupIndex === undefined || info.groupIndex < 0) { return null; }
  return data[info.groupIndex].entries[info.entryIndex];
}

function isTableEntryArray(arr: TableData): arr is TableEntry[] {
  return arr.length <= 0 || isTableEntry(arr[0]);
}
function isTableEntry(entry: TableDataItem): entry is TableEntry {
  return (entry as Object).hasOwnProperty('_key');
}

function tableDataFindIndex(
  tableData: TableData,
  predicate: (entry: TableEntry, index: TableEntryIndexInfo, arr: TableData) => boolean,
): TableEntryIndexInfo {
  const info: TableEntryIndexInfo = { entryIndex: -1 };
  if (isTableEntryArray(tableData)) {
    info.entryIndex = tableData.findIndex((e, index, arr) => {
      return predicate(e, { entryIndex: index }, arr);
    });
  } else {
    info.groupIndex = -1;
    tableData.some((g, index) => {
      const entryIndex = g.entries.findIndex((e, eIndex) => {
        return predicate(e, { entryIndex: eIndex, groupIndex: index }, tableData);
      });
      if (entryIndex >= 0) {
        info.entryIndex = entryIndex;
        info.groupIndex = index;
        return true;
      }
    });
  }

  return info;
}

function traverseTableData(
  tableData: TableData,
  cb: (entry: TableEntry, index: TableEntryIndexInfo, arr: TableData) => void,
) {
  if (isTableEntryArray(tableData)) {
    tableData.forEach((e, index, arr) => {
      cb(e, { entryIndex: index }, arr);
    });
  } else {
    tableData.forEach((g, gIndex) => {
      g.entries.forEach((e, eIndex) => {
        cb(e, { groupIndex: gIndex, entryIndex: eIndex }, tableData);
      });
    });
  }
}

function getTableDataEntryLength(data: TableData) {
  if (isTableEntryArray(data)) {
    return data.length;
  }

  let count = 0;
  traverseTableData(data, () => { count += 1; });
  return count;
}

export const utilService = {
  AUTO_COL_FLEX_MULTIPLIER,
  getColStyles,
  calcTableWidth,
  calcColumnAdjustment,
  genDefaultSortFunc,
  isRowExpandable,
  calcEffectiveExpandMode,
  searchEntry,
  isTableEntryArray,
  isTableEntry,
  tableDataFindIndex,
  traverseTableData,
  getTableDataEntryLength,
};
