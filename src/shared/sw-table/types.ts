import { VNode, CreateElement } from 'vue';
import { SimpleClientRect, VueRenderFunction } from '../common-types';
import { IconConfig } from '../sw-icon';
import { TooltipDirValues } from '../tooltip-dir';

export type TableRowExpandMode = 'in-row' | 'slide';
export type TableRowSelectionMode = 'free' | 'single';

export interface TableRowColspanSpec {
  [colKey: string]: number | ((colIndex: number, cols: TableColumnConfig[]) => number);
}

export interface TableEntry {
  _key: string;
  _children?: TableEntry[];
  /**
   * Indicate if the `_children` array need to be initially lazily loaded.
   * Or if there is no children array for this entry, but table row is expandible,
   * this indicate if the expanded contents need to be lazy loaded.
   * If `true`, loading piece will be shown in the expanded area.
   */
  _toLoadChildren?: boolean;
  /** total number of children entries. Useful when you want to do bufferring loading along with pagination for nested tables. */
  _childTotal?: number;
  /** If specified boolean value, this will be used as the master/primary flag to determine if current entry is selectable */
  _selectable?: boolean;
  /** If specified boolean value, this will be used as the master/primary flag to determine if current entry is expandable */
  _expandable?: boolean;
  /** Config for colspan specific to the row of this entry, the key is `key` in `TableColumnConfig` */
  _colspan?: TableRowColspanSpec;
  /** Class name for the row of this entry */
  _className?: string;
  /** Color for the indicator if indicator col is included */
  _indicatorColor?: string;
  /** draggable for this row if indicator col is included */
  _indicatorDraggable?: boolean;
  [name: string]: any;
}

export type GroupLabelRenderFunc = (h: CreateElement, data: { group: TableEntryGroup, index: number }) => VNode;
export interface TableEntryGroup {
  key: string;
  label: string | GroupLabelRenderFunc;
  entries: TableEntry[];
}

export type TableData = TableEntry[] | TableEntryGroup[];
export type TableDataItem = TableEntry | TableEntryGroup;

export interface TableEntryIndexInfo {
  groupIndex?: number;
  entryIndex: number;
}

/** the `index` is not the entry index in original data array, but just index in the display order of table body. */
export type TableColumnRenderFunc = (h: CreateElement, data: { entry: TableEntry, index: number }) => VNode | VNode[];

export interface TableColumnConfig {
  key: string;
  type?: 'handle' | 'index' | 'selection' | 'expand' | 'placeholder';
  dataKey?: string;
  title?: string | VueRenderFunction;
  tooltip?: TooltipDirValues;
  width?: number | 'auto'; /** only supports px */
  scalable?: boolean; /** only effective when `width` is set to px */
  render?: TableColumnRenderFunc;
  /**
   * if it is function, we use the function to sort;
   * if it is `true`, we use default sort function to sort;
   * if it is `false`, we don't sort, but show sorting icon for the column;
   * if it is not specified, the column won't be sorted neither displayed as sortable.
   */
  sorter?: ((a: TableEntry, b: TableEntry) => number) | boolean;
  children?: TableColumnConfig[];
  fixed?: boolean;
  className?: string | ((entry: TableEntry, index: number) => string | void);
  headerCellClassName?: string;
}

export interface SortedColumnInfo {
  colKey: string;
  ascending: boolean;
}

export interface TableFooterConfig {
  render?: VueRenderFunction;
  colFooterRenders?: {[colKey: string]: VueRenderFunction};
}

export interface TableBodyMetrics {
  left: number | null;
  top: number | null;
  width: number | null;
  clientWidth: number | null;
  height: number | null;
  clientHeight: number | null;
}

export interface TableHeaderMetrics {
  clientHeight: number | null;
}

export interface PaginationConfig {
  pageNumber: number;
  pageSize: number;
  pageSizeOpts?: number[];
  placement?: 'top' | 'bottom';
  /** The offset that the first entry in the actual fed `tabelData` is away from the first entry of conceptual full dataset */
  datasetOffset?: number;
  /** The total entry number of the conceptual full dataset */
  datasetTotal?: number;
  nestedPagination?: PaginationConfig | null;
}

export interface InfiniteScrollConfig {
  datasetTotal?: number;
}

export interface ColumnResizerTrigger {
  rect: SimpleClientRect;
  col: TableColumnConfig;
  colEl: HTMLElement;
}
export interface ClaimedColumnResizerTrigger {
  rect: SimpleClientRect | null;
  col?: TableColumnConfig;
  colEl?: HTMLElement;
  relatedTarget?: HTMLElement;
}

export interface FixedColumnsConfigDescriptor {
  left: TableColumnConfig[] | null;
  right: TableColumnConfig[] | null;
}

export type TableEntryFloatActionsGenerator = (entry: TableEntry, index: number) => TableEntryFloatAction[];
export interface TableEntryFloatAction {
  icon: IconConfig;
  disabled?: boolean;
  tooltip?: TooltipDirValues;
  action: (entry: TableEntry, index: number) => void;
}

export interface TableEntrySelectingEventData {
  entryKey: string;
  selecting: boolean;
}

export interface TableEntryExpandingEventData {
  entryKey: string;
  expanding: boolean;
}

export interface GroupCollapseRequestData {
  groupKey: string;
  collapsing: boolean;
}

export interface ColumnSortRequestData {
  colKey: string;
  cancel?: boolean;
  ascending?: boolean;
}

export interface ColumnAdjustRequestData {
  [colKey: string]: { width: number | string };
}

export interface PageNumberChangeRequestData {
  pageNumber: number;
  isTransient: boolean;
}

export interface PageSizeChangeRequestData {
  pageSize: number;
}

export interface TableRowClaimExpandEventData {
  el: HTMLElement;
  entry: TableEntry;
  index: number;
  expanded: boolean;
}

export interface ClaimedRowInsertCaret {
  srcEntry: TableEntry;
  rowEl: HTMLElement | null;
  position?: 'before' | 'after';
}

export interface TableEntryMoveRequestData {
  entry: TableEntry;
  targetEntry: TableEntry | null;
  targetGroup?: TableEntryGroup;
}

/**
 * FLoat Action placement type could be by the side of pointer.
 * default is `end`
 */
export type FloatActionPlacement = 'by-pointer' | 'end';

export interface FloatActionPointerPos {
  clientX: number;
  clientY: number;
}
