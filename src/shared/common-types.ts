import { CreateElement, VNode } from 'vue';

/** shortcut for language */
export type LooseObject = Record<string, any>;
export type NonPartial<T> = {
  [P in keyof T]: NonNullable<T[P]>;
};
export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;
export type ArgumentsType<T> = T extends  (...args: infer U) => any ? U: never; // tslint:disable-line

/** DOM/CSS specific */
export type CSSStyleOptions = Partial<CSSStyleDeclaration>;
export type SimpleClientRect = Pick<ClientRect, 'top' | 'left' | 'right' | 'bottom' | 'width' | 'height'>;
export interface DimensionRect {
  top: number;
  left: number;
  width: number;
  height: number;
}

/** Vue Specific */
export type VuexMappedMutation<P> = P extends undefined ? () => void : (payload: P) => void;
export type VuexMappedAction<P, R> = P extends undefined ? () => Promise<R> : (payload: P) => Promise<R>;
export type VueRenderFunction = (h: CreateElement) => VNode;

/** shared types for components */
export type StatusName = 'informational' | 'critical' | 'high' | 'normal' | 'major' |
  'minor' | 'low' | 'unknown' | 'success' | 'failed' | 'inqueue' | 'inprogress' | 'up' |
  'down' | 'debug' | 'users' | 'apps' | 'activated' | 'alert';
