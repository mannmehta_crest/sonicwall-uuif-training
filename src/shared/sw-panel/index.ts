import SwPanel from './sw-panel.vue';
import SwPanelGroup from './sw-panel-group.vue';

export * from './types';

export {
  SwPanel,
  SwPanelGroup,
};
