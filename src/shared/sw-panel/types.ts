import { StatusName } from '../common-types';

export interface PanelSummaryConfig {
  text?: string;
  controlMode: 'status' | 'toggle' | 'none';
  controlValue?: StatusName | boolean;
}

export interface PanelBulletConfig {
  title: string;
  text?: string;
  controlValue?: StatusName;
}

export interface ProvidedPanelParam {
  expandedPanels: string[];
  theme: 'light' | 'dark';
}
