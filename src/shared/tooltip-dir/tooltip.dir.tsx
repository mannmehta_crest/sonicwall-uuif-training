import { popperService, PopperDirValues, PopperDirModifiers } from '../popper-service';
import SwTooltip from './sw-tooltip.vue';

interface TooltipDirValuesInternal extends Partial<PopperDirValues> {
  compact?: boolean;
  maxWidth?: number;
  maxHeight?: number;
}

export type TooltipDirValues = Pick<TooltipDirValuesInternal,
  'persistent' | 'placement' | 'content' | 'enabled' | 'includePopperForHover' | 'doAnimation' | 'delayForHover' |
  'compact' | 'maxWidth' | 'maxHeight'>;

export interface TooltipDirModifiers extends Partial<PopperDirModifiers> {}

const tooltip = popperService.generateDirective<TooltipDirValuesInternal, TooltipDirModifiers>({
  dirId: 'tooltip',
  component: SwTooltip,
  componentPropNames: ['compact', 'maxWidth', 'maxHeight'],
  valueDefault: {
    compact: false,
    maxWidth: 200,
    maxHeight: 200,
    /** below are pre-defined value options */
    enabled: true,
    content: 'No tooltip?',
    doAnimation: false,
    persistent: false,
    placement: 'top',
    trigger: 'hover',
    includePopperForHover: true,
    delayForHover: 400,
  },
  modifierNames: [],
  visibleTransitionName: 'sw-tooltip-fade',
});

export default tooltip;
