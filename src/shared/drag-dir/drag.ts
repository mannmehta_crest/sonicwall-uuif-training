import Vue, { DirectiveOptions } from 'vue';
import SwDragOverlay from './sw-drag-overlay.vue';

import { DragDirValues } from './types';

const drag: DirectiveOptions = {
  inserted(el, binding) {
    const oDiv = el;
    const dirValues: DragDirValues = binding.value;
    const useOverlay: boolean = binding.modifiers.overlay;
    const preventNativeDrag: boolean = binding.modifiers.prevent;

    function captureClick(clickEvent: MouseEvent) {
      if (oDiv.contains(clickEvent.target as HTMLElement)) {
        clickEvent.stopPropagation();
        clickEvent.stopImmediatePropagation();
        clickEvent.preventDefault();
      }
      document.removeEventListener('change', captureChange, true); // for Edge
      document.removeEventListener('click', captureClick, true);
    }

    function captureChange(changeEvent: Event) {
      changeEvent.stopPropagation();
      changeEvent.stopImmediatePropagation();
      changeEvent.preventDefault();
      document.removeEventListener('change', captureChange, true);
    }

    function captureDrag(dragEvent: DragEvent) {
      dragEvent.preventDefault();
      dragEvent.stopPropagation();
      dragEvent.stopImmediatePropagation();
    }

    function onMouseDown(event: MouseEvent) {
      const oDivRect = oDiv.getBoundingClientRect();
      const disX = event.clientX - oDivRect.left;
      const disY = event.clientY - oDivRect.top;
      const width = oDivRect.width;
      const height = oDivRect.height;
      let isDragging = false;
      let overlayInstance: Vue | null = null;

      /** remove any added capture handlers which have never been executed */
      document.removeEventListener('change', captureChange, true);
      document.removeEventListener('click', captureClick, true);

      if (preventNativeDrag) {
        /** for Firefox */
        document.addEventListener('drag', captureDrag, true);
        document.addEventListener('dragstart', captureDrag, true);
        document.addEventListener('dragend', captureDrag, true);
      }

      function onMouseMove(moveEvent: MouseEvent) {
        let l = moveEvent.clientX - disX; // Math.max(moveEvent.clientX - disX, 0);
        let t = moveEvent.clientY - disY; // Math.max(moveEvent.clientY - disY, 0);

        if (l !== oDivRect.left || t !== oDivRect.top) { isDragging = true; }

        if (preventNativeDrag) {
          /** for Chrome and Edge */
          moveEvent.preventDefault();
          moveEvent.stopPropagation();
          moveEvent.stopImmediatePropagation();
        }

        if (typeof dirValues.positioning === 'function') {
          dirValues.positioning({ x: l, y: t });
        } else {
          const [
            clientWidth,
            clientHeight,
          ] = [
            document.body.clientWidth,
            document.body.clientHeight,
          ];
          if (l + width + 0 > clientWidth) { l = clientWidth - 0 - width; }
          if (t + height + 0 > clientHeight) { t = clientHeight - 0 - height; }
          oDiv.style.left = `${l}px`;
          oDiv.style.top = `${t}px`;
        }
      }

      function onMouseUp(upEvent: MouseEvent) {
        if (isDragging) {
          upEvent.preventDefault();
          upEvent.stopImmediatePropagation();
          upEvent.stopPropagation();
          document.addEventListener('change', captureChange, true);
          document.addEventListener('click', captureClick, true);
        }
        cleanOverlay();
        if (preventNativeDrag) {
          document.removeEventListener('drag', captureDrag, true);
          document.removeEventListener('dragstart', captureDrag, true);
          document.removeEventListener('dragend', captureDrag, true);
        }
        document.removeEventListener('mousemove', onMouseMove, true);
        document.removeEventListener('mouseup', onMouseUp, true);

        if (typeof dirValues.done === 'function') {
          const l = upEvent.clientX - disX; // Math.max(upEvent.clientX - disX, 0);
          const t = upEvent.clientY - disY; // Math.max(upEvent.clientY - disY, 0);
          dirValues.done({ x: l, y: t });
        }
      }

      function createOverlay() {
        if (!oDiv.offsetParent) { return; }
        overlayInstance = new SwDragOverlay({
          propsData: {
            id: `sw-drag-overlay-${Math.floor(Math.random() * 1000000)}`,
          },
        });
        overlayInstance.$mount();
        oDiv.offsetParent.appendChild(overlayInstance.$el);
      }

      function cleanOverlay() {
        if (overlayInstance) {
          if (oDiv.offsetParent) {
            const oldOverlayEl = oDiv.offsetParent.querySelector(`#${(overlayInstance as any).id}`);
            if (oldOverlayEl) {
              oDiv.offsetParent.removeChild(oldOverlayEl);
            }
          }
          overlayInstance.$destroy();
          overlayInstance = null;
        }
      }

      if (useOverlay) {
        if (overlayInstance) { cleanOverlay(); }
        createOverlay();
      }

      if (typeof dirValues.start === 'function') {
        dirValues.start();
      }

      document.addEventListener('mousemove', onMouseMove, true);
      document.addEventListener('mouseup', onMouseUp, true);
    }

    oDiv.addEventListener('mousedown', onMouseDown);
  },
};

export default drag;
