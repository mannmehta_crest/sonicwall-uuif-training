export type DragPosition = {x: number, y: number};
export type PositioningFunction = (toClientPos: DragPosition) => void;

export interface DragDirValues {
  start: () => void;
  positioning: PositioningFunction;
  done: PositioningFunction;
}
