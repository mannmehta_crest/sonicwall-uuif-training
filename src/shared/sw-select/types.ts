import { VueRenderFunction } from '../common-types';

export interface SelectOption {
  id: string;
  label: string | VueRenderFunction;
  /** effective only when using renderfunc for label and searchable */
  labelStringRep?: string | (() => string);
  value?: string | number;
  disabled?: boolean;
}
