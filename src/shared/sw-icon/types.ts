export interface IconConfig {
  name: string;
  size?: string | number;
  color?: string;
  rotate?: number;
  bottomAlign?: boolean;
  asBlock?: boolean;
}
