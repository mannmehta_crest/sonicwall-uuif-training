/**
 * For now this service is just a simple wrapper of the awesome media query javascript
 * library [enquire.js](http://wicky.nillia.ms/enquire.js/), and provides some default
 * setup suitable for UUIF and most application using UUIF/UUIT.
 *
 * The default breakpoints make reference to
 * https://ricostacruz.com/til/css-media-query-breakpoints
 */

import enquire from 'enquire.js';
import { BehaviorSubject } from 'rxjs';
import { MediaBreakpoint, MediaQueryServiceInitOptions,
  MediaQueryServiceEvent, MediaQueryServiceEventCallback,
  MediaQueryEventSubscription,
} from './types';

const defaultBreakpoints: MediaBreakpoint[] = [
  { name: 'mobile-portrait', minWidth: 0, maxWidth: 479.98 },
  { name: 'mobile-landscape', minWidth: 480, maxWidth: 767.98 },
  { name: 'tablet-portrait', minWidth: 768, maxWidth: 991.98 },
  { name: 'tablet-landscape', minWidth: 992, maxWidth: 1199.98 },
  { name: 'desktop', minWidth: 1200 },
];

const subject = new BehaviorSubject<MediaQueryServiceEvent | null>(null);

class MediaQueryService {
  private isInitialized: boolean = false;
  private breakpoints: MediaBreakpoint[] = [];

  public init(options?: MediaQueryServiceInitOptions) {
    if (this.isInitialized) { return; }

    const effectiveOptions: MediaQueryServiceInitOptions = Object.assign(
      {
        breakpoints: defaultBreakpoints,
      },
      options,
    );
    this.breakpoints = effectiveOptions.breakpoints!;

    this.registerBreakpoints();

    this.isInitialized = true;
  }

  public subscribe(callback: MediaQueryServiceEventCallback): MediaQueryEventSubscription {
    return subject.subscribe((event) => {
      if (!event) { return; }
      callback(event);
    });
  }

  public unsubscribe(subscription: MediaQueryEventSubscription) {
    subscription.unsubscribe();
  }

  private registerBreakpoints() {
    function sendEvent(type: MediaQueryServiceEvent['type'], bp: MediaBreakpoint) {
      subject.next({
        type,
        breakpointName: bp.name,
        sourceBreakpoint: bp,
      });
    }

    this.breakpoints.forEach((bp) => {
      const queryTerm = this.composeMediaQueryCriteria(bp);
      enquire.register(queryTerm, {
        deferSetup: true,
        match: () => {
          sendEvent('match', bp);
        },
        unmatch: () => {
          sendEvent('unmatch', bp);
        },
        setup: () => {
          sendEvent('setup', bp);
        },
        destroy: () => {
          sendEvent('destroy', bp);
        },
      });
    });
  }

  private composeMediaQueryCriteria(breakpoint: MediaBreakpoint): string {
    const mediaFeatures: string[] = [];

    if (breakpoint.minWidth !== undefined) {
      mediaFeatures.push(`(min-width: ${breakpoint.minWidth}px)`);
    }
    if (breakpoint.maxWidth !== undefined) {
      mediaFeatures.push(`(max-width: ${breakpoint.maxWidth}px)`);
    }

    return `screen and ${mediaFeatures.join(' and ')}`;
  }
}

export const mediaQueryService = new MediaQueryService();
