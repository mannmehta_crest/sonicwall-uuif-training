import { Subscription } from 'rxjs';

export interface MediaBreakpoint {
  name: string;
  minWidth?: number;
  maxWidth?: number;
}

export interface MediaQueryServiceEvent {
  type: 'setup' | 'match' | 'unmatch' |  'destroy';
  breakpointName: string;
  readonly sourceBreakpoint: MediaBreakpoint;
}
export type MediaQueryServiceEventCallback = (event: MediaQueryServiceEvent) => void;

export interface MediaQueryServiceInitOptions {
  breakpoints?: MediaBreakpoint[];
}

export type MediaQueryEventSubscription = Subscription;
