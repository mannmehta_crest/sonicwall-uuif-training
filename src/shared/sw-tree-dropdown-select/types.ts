import { VueRenderFunction } from '../common-types';
import { TooltipDirValues } from '../tooltip-dir';

export interface TreeDropdownNode {
  label: VueRenderFunction | string;
  value: string;
  selected?: boolean;
  icon?: string;
  children: TreeDropdownNode[];
  id: string;
  expanded: boolean;
  otherContent?: VueRenderFunction | string;
  disabled?: boolean;
  tooltip?: TooltipDirValues;
}
