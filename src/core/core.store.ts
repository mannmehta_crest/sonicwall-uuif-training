/**
 * Core states will be in global scope even they are in core state module (i.e. not in the
 * isolated namespace) as they should be always appllication-level data accessed by various
 * feature modules.
 *
 */

import { Module, Payload } from 'vuex';
import axios, { AxiosError } from 'axios';
import { RootStoreState } from '@/store';

export interface CoreStoreState {
  appVersion: { version: string; buildTime: string; gitCommit: string | null; };
  appLocale: string;
  navCollapsed: boolean;
  selectedTopTabItemId: string;
  selectedNavItemId: string;
}

/** mutations and actions */
export const MUTATION_SET_APP_VERSION = 'setAppVersion';
export const MUTATION_SET_APP_LOCALE = 'setAppLocale';
export const MUTATION_SELECT_TOP_TAB_ITEM = 'selectTopTabItem';
export const MUTATION_SELECT_NAV_ITEM = 'selectNavItem';
export const MUTATION_SET_NAV_COLLAPSED = 'setNavCollapsed';

/** payloads */
export interface SelectTopTabItemPayload extends Partial<Payload> {
  navId: string;
}
export interface SelectNavItemPayload extends Partial<Payload> {
  navId: string;
}
export interface SetNavCollapsedPayload extends Partial<Payload> {
  collapse: boolean;
}
export interface SetAppVersionPayload extends Partial<Payload> {
  version: CoreStoreState['appVersion'];
}
export interface SetAppLocalePayload extends Partial<Payload> {
  locale: string;
}

export default {
  state: {
    appVersion: {
      version: process.env.APP_VERSION,
      buildTime: process.env.APP_BUILD_TIME,
      gitCommit: process.env.APP_GIT_COMMIT_ID,
    },
    appLocale: 'en-US',
    navCollapsed: false,
    selectedTopTabItemId: '',
    selectedNavItemId: '',
  },
  /* tslint:disable:function-name */
  mutations: {
    [MUTATION_SET_APP_VERSION](state, payload: SetAppVersionPayload) {
      state.appVersion = payload.version;
    },
    [MUTATION_SET_APP_LOCALE](state, payload: SetAppLocalePayload) {
      state.appLocale = payload.locale;
    },
    [MUTATION_SELECT_TOP_TAB_ITEM](state, payload: SelectTopTabItemPayload) {
      state.selectedTopTabItemId = payload.navId;
    },
    [MUTATION_SELECT_NAV_ITEM](state, payload: SelectNavItemPayload) {
      state.selectedNavItemId = payload.navId;
    },
    [MUTATION_SET_NAV_COLLAPSED](state, payload?: SetNavCollapsedPayload) {
      state.navCollapsed = payload ? payload.collapse : !state.navCollapsed;
    },
  },
  actions: {
    getAppVersion({ commit }) {
      axios.get('/version.json')
        .then((resp) => {
          commit(MUTATION_SET_APP_VERSION, { version: resp.data } as SetAppVersionPayload);
        })
        .catch((error: AxiosError) => {
          console.error(`Failed to load build version: ${error.message}`);
          commit(MUTATION_SET_APP_VERSION, { version: { version: 'failed', buildTime: '', gitCommit: null } } as SetAppVersionPayload);
        });
    },
  },
  /* tslint:enable:function-name */
} as Module<CoreStoreState, RootStoreState>;
