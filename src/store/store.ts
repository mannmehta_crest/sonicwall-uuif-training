import Vue from 'vue';
import Vuex, { Store } from 'vuex';

import { CoreStoreModule, CoreStoreState } from '@/core';
import { SwFtrDashboardStoreModule, SwFtrDashboardStoreState } from '@/modules/sw-ftr-dashboard';

/** we have no states in the root */
export interface RootStoreState {
  CoreStoreModule: CoreStoreState;
  SwFtrDashboardStoreModule: SwFtrDashboardStoreState;
  [module: string]: any;
}

Vue.use(Vuex);

export const store = new Vuex.Store<RootStoreState>({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    CoreStoreModule,
    SwFtrDashboardStoreModule,
  },
});

export type AppStore = Store<RootStoreState>;
