import Vue from 'vue';
import VueRouter, { RouterOptions } from 'vue-router';
import { SwAppMain } from '@/core';
import { SwFtrHelloSonicwallRoute, getDefaultParams } from '@/modules/sw-ftr-hello-sonicwall';
import { SwFtrDashboardRoute } from '@/modules/sw-ftr-dashboard';
import { SwFtrChartDemonstrationRoute } from '@/modules/sw-ftr-chart-demonstration';

const globalDefaultRouteName: string = 'globalDefault';

Vue.use(VueRouter);

export const router = new VueRouter(<RouterOptions>{
  mode: 'history',
  base: process.env.PUBLIC_PATH,
  routes: [
    {
      path: '/m',
      component: SwAppMain,
      children: [
        { ...SwFtrHelloSonicwallRoute, name: globalDefaultRouteName },
        { ...SwFtrDashboardRoute },
        { ...SwFtrChartDemonstrationRoute },
      ],
    },
    {
      /** for whatever unmatched routes */
      path: '/*',
      redirect: { name: globalDefaultRouteName, params: getDefaultParams() },
    },
  ],
});
