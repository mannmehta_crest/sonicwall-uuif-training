import 'core-js/stable';

import 'highlight.js/styles/default.css';
import './assets/scss/uuif.scss';
import './assets/scss/app.scss';

import Vue from 'vue';

import Bootstrap from './bootstrap.vue';
import { router } from './router';
import { store } from './store';
import { i18nService } from '@/shared/i18n-service';
import { navService } from '@/shared/nav-service';
import { dropdown } from '@/shared/dropdown-dir';
import { tooltip } from '@/shared/tooltip-dir';
import { drag } from '@/shared/drag-dir';

/** init the nav id on startup and watch it for changes */
navService.initNavId(router, store);

/** init global directives */
Vue.directive('dropdown', dropdown);
Vue.directive('tooltip', tooltip);
Vue.directive('drag', drag);

new Vue({
  router,
  store,
  i18n: i18nService.i18n,
  el: '#app',
  template: '<Bootstrap/>',
  components: { Bootstrap },
});
