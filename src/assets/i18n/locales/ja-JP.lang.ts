import _ from 'lodash';
import { LocaleMessageObject } from 'vue-i18n';

import { I18nLangExport } from '@/assets/i18n/types';
import { lang as libLang } from '@/assets/i18n/locales/lib/ja-JP.lib-lang';

const appMessageObj: LocaleMessageObject = {
  navService: {
    items: {
      home: 'ホーム',
      component: 'コンポーネント',
      'design-patterns': 'デザイン パターン',

      intro: '始めに',

      'layout-comp': 'レイアウト',
      'comp-media-query-service': 'メディア クエリ サービス',
      'comp-flex-layout': 'フレックス レイアウト',
      'comp-scroll-view': 'スクロール表示',
      'comp-tab-layout': 'タブ レイアウト',
      'comp-modal': 'モード',
      'comp-action-bar': 'アクション バー',
      'comp-content-toolbar': 'コンテンツ ツールバー',
      'comp-title-pane': 'タイトル ペイン',
      'comp-panel': 'パネル',
      'comp-card-view': 'カード表示',
      'info-comp': '情報',
      'comp-icon': 'アイコン',
      'comp-app-icon': 'アプリ アイコン',
      'comp-country-icon': '国アイコン',
      'comp-avatar': 'アバター',
      'comp-badge': 'バッジ',
      'comp-chip': 'チップ',
      'comp-count-block': 'カウント ブロック',
      'comp-tooltip': 'ツールチップ',
      'comp-popover': 'ポップオーバー',
      'comp-dropdown': 'ドロップダウン',
      'comp-drop-menu': 'ドロップ メニュー',
      'comp-progress': '進捗',
      'comp-spinning-progress': '進捗進行中',
      'comp-blocking-progress': '進捗遮断中',
      'comp-status-info': '状況情報',
      'comp-status-flag': '状況フラグ',
      'comp-msg-service': 'メッセージ サービス',
      'comp-confirm-service': 'サービスの確認',
      'comp-table': 'テーブル',
      'comp-tree-list': 'ツリー リスト',
      'comp-leaflet-map': 'リーフレット マップ',
      'comp-chart': 'グラフ',
      'comp-graphs': 'KeyLines グラフ',

      'nav-comp': 'ナビゲーション',
      'comp-icon-tab': 'アイコン タブ',
      'comp-breadcrumb': 'パンくずリスト',
      'comp-dash-switch': 'ダッシュ スイッチ',
      'comp-steps': 'ステップ ウィザード',

      'form-comp': 'フォーム',
      'comp-icon-form': 'フォーム',
      'comp-icon-button': 'アイコン ボタン',
      'comp-checkbox': 'チェックボックス',
      'comp-radio': 'ラジオ',
      'comp-slider': 'スライダー',
      'comp-button': 'ボタン',
      'comp-circular-button': '丸形ボタン',
      'comp-toggle': 'トグル ボタン',
      'comp-datepicker': '日時選択',
      'comp-textfield': 'テキスト フィールド',
      'comp-select': '選択',
      'comp-file-upload': 'ファイル アップロード',
      'comp-color-picker': 'Color Picker',
      'comp-validation-service': '検証サービス',

      'misc-comp': 'その他',
      'comp-circle-menu': '丸形メニュー',
      'comp-tree-dropdown': 'ツリー ドロップダウン選択',

      'despat-despat': 'デザイン パターン',
      'despat-login': 'ログイン',
      'despat-filters': 'フィルタ',
      'despat-forms': 'フォーム例',
      'despat-analytics': '分析',
      'despat-geo-map': '地域地図',
      'despat-avatar-dropdown-menu': 'アバター ドロップダウン メニュー',
    },
  },
  modules: {
    generals: {
      showcases: 'ショーケース',
      apiTables: 'API テーブル',
    },
    tabLayout: {
      basicTitle: '基本タブ レイアウト',
      basicDesc: [
        'タブのすべてのプロパティを含む配列を定義するには、「{0}」種別を使用します。',
        '選択されたタブの ID を指定するには、「{0}」を使用します。',
        '要求を選択したタブに応答するには、「{0}」を使用します。',
        'つまり、「{1}」と「{2}」を組み合わせるには、「{0}」を使用します。',
      ],
    },
  },
};

const messageObj = _.merge({}, libLang.messageObj, appMessageObj);

export const lang =  {
  messageObj,
  localeName: libLang.localeName,
} as I18nLangExport;
