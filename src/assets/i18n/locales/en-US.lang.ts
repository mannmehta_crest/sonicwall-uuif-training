import _ from 'lodash';
import { LocaleMessageObject } from 'vue-i18n';

import { I18nLangExport } from '@/assets/i18n/types';
import { lang as libLang } from '@/assets/i18n/locales/lib/en-US.lib-lang';

const appMessageObj: LocaleMessageObject = {
  navService: {
    items: {
      home: 'Home',
      component: 'Component',
      dashboard : 'Dashboard',
      'design-patterns': 'Design Patterns',
      'rt-charts': 'Real Time Charts',
      intro: 'Introduction',
      'charts-api': 'Charts & API',
      'table-component': 'Table & Modal',
      'layout-comp': 'Layout',
      'comp-media-query-service': 'Media Query Service',
      'comp-flex-layout': 'Flex Layout',
      'comp-scroll-view': 'Scroll View',
      'comp-tab-layout': 'Tab Layout',
      'comp-modal': 'Modal',
      'comp-action-bar': 'Action Bar',
      'comp-content-toolbar': 'Content Toolbar',
      'comp-title-pane': 'Title Pane',
      'comp-panel': 'Panel',
      'comp-card-view': 'Card View',
      'info-comp': 'Informational',
      'comp-icon': 'Icon',
      'comp-app-icon': 'App Icon',
      'comp-country-icon': 'Country Icon',
      'comp-avatar': 'Avatar',
      'comp-badge': 'Badge',
      'comp-chip': 'Chip',
      'comp-count-block': 'Count Block',
      'comp-tooltip': 'Tooltip',
      'comp-popover': 'Popover',
      'comp-dropdown': 'Dropdown',
      'comp-drop-menu': 'Drop Menu',
      'comp-progress': 'Progress',
      'comp-spinning-progress': 'Spinning Progress',
      'comp-blocking-progress': 'Blocking Progress',
      'comp-status-info': 'Status Info',
      'comp-status-flag': 'Status Flag',
      'comp-msg-service': 'Message Service',
      'comp-confirm-service': 'Confirm Service',
      'comp-table': 'Table',
      'comp-tree-list': 'Tree List',
      'comp-leaflet-map': 'Leaflet Map',
      'comp-chart': 'Charts',
      'comp-graphs': 'KeyLines Graphs',

      'nav-comp': 'Navigation',
      'comp-icon-tab': 'Icon Tab',
      'comp-breadcrumb': 'Breadcrumb',
      'comp-dash-switch': 'Dash Switch',
      'comp-steps': 'Step Wizard',

      'form-comp': 'Form',
      'comp-icon-form': 'Form',
      'comp-icon-button': 'Icon Button',
      'comp-checkbox': 'Checkbox',
      'comp-radio': 'Radio',
      'comp-slider': 'Slider',
      'comp-button': 'Button',
      'comp-circular-button': 'Circular Button',
      'comp-toggle': 'Toggle Button',
      'comp-datepicker': 'Datetime Picker',
      'comp-textfield': 'Textfield',
      'comp-select': 'Select',
      'comp-file-upload': 'File Upload',
      'comp-color-picker': 'Color Picker',
      'comp-validation-service': 'Validation Service',

      'misc-comp': 'Miscellaneous',
      'comp-circle-menu': 'Circle Menu',
      'comp-tree-dropdown': 'Tree Dropdown Select',

      'despat-despat': 'Design Patterns',
      'despat-login': 'Login',
      'despat-filters': 'Filters',
      'despat-forms': 'Form Examples',
      'despat-analytics': 'Analytics',
      'despat-geo-map': 'Geo Map',
      'despat-avatar-dropdown-menu': 'Avatar Dropdown Menu',
    },
  },
  modules: {
    generals: {
      showcases: 'Showcases',
      apiTables: 'API Table',
    },
    tabLayout: {
      basicTitle: 'Basic Tab Layout',
      basicDesc: [
        'Use {0} type to define an array containing all properties of tabs.',
        'Use {0} to specify the id of selected tab.',
        'Use {0} to respond to tab selecting request.',
        'In short, you can use {0} to combine {1} and {2}.',
      ],
    },
  },
};

const messageObj = _.merge({}, libLang.messageObj, appMessageObj);

export const lang = {
  messageObj,
  localeName: libLang.localeName,
} as I18nLangExport;
