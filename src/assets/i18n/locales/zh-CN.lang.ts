import _ from 'lodash';
import { LocaleMessageObject } from 'vue-i18n';

import { I18nLangExport } from '@/assets/i18n/types';
import { lang as libLang } from '@/assets/i18n/locales/lib/zh-CN.lib-lang';

const appMessageObj: LocaleMessageObject = {
  navService: {
    items: {
      home: '首页',
      component: '组件',
      'design-patterns': '设计模式',

      intro: '介绍',

      'layout-comp': '布局',
      'comp-media-query-service': 'Media Query 服务',
      'comp-flex-layout': '柔性布局',
      'comp-scroll-view': '滚动视图',
      'comp-tab-layout': '标签页布局',
      'comp-modal': '对话框',
      'comp-action-bar': '动作条布局',
      'comp-content-toolbar': '内容工具条',
      'comp-title-pane': '标题板块',
      'comp-panel': '面板',
      'comp-card-view': '卡片视图',

      'info-comp': '消息类',
      'comp-icon': '图标',
      'comp-app-icon': '应用图标',
      'comp-country-icon': '国家/地区图标',
      'comp-avatar': '头像',
      'comp-badge': '徽章',
      'comp-chip': '小碎片',
      'comp-count-block': '计数块',
      'comp-tooltip': '提示消息',
      'comp-popover': '弹出悬浮',
      'comp-dropdown': '弹出指令',
      'comp-drop-menu': '弹出菜单',
      'comp-progress': '进度条',
      'comp-spinning-progress': '旋转进度指示',
      'comp-blocking-progress': '进度遮盖',
      'comp-status-info': '状态信息',
      'comp-status-flag': '状态标志',
      'comp-msg-service': '消息服务',
      'comp-confirm-service': '确认框服务',
      'comp-table': '表格',
      'comp-tree-list': '树形列表',
      'comp-leaflet-map': 'Leaflet地图',
      'comp-chart': '图标',
      'comp-graphs': 'KeyLines图形',

      'nav-comp': '导航',
      'comp-icon-tab': '图标标签页',
      'comp-breadcrumb': '面包屑',
      'comp-dash-switch': '条式切换器',
      'comp-steps': '步进导引',

      'form-comp': '表单',
      'comp-icon-form': '表单',
      'comp-icon-button': '图标按钮',
      'comp-checkbox': '勾选框',
      'comp-radio': '单选按钮',
      'comp-slider': '滑块',
      'comp-button': '按键',
      'comp-circular-button': '圆形按键',
      'comp-toggle': '切换钮',
      'comp-datepicker': '时间日期选择器',
      'comp-textfield': '文本框',
      'comp-select': '下拉选择',
      'comp-file-upload': '上传文件',
      'comp-color-picker': '颜色选择器',
      'comp-validation-service': '校验服务',

      'misc-comp': '其它类',
      'comp-circle-menu': '圆形菜单',
      'comp-tree-dropdown': '树形下拉选择',

      'despat-despat': '设计模式',
      'despat-login': '登录页面',
      'despat-filters': '过滤器',
      'despat-forms': '表单示例',
      'despat-analytics': '分析页面',
      'despat-geo-map': '地理地图',
      'despat-avatar-dropdown-menu': '头像下拉菜单',
    },
  },
  modules: {
    generals: {
      showcases: '示例',
      apiTables: 'API 接口表',
    },
    tabLayout: {
      basicTitle: '基本标签页布局',
      basicDesc: [
        '使用 {0} 类型来定义一个包含所有标签页属性的数组.',
        '使用 {0} 来指定选中标签页的id.',
        '使用 {0} 来响应标签选中请求.',
        '简单的, 你可以使用 {0} 来代替 {1} 和 {2} 的组合.',
      ],
    },
  },
};

const messageObj = _.merge({}, libLang.messageObj, appMessageObj);

export const lang = {
  messageObj,
  localeName: libLang.localeName,
} as I18nLangExport;
