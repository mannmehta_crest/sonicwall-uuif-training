import { LocaleMessageObject } from 'vue-i18n';
import { I18nLangExport } from '../../types';

const messageObj: LocaleMessageObject = {
  libShared: {
    SwStatusInfo: {
      informational: '提示的',
      failed: '错误/异常',
      major: '警告',
      success: '成功',
      critical: '严重错误',
      high: '高危',
      normal: '正常',
      minor: '提醒',
      low: '低危',
      unknown: '未知',
      inqueue: '在队列中',
      inprogress: '处理中',
      up: '上升',
      down: '下降',
      debug: '调试',
      users: '风险用户',
      apps: '风险应用',
      activated: '激活的',
      alert: '报警',
    },
    SwTable: {
      paginationNumberInfo: {
        current: '当前显示 {start}-{end} 条，',
        total: '总计 {n} 条记录',
      },
      paginationPageSizeInfo: '每页 {count} 条',
      pagePrefix: '当前页',
    },
    SwDatetimePicker: {
      daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
      months: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
      startTime: '起始时间',
      endTime: '结束时间',
      hour: '小时',
      minute: '分钟',
      second: '秒',
    },
  },
};

export const lang = {
  messageObj,
  localeName: 'zh-CN',
} as I18nLangExport;
