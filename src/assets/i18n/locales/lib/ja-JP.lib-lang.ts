import { LocaleMessageObject } from 'vue-i18n';
import { I18nLangExport } from '../../types';

const messageObj: LocaleMessageObject = {
  libShared: {
    SwStatusInfo: {
      informational: '情報',
      failed: 'エラー',
      major: '注意',
      success: '成功',
      critical: '重大',
      high: '高',
      normal: '通常',
      minor: '軽微',
      low: '低',
      unknown: '不明',
      inqueue: '待機中',
      inprogress: '処理中',
      up: '上',
      down: '舌',
      debug: 'デバッグ',
      users: '危険なユーザ',
      apps: '危険なアプリ',
      activated: '有効化済み',
      alert: '警告',
    },
    SwTable: {
      paginationNumberInfo: {
        current: '{start} ～ {end} を表示中。総数: ',
        total: '登録なし | 1 件| {n} 件',
      },
      paginationPageSizeInfo: '1 ページあたり {count}',
      pagePrefix: 'ページ',
    },
    SwDatetimePicker: {
      daysOfWeek: ['日', '月', '火', '水', '木', '金', '土'],
      months: ['1 月', '2 月', '3 月', '4 月', '5 月', '6 月', '7 月', '8 月', '9 月', '10 月', '11 月', '12 月'],
      startTime: '開始日時',
      endTime: '終了日時',
      hour: '時',
      minute: '分',
      second: '秒',
    },
  },
};

export const lang = {
  messageObj,
  localeName: 'ja-JP',
} as I18nLangExport;
