import { LocaleMessageObject } from 'vue-i18n';
import { I18nLangExport } from '../../types';

const messageObj: LocaleMessageObject = {
  libShared: {
    SwStatusInfo: {
      informational: 'Informational',
      failed: 'Error',
      major: 'Warning',
      success: 'Success',
      critical: 'Critical',
      high: 'High',
      normal: 'Normal',
      minor: 'Minor',
      low: 'Low',
      unknown: 'Unknown',
      inqueue: 'In Queue',
      inprogress: 'In Progress',
      up: 'Up',
      down: 'Down',
      debug: 'Debug',
      users: 'Risky Users',
      apps: 'Risky Apps',
      activated: 'Activated',
      alert: 'Alert',
    },
    SwTable: {
      paginationNumberInfo: {
        current: 'Showing {start}-{end} of ',
        total: 'no record | 1 record | {n} records',
      },
      paginationPageSizeInfo: '{count} per page',
      pagePrefix: 'Page',
    },
    SwDatetimePicker: {
      daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
      months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      startTime: 'START TIME',
      endTime: 'END TIME',
      hour: 'Hour',
      minute: 'Min',
      second: 'Sec',
    },
  },
};

export const lang = {
  messageObj,
  localeName: 'en-US',
} as I18nLangExport;
