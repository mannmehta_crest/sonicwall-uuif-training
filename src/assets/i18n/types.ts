import { LocaleMessageObject } from 'vue-i18n';

export interface I18nLangExport {
  localeName: string;
  messageObj: LocaleMessageObject;
}
