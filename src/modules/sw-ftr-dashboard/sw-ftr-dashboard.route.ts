import { RouteConfig } from 'vue-router';
import SwFtrDashboard from './sw-ftr-dashboard.vue';

export const SwFtrDashboardRoute: RouteConfig = {// tslint:disable-line:variable-name
  path: 'feature/dashboard',
  component: SwFtrDashboard,
};
