import { Module } from 'vuex';
import { RootStoreState } from '@/store';
import { ApiService } from '@/shared/api-service';
import { TableEntry } from '@/shared/sw-table';
import { SingleAPIResponseData } from './types';

const adapter: any = new ApiService('https://reqres.in/');
export interface SwFtrDashboardStoreState {
  tableData: TableEntry[];
}

export const MUTATION_SET_DASHBOARD_TABLE_DATA = 'mutateDashboardTableData';
export const ACTION_SET_DASHBOARD_TABLE_DATA = 'setDashboardTableData';
export const MUTATION_CLEAR_DASHBOARD_TABLE_DATA = 'clearDashboardTableData';
export const MUTATION_DELETE_TABLE_DATA_ROW = 'deleteTableDataRow';

export default {
  namespaced: true,
  state: {
    tableData: [],
  },
  mutations: {
    /* tslint:disable-next-line */
    [MUTATION_SET_DASHBOARD_TABLE_DATA](state, payload : SingleAPIResponseData[]) {
      payload.forEach((element: any) => {
        element._key = state.tableData.length;
        state.tableData.push(element);
      });
    },
    /* tslint:disable-next-line */
    [MUTATION_CLEAR_DASHBOARD_TABLE_DATA](state) {
      state.tableData = [];
    },
    /* tslint:disable-next-line */
    [MUTATION_DELETE_TABLE_DATA_ROW](state, payload: string) {
      let indexToBeRemoved = -1;
      for (const key in state.tableData) {
        if (state.tableData[key]._key === payload) {
          indexToBeRemoved = parseInt(key, 10);
          break;
        }
      }
      state.tableData.splice(indexToBeRemoved, 1);
    },
  },
  actions: {
    /* tslint:disable-next-line */
    [ACTION_SET_DASHBOARD_TABLE_DATA]({ commit }) {
      const responseData: SingleAPIResponseData[] = [];
      adapter.get('api/products?page=1')
      .then((res: any) => {
        res.data.forEach((element: SingleAPIResponseData) => {
          responseData.push(element);
        });
      })
      .then(() => {
        adapter.get('api/products?page=2')
        .then((res : any) => {
          res.data.forEach((element: SingleAPIResponseData) => {
            responseData.push(element);
          });
        })
        .then(() => {
          commit(MUTATION_SET_DASHBOARD_TABLE_DATA, responseData);
        });
      })
      .catch(() => {
        commit(MUTATION_SET_DASHBOARD_TABLE_DATA, responseData);
      });
    },
  },
  getters: {
  },
} as Module<SwFtrDashboardStoreState, RootStoreState>;
