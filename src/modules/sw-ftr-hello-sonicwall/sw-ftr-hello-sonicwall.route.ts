import { RouteConfig } from 'vue-router';
import SwFtrHelloSonicwall from './sw-ftr-hello-sonicwall.vue';

export const SwFtrHelloSonicwallRoute: RouteConfig = {// tslint:disable-line:variable-name
  path: 'feature/hello-sonicwall',
  component: SwFtrHelloSonicwall,
};

export function getDefaultParams() {
  return {};
}
