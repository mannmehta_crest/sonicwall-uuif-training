import { RouteConfig } from 'vue-router';
import SwFtrChartDemonstration from './sw-ftr-chart-demonstration.vue';

export const SwFtrChartDemonstrationRoute: RouteConfig = {// tslint:disable-line:variable-name
  path: 'feature/charts',
  component: SwFtrChartDemonstration,
};
