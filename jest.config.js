const { pathsToModuleNameMapper } = require('ts-jest/utils');
const { compilerOptions } = require('./tsconfig');

module.exports = {
  testEnvironment: 'jsdom',
  testRegex: `/__tests__/unit/.+\\.spec\\.tsx?$`,
  roots: ['<rootDir>/src/', '<rootDir>/__tests__/'],
  moduleFileExtensions: ['ts', 'tsx', 'vue', 'json', 'js', 'jsx'],
  moduleNameMapper: {
    '\\.(png|jpe?g|gif|svg|mp4|webm|ogg|mp3|wav|flac|aac|woff2?|eot|ttf|otf)(\\?.*)?$': '<rootDir>/__tests__/__mocks__/file-mock.js',
    '\\.(css|scss)(\\?.*)?$': '<rootDir>/__tests__/__mocks__/style-mock.js',

    ...pathsToModuleNameMapper(compilerOptions.paths, { prefix: '<rootDir>/' }),
  },
  transform: {
    '.+\\.vue$': 'vue-jest',
    '.+\\.tsx?$': '<rootDir>/__tests__/__transformers__/ts-babel-transformer.js',
    '.+\\.jsx?$': '<rootDir>/__tests__/__transformers__/babel-transformer.js',
  },

  collectCoverage: false,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.{ts,tsx,vue}',
    '!<rootDir>/src/**/*.vue.{ts,tsx}',
    '!<rootDir>/src/**/*.d.ts',
    '!<rootDir>/src/shared/sw-chart/sw-chart.vue',
  ],
  coverageReporters: ['json'],
  coverageDirectory: '<rootDir>/__tests__/output',

  globals: {
    'vue-jest': {
      babelConfig: false,
    },
    'ts-jest': {
      tsConfig: './__tests__/tsconfig.json',
      isolatedModules: process.env.TYPE_CHECK !== 'on',
      diagnostics: {
        ignoreCodes: [7006],
      },
    },
  },
};
