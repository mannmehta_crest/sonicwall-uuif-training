#!/bin/bash

set -ue

GIT_STATUS=`git status --porcelain`
BASE_DIR=$(dirname $0)/..
RELEASE_VER=$1

APP_NAME=`grep -E '\s*"name": ".*",' $BASE_DIR/package.json | sed 's/\s*"name": "\(.*\)",\{0,1\}/\1/'`

if [[ ! "$RELEASE_VER" =~ ^[0-9]+\.[0-9]+\.[0-9]+(-(alpha|beta)\.[0-9]+)?$ ]]; then
  echo "The version number ${RELEASE_VER} is invalid"
  exit 1
fi

if [ -z "$APP_NAME" ]; then
  echo "Cannot identify the app name, please check the \"name\" field in package.json"
  exit 1
fi

echo "Check the work tree:"
if [ -n "$GIT_STATUS" ]; then
  echo "The working tree is not clean. Commit any local changes first..."
  exit 1
fi
echo "The work tree is clean."

echo "Update files to indicate version ${RELEASE_VER}"
sed -i "s/\(\"version\"\:\s*\)\"[0-9]\{1,\}\.[0-9]\{1,\}\.[0-9]\{1,\}\(-\(alpha\|beta\)\.[0-9]\{1,\}\)\{0,1\}\"/\1\"${RELEASE_VER}\"/" $BASE_DIR/package.json
if [ -e "$BASE_DIR/package-lock.json" ]; then
  sed -i "s/^\(\s\{0,2\}\"version\"\:\s*\)\"[0-9]\{1,\}\.[0-9]\{1,\}\.[0-9]\{1,\}\(-\(alpha\|beta\)\.[0-9]\{1,\}\)\{0,1\}\"/\1\"${RELEASE_VER}\"/" $BASE_DIR/package-lock.json
elif [ -e "$BASE_DIR/package-lock.json.template" ]; then
  sed -i "s/^\(\s\{0,2\}\"version\"\:\s*\)\"[0-9]\{1,\}\.[0-9]\{1,\}\.[0-9]\{1,\}\(-\(alpha\|beta\)\.[0-9]\{1,\}\)\{0,1\}\"/\1\"${RELEASE_VER}\"/" $BASE_DIR/package-lock.json.template
fi

echo "Make the release tag"
git add $BASE_DIR/package.json
if [ -e "$BASE_DIR/package-lock.json" ]; then
git add $BASE_DIR/package-lock.json
elif [ -e "$BASE_DIR/package-lock.json.template" ]; then
git add $BASE_DIR/package-lock.json.template
fi
git commit -m "${APP_NAME}: Release version ${RELEASE_VER}"
git tag -a v${RELEASE_VER} -m "${APP_NAME}: Release version ${RELEASE_VER}"

echo "Publish the release to remote"
git push origin
git push origin --tags

echo "All set!"
