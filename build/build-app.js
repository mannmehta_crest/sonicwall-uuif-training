'use strict';

const chalk = require('chalk');
const utils = require('./utils');

const appKey = process.env.APP_KEY;

if (!appKey) {
  console.log(`The ${chalk.red('APP_KEY')} is not defined, exit now.`);
  process.exit(1);
}

console.log('');
utils.info(`Building ${chalk.bold(appKey)} ...`);

const webpack = require('webpack');
const prodWebpackConfig = require('./webpack.prod.conf');

/** start the building */
webpack(prodWebpackConfig, (err, stats) => {
  if (err) throw err;

  // if (appKey === 'main') {
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false, // If you are using ts-loader, setting this to true will make TypeScript errors show up during build.
      chunks: false,
      chunkModules: false,
      excludeAssets: [/\.(woff2?|svg|ttf|eot)$/, /-svg\.\w+\.js(\.map)?$/],
      /** why we filter these warnings, check this out:
       * https://github.com/TypeStrong/ts-loader/issues/751
       */
      warningsFilter: /export .* was not found in/,
    }) + '\n\n');
  // }

  if (stats.hasErrors()) {
    console.log(chalk.red('  Build failed with errors.\n'));
    process.exit(1);
  }
});
