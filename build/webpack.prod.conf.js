'use strict'
const path = require('path')
const chalk = require('chalk')
const utils = require('./utils')
const webpack = require('webpack')
const config = require('../config')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')

/** load environment that source code will see, based on build target */
console.log(`\n> Load production environment for target: ${chalk.yellow.bold(process.env.PROD_TARGET || 'prod')}`);
let prodEnv = require('../config/prod.env')
if (process.env.PROD_TARGET === 'debug') {
  prodEnv = require('../config/prod-debug.env')
}

/** config for sub-apps */
const appKey = process.env.APP_KEY;
let outputPath = config.build.assetsRoot;
let indexFilename = config.build.index;
let indexTemplate = path.resolve(__dirname, '../index.html');

/** tweak some of the config for different sub-apps */
if (appKey !== 'main') {
  const subappConfig = config.subapps[appKey];
  if (!subappConfig) {
    console.log(`The sub-app [${appkey}] is not configured properly, exit now.`);
    process.exit(1);
  }

  const subappDir = path.resolve(__dirname, '../src/sub-apps', subappConfig.directory);

  outputPath = path.resolve(config.build.assetsRoot, subappConfig.directory),
  indexFilename = path.resolve(config.build.assetsRoot, subappConfig.directory, 'index.html'),
  indexTemplate = path.resolve(subappDir, subappConfig.index);
  baseWebpackConfig.entry = {
    [appKey]: path.resolve(subappDir, subappConfig.entry),
  };
}

const webpackConfig = merge(baseWebpackConfig, {
  mode: 'production',
  module: {
  },
  devtool: config.build.productionSourceMap ? config.build.devtool : false,
  output: {
    path: outputPath,
    filename: utils.assetsPath('js/[name].[chunkhash].js'),
    chunkFilename: utils.assetsPath('js/[name].[chunkhash].js')
  },
  optimization: {
    minimize: config.build.productionMinification,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          compress: {
            warnings: false
          }
        },
        sourceMap: config.build.productionSourceMap,
        parallel: true
      }),
    ],
    splitChunks: {
      chunks: 'all',
      maxAsyncRequests: 10,
      automaticNameMaxLength: 36,
      cacheGroups: {
        vendors: {
          reuseExistingChunk: true,
        },
        default: {
          minChunks: 1,
          reuseExistingChunk: true,
        },
      },
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': prodEnv
    }),
    new MiniCssExtractPlugin({
      filename: utils.assetsPath('css/[name].[contenthash].css'),
      ignoreOrder: true,
    }),
    // Compress extracted CSS. We are using this plugin so that possible
    // duplicated CSS from different components can be deduped.
    new OptimizeCSSPlugin({
      cssProcessorOptions: config.build.productionSourceMap
        ? { safe: true, map: { inline: false } }
        : { safe: true }
    }),
    // generate dist index.html with correct asset hash for caching.
    // you can customize output by editing /index.html
    // see https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      filename: indexFilename,
      template: indexTemplate,
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
        // more options:
        // https://github.com/kangax/html-minifier#options-quick-reference
      },
      chunksSortMode: 'none'
    }),
    // keep module.id stable when vendor modules does not change
    new webpack.HashedModuleIdsPlugin(),

    // copy custom static assets
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, '../static'),
        to: config.build.assetsSubDirectory,
        ignore: ['.*']
      },
      {
        from: path.resolve(__dirname, '../favicon.ico'),
        to: '.',
      }
    ])
  ]
})

if (config.build.productionGzip) {
  const CompressionWebpackPlugin = require('compression-webpack-plugin')

  webpackConfig.plugins.push(
    new CompressionWebpackPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: new RegExp(
        '\\.(' +
        config.build.productionGzipExtensions.join('|') +
        ')$'
      ),
      threshold: 10240,
      minRatio: 0.8
    })
  )
}

if (config.build.bundleAnalyzerReport) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
  webpackConfig.plugins.push(new BundleAnalyzerPlugin())
}

module.exports = webpackConfig
