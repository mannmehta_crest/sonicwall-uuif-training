'use strict'
const utils = require('./utils')
const webpack = require('webpack')
const config = require('../config')
const merge = require('webpack-merge')
const path = require('path')
const baseWebpackConfig = require('./webpack.base.conf')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const portfinder = require('portfinder')

const HOST = process.env.HOST
const PORT = process.env.PORT && Number(process.env.PORT)

const devWebpackConfig = merge(baseWebpackConfig, {
  mode: 'development',
  module: {
    // rules: utils.styleLoaders({ sourceMap: config.dev.cssSourceMap, usePostCSS: true })
  },
  // cheap-module-eval-source-map is faster for development
  devtool: config.dev.devtool,

  // these devServer options should be customized in /config/index.js
  devServer: {
    clientLogLevel: 'warning',
    historyApiFallback: {
      rewrites: [
        { from: /.*/, to: path.posix.join(config.dev.assetsPublicPath, 'index.html') },
      ],
    },
    hot: true,
    contentBase: false, // since we use CopyWebpackPlugin.
    compress: true,
    host: HOST || config.dev.host,
    port: PORT || config.dev.port,
    open: config.dev.autoOpenBrowser,
    overlay: config.dev.errorOverlay
      ? { warnings: false, errors: true }
      : false,
    publicPath: config.dev.assetsPublicPath,
    proxy: config.dev.proxyTable,
    quiet: true, // necessary for FriendlyErrorsPlugin
    watchOptions: {
      poll: config.dev.poll,
      ignored: /node_modules(?!\/@sonicwall)/
    }
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': require('../config/dev.env')
    }),
    new webpack.HotModuleReplacementPlugin(),
    // new webpack.NamedModulesPlugin(), // HMR shows correct file names in console on update.
    new webpack.NoEmitOnErrorsPlugin(),
    // https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.html',
      inject: true,
      chunksSortMode: 'none',
    }),
    // copy custom static assets
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, '../static'),
        to: config.dev.assetsSubDirectory,
        ignore: ['.*']
      },
      /** copy build result of all sub apps */
      ...(() => {
        if (!config.subapps) { return []; }
        return Object.values(config.subapps).map((appConfig) => {
          return {
            from: path.resolve(config.build.assetsRoot, appConfig.directory),
            to: appConfig.directory,
            ignore: ['.*'],
          };
        });
      })(),
    ])
  ],
  watchOptions: {
    poll: config.dev.poll,
    ignored: /node_modules/
  }
})

module.exports = new Promise((resolve, reject) => {
  portfinder.basePort = process.env.PORT || config.dev.port
  portfinder.getPort((err, port) => {
    if (err) {
      reject(err)
    } else {
      // publish the new Port, necessary for e2e tests
      process.env.PORT = port
      // add port to devServer config
      devWebpackConfig.devServer.port = port

      /** build sub apps first if any */
      Promise.resolve()
      .then(() => {
        if (!config.subapps) { return; }
        if (process.env.NO_SUBAPPS === 'true') { return; }

        const excludingSubapps = (process.env.NO_SUBAPPS || '').split(',');
        const apps = Object.assign({}, config.subapps);
        excludingSubapps.forEach((appKey) => {
          delete apps[appKey];
        });

        utils.info('Building all sub apps ...');
        return utils.launchBuildApps(Object.keys(apps));
      })
      .then(() => {
        // Add FriendlyErrorsPlugin
        devWebpackConfig.plugins.push(new FriendlyErrorsPlugin({
          compilationSuccessInfo: {
            messages: [
              `Your application is running here: http://${devWebpackConfig.devServer.host}:${port}`,
            ],
          },
          onErrors: config.dev.notifyOnErrors
          ? utils.createNotifierCallback()
          : undefined,
          // Need to filter out unnecessary "export not found" warnings
          // additionalTransformers: [
          //   (error) => {
          //     if (/export .* was not found in/.test(error.message) && /.tsx?$/.test(error.file)) {
          //       // return Object.assign({}, error, { type: 'export-not-found' });
          //       return {};
          //     }
          //     return error;
          //   }
          // ],
        }))

        utils.info('> Building main app and launching dev server ...');
        resolve(devWebpackConfig)
      })
      .catch((error) => {
        reject(error);
      });
    }
  })
})
