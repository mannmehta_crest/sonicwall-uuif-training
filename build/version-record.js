'use strict';

const fs = require('fs');
const path = require('path');
const chalk = require('chalk')
const shell = require('shelljs')
const utils = require('./utils');
const packageConfig = require('../package.json');
const config = require('../config');

function getGitCommit() {
  if (!shell.which('git')) { return null; }
  try {
    const gitCommit = utils.exec('git log -1 --pretty=format:"%h"');
    return gitCommit;
  } catch (e) {
    return null;
  }
}

function recordToFile() {
  const objToWrite = {
    version: packageConfig.version,
    buildTime: new Date().toUTCString(),
    gitCommit: getGitCommit(),
  };

  fs.writeFileSync(
    path.resolve(config.build.assetsRoot, 'version.json'),
    JSON.stringify(objToWrite)
  );

  console.log(chalk.blue(`  Build version:\n${JSON.stringify(objToWrite, null, '  ')}\n`));
};

module.exports = {
  getGitCommit,
  recordToFile,
};
