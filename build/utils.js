'use strict'
const childProcess = require('child_process');
const path = require('path')
const chalk = require('chalk');
const config = require('../config')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const packageConfig = require('../package.json')

const libDir =  '../libs';

const cacheLoader = {
  loader: 'cache-loader',
  options: {
    cacheDirectory: path.join(__dirname, '..', 'cache'),
  },
};

exports.assetsPath = function (_path) {
  const assetsSubDirectory = process.env.NODE_ENV === 'production'
    ? config.build.assetsSubDirectory
    : config.dev.assetsSubDirectory

  return path.posix.join(assetsSubDirectory, _path)
}

exports.cssLoaders = function (options) {
  options = options || {}

  const cssLoader = {
    loader: 'css-loader',
    options: {
      sourceMap: options.sourceMap
    }
  }

  const postcssLoader = {
    loader: 'postcss-loader',
    options: {
      sourceMap: options.sourceMap
    }
  }

  // generate loader string to be used with extract text plugin
  function generateLoaders (loader, loaderOptions) {
    const loaders = options.usePostCSS ? [cssLoader, postcssLoader] : [cssLoader]

    if (loader) {
      loaders.push({
        loader: loader + '-loader',
        options: Object.assign({}, loaderOptions, {
          sourceMap: options.sourceMap
        })
      })
    }

    // Extract CSS when that option is specified
    // (which is the case during production build)
    if (options.extract) {
      return [MiniCssExtractPlugin.loader].concat(loaders)
      // return ExtractTextPlugin.extract({
      //   use: loaders,
      //   fallback: 'vue-style-loader'
      // })
    } else {
      return ['vue-style-loader'].concat(loaders)
    }
  }

  // https://vue-loader.vuejs.org/en/configurations/extract-css.html
  return {
    css: generateLoaders(),
    postcss: generateLoaders(),
    less: generateLoaders('less'),
    sass: generateLoaders('sass', { indentedSyntax: true }),
    scss: generateLoaders('sass').concat({
      /* load variable first for every components */
      loader: 'sass-resources-loader',
      options: {
        resources: options.sassResources || config.build.preloadedResources,
      }
    }),
    stylus: generateLoaders('stylus'),
    styl: generateLoaders('stylus')
  }
}

// Generate loaders for standalone style files (outside of .vue)
exports.styleLoaders = function (options) {
  const output = []
  const loaders = exports.cssLoaders(options)

  for (const extension in loaders) {
    const loader = loaders[extension]
    output.push(Object.assign(
      {
        test: new RegExp('\\.' + extension + '$'),
        use: loader
      },
      options.resourceConditions,
    ))
  }

  return output
}

// Customized TypeScript loaders
exports.tsLoaders = function(isProduction) {
  const loaders = {
    ts: [{
      loader: 'ts-loader',
      options: {
        appendTsxSuffixTo: [/\.vue$/], // also use tsx to compile ts part
        transpileOnly: true,
      }
    }],
    tsx: [{
      loader: 'babel-loader',
    }, {
      loader: 'ts-loader',
      options: {
        appendTsxSuffixTo: [/\.vue$/],
        transpileOnly: true,
      },
    }],
  };
  if (isProduction !== true) {
    loaders.ts.unshift(cacheLoader);
    loaders.tsx.unshift(cacheLoader);
  }

  return loaders;
}

exports.createNotifierCallback = () => {
  const notifier = require('node-notifier')

  return (severity, errors) => {
    if (severity !== 'error') return

    const error = errors[0]
    const filename = error.file && error.file.split('!').pop()

    notifier.notify({
      title: packageConfig.name,
      message: severity + ': ' + error.name,
      subtitle: filename || '',
      icon: path.join(__dirname, 'logo.png')
    })
  }
}

exports.resolveLibs = function(file) {
  return path.resolve(__dirname, libDir, file);
}

exports.info = function(message) {
  console.log(`${chalk.black.bgBlue(' I ')} ${message}`);
}

exports.exec = function(cmd, cwd = undefined) {
  return childProcess.execSync(cmd, { cwd }).toString().trim()
}
exports.spawn = function(cmd, args = [], cwd = undefined) {
  return new Promise((resolve, reject) => {
    const myProcess = childProcess.spawn(cmd, args, {
      cwd,
      env: process.env,
      stdio: 'inherit',
    });
    myProcess.on('exit', (code) => {
      if (code !== 0) { return reject(); }
      resolve();
    });
  });
}

exports.launchBuildApps = function(appKeys) {
  const buildModule = path.resolve(__dirname, './build-app.js');
  let chain = Promise.resolve();

  /** NOTE: we do chain instead of parallel in order for reducing memory consumption */
  appKeys.forEach((appKey) => {
    chain = chain.then(() => {
      return new Promise((resolve, reject) => {
        const appBuildProcess = childProcess.fork(buildModule, [], {
          cwd: path.resolve(__dirname, '..'),
          env: Object.assign({}, process.env, {
            APP_KEY: appKey,
            PUBLIC_PATH: appKey === 'main' ? config.build.assetsPublicPath :
              `${config.build.assetsPublicPath}${config.subapps[appKey].directory}/`,
          }),
        });
        appBuildProcess.on('exit', (code) => {
          if (code !== 0) { return reject(); }
          resolve();
        });
      });
    });
  });

  return chain;
}
