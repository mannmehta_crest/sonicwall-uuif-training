'use strict';

process.env.NODE_ENV = 'production';

const ora = require('ora')
const rm = require('rimraf')
const chalk = require('chalk')
const config = require('../config')
const utils = require('./utils');
const recordVersion = require('./version-record');

const spinner = ora('building for production...')
spinner.start()

rm(config.build.assetsRoot, err => {
  if (err) throw err;

  const apps = [];

  if (config.subapps) {
    Object.keys(config.subapps).forEach((appKey) => {
      apps.push(appKey);
    });
  }

  apps.push('main');

  utils.launchBuildApps(apps)
  .then(() => {
    spinner.stop();

    recordVersion.recordToFile();
  })
  .then(() => {
    console.log(chalk.cyan('  Build complete.\n'));
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ));
  })
  .catch(() => {
    spinner.stop();

    console.log(chalk.red('  Some apps build failed with errors.\n'))
    process.exit(1);
  });
})
