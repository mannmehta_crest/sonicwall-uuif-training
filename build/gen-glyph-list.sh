#!/bin/bash

MY_DIR=`dirname $0`
ROOT_DIR=${MY_DIR}/..
FILENAME=${ROOT_DIR}/src/assets/fonts/icon/glyph-list.ts

rm -f $FILENAME
touch $FILENAME

echo "export default [" >> $FILENAME
grep -o -P '(?<=\.icon-).+(?=:)' ${ROOT_DIR}/src/assets/scss/rules/_icons-rules.scss | xargs -I name echo "  'name'," >> $FILENAME
echo "];" >> $FILENAME
