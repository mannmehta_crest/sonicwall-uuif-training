if (process.env.NODE_ENV === 'development') {
  console.log(`\n> Now loading babel configuration from ${__filename}\n`);
}

module.exports = {
  presets: [],
  plugins: [
    'transform-vue-jsx',
    '@babel/plugin-syntax-dynamic-import',
  ],
  env: {
    test: {
      presets: [
        [
          '@babel/preset-env',
          {
            modules: 'commonjs',
          },
        ],
      ],
      plugins: [
        'transform-vue-jsx',
        '@babel/plugin-syntax-dynamic-import',
        // 'istanbul',
      ]
    }
  }
};
