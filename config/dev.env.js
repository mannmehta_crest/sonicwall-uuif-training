'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')
const config = require('./');

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  PUBLIC_PATH: JSON.stringify(config.dev.assetsPublicPath),
})
