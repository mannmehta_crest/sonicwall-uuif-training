'use strict'

const config = require('./');
const packageConfig = require('../package.json');
const recordVersion = require('../build/version-record');

/** capture the product version info here and set it to environment */
const versionInfo = {
  version: packageConfig.version,
  buildTime: new Date().toUTCString(),
  gitCommit: recordVersion.getGitCommit(),
};

module.exports = {
  NODE_ENV: '"production"',
  PUBLIC_PATH: JSON.stringify(config.build.assetsPublicPath),
  APP_VERSION: JSON.stringify(versionInfo.version),
  APP_BUILD_TIME: JSON.stringify(versionInfo.buildTime),
  APP_GIT_COMMIT_ID: JSON.stringify(versionInfo.gitCommit),
}
