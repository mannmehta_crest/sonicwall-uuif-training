declare module "*.vue" {
  import { SwExtendedVueShim } from '@/shared/sw-extended-vue';
  export default SwExtendedVueShim;
}
